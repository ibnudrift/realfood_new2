<?php

/**
 * This is the model class for table "list_career".
 *
 * The followings are the available columns in table 'list_career':
 * @property integer $id
 * @property string $image
 * @property string $image2
 * @property string $nama
 * @property string $nama_sub
 * @property string $location
 * @property string $content
 * @property string $qualification
 * @property integer $actives
 */
class ListCareer extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ListCareer the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'list_career';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		return array(
			array('nama', 'required'),
			// array('actives', 'numerical', 'integerOnly'=>true),
			array('nama, nama_sub', 'length', 'max'=>225),
			array('content, qualification, image, image2, location, category_id, nama_sub, actives, dates_input', 'safe'),
			// Please remove those attributes that should not be searched.
			array('id, image, image2, nama, nama_sub, location, content, qualification, actives', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'image' => 'Image',
			'image2' => 'Image2',
			'nama' => 'Nama',
			'nama_sub' => 'Nama Sub',
			'location' => 'Location',
			'content' => 'Content',
			'qualification' => 'Qualification',
			'actives' => 'Actives',
			'category_id' => 'Categorys Name',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('image',$this->image,true);
		$criteria->compare('image2',$this->image2,true);
		$criteria->compare('nama',$this->nama,true);
		$criteria->compare('nama_sub',$this->nama_sub,true);
		$criteria->compare('location',$this->location,true);
		$criteria->compare('content',$this->content,true);
		$criteria->compare('qualification',$this->qualification,true);
		$criteria->compare('actives',$this->actives);
		$criteria->compare('category_id',$this->category_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}