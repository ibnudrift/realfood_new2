<?php

/**
 * This is the model class for table "tb_career_category".
 *
 * The followings are the available columns in table 'tb_career_category':
 * @property integer $id
 * @property string $nama
 * @property string $image
 * @property string $subtitle
 * @property integer $sortings
 */
class CareerCategory extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CareerCategory the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tb_career_category';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('sortings', 'numerical', 'integerOnly'=>true),
			array('nama, image, subtitle', 'length', 'max'=>225),
			// The following rule is used by search().
			array('image', 'safe'),
			// Please remove those attributes that should not be searched.
			array('id, nama, image, subtitle, sortings', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nama' => 'Nama',
			'image' => 'Image',
			'subtitle' => 'Subtitle',
			'sortings' => 'Sortings',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('nama',$this->nama,true);
		$criteria->compare('image',$this->image,true);
		$criteria->compare('subtitle',$this->subtitle,true);
		$criteria->compare('sortings',$this->sortings);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}