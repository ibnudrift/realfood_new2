<?php
$baseUrl = Yii::app()->request->hostInfo . Yii::app()->request->baseUrl;
$url = Yii::app()->request->hostInfo;
?>
<div bgcolor="#ffffff">
<font face="tahoma, arial"> 
<table border="0" cellpadding="0" cellspacing="0" >
	<tr>
		<td colspan="3">Detail Career</td>
	</tr>
	<tr>
		<td>Division</td>
		<td>:</td>
		<td><?php echo $model->divisi; ?></td>
	</tr>
	<tr>
		<td>Name</td>
		<td>:</td>
		<td><?php echo $model->name; ?></td>
	</tr>	
	<tr>
		<td>Phone Number</td>
		<td>:</td>
		<td><?php echo $model->phone; ?></td>
	</tr>
	<tr>
		<td>Email</td>
		<td>:</td>
		<td><?php echo $model->email; ?></td>
	</tr>
	<tr>
		<td>Address</td>
		<td>:</td>
		<td><?php echo $model->address; ?></td>
	</tr>
	<tr>
		<td>City</td>
		<td>:</td>
		<td><?php echo $model->city; ?></td>
	</tr>
	<tr>
		<td>Country</td>
		<td>:</td>
		<td><?php echo $model->country; ?></td>
	</tr>
	<tr>
		<td>Message</td>
		<td>:</td>
		<td><?php echo nl2br($model->body); ?>
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
</table>

</font>