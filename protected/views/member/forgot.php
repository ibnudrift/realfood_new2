<section class="top-content-inside about">
    <div class="container">
        <div class="titlepage-Inside">
            <h1>Forgot Password</h1>
        </div>
    </div>
    <div class="celar"></div>
</section>
<section class="middle-content">
    <div class="prelatife container">
        <div class="clear height-20"></div>
        <div class="height-3"></div>
        <div class="prelatife product-list-warp">
            <div class="box-featured-latestproduct" id="cart-shop">
                <div class="box-title">
                    <div class="titlebox-featured" alt="title-product">Forgot Password</div>
                    <div class="clear"></div>
                </div>
                <div class="box-product-detailg">
                    <div class="clear height-25"></div>


	<!-- /. Start Content About -->
	<div class="inside-content">
		<div class="m-ins-content m-ins-myaccount">
			<?php if(Yii::app()->user->hasFlash('success')): ?>
			
			    <?php $this->widget('bootstrap.widgets.TbAlert', array(
			        'alerts'=>array('success'),
			    )); ?>
			
			<?php endif; ?>
			<div class="margin-15">
<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id'=>'login-form',
    'type'=>'horizontal',
    // 'action'=>array('index'),
    //'htmlOptions'=>array('class'=>'well'),
	'enableClientValidation'=>false,
	'clientOptions'=>array(
		'validateOnSubmit'=>false,
	),
)); ?>
					<div class="box-account-history">
						<div class="center">
							<div class="height-50"></div>
							<div class="height-10"></div>
							<p>Enter your email address to recover / reset your forgotten password</p>
							<?php echo CHtml::errorSummary($modelLogin, '', '', array('class'=>'alert alert-danger')); ?>
							<div class="clear height-30"></div>
							<img src="<?php echo Yii::app()->baseUrl ?>/asset/images/mail.gif" alt="">
					    	<?php echo $form->textField($modelLogin, 'username', array('class'=>'form-control', 'style'=>'display: inline-block; width: auto; height: 30px;')) ?>
					    	<button type="submit" class="btn back-btn-primary-gold">SUBMIT</button>
						</div>
						 

					</div>
<?php $this->endWidget(); ?>

			</div>
			<div class="height-50"></div>
			<div class="height-50"></div>

			<div class="clear"></div>
		</div>
		<div class="clear"></div>
	</div>
	<!-- /. End Content About -->

                </div>
            </div>
        </div>
        <div class="clear height-35"></div>
        <div class="clearfix"></div>
        <!-- Go to www.addthis.com/dashboard to customize your tools -->
        <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-54092b87219ecbb4" async="async"></script>
        <!-- Go to www.addthis.com/dashboard to customize your tools -->
        <div class="addthis_native_toolbox"></div>
        <div class="clear height-35"></div>
    </div>
</section>



