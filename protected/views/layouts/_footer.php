<section class="footer">
    <div class="prelative container">
        <div class="py-4 mb-2"></div>
        <div class="row">
            <div class="col-md-60">
                <div class="img-above">
                    <img src="<?php echo Yii::app()->baseUrl; ?>/asset/images/new/logo-footer_07.jpg" alt="Bird nest drink - fit with Realfood">
                </div>
            </div>
        </div>
        <div class="py-2 mb-1"></div>
        <div class="row">
            <div class="col-md-40">
                <ul class="list-inline m-0 footers_menu">
                    <li class="list-inline-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/index', 'lang'=>Yii::app()->language)); ?>">Home</a></li>
                    <li class="list-inline-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/about', 'lang'=>Yii::app()->language)); ?>">Our Story</a></li>
                    <li class="list-inline-item"><a href="<?php echo CHtml::normalizeUrl(array('/product/landing', 'lang'=>Yii::app()->language)); ?>">Products</a></li>
                    <li class="list-inline-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/business', 'lang'=>Yii::app()->language)); ?>">Business Opportunity</a></li>
                    <li class="list-inline-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/gema', 'lang'=>Yii::app()->language)); ?>">Impact</a></li>
                    <li class="list-inline-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/business', 'lang'=>Yii::app()->language)); ?>#block-faq">FAQ</a></li>
                    <li class="list-inline-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/career', 'lang'=>Yii::app()->language)); ?>">Career</a></li>
                    <li class="list-inline-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/contact', 'lang'=>Yii::app()->language)); ?>">Contact Us</a></li>
                </ul>
            </div>
            <div class="col-md-20">
                <ul class="list-inline text-right m-0 ">
                    <li class="list-inline-item first"><span>Social Media</span></li>
                    <?php if ($this->setting['url_facebook'] != ''): ?>
                    <li class="list-inline-item"><a target="_blank" href="<?php echo $this->setting['url_facebook'] ?>"><i class="fa fa-facebook-square"></i></a></li>
                    <?php endif ?>
                    <?php if ($this->setting['url_instagram'] != ''): ?>
                    <li class="list-inline-item"><a target="_blank" href="<?php echo $this->setting['url_instagram'] ?>"><i class="fa fa-instagram"></i></a></li>
                    <?php endif ?>
                    <?php if ($this->setting['url_twitter'] != ''): ?>
                    <li class="list-inline-item"><a target="_blank" href="<?php echo $this->setting['url_twitter'] ?>"><i class="fa fa-twitter"></i></a></li>
                    <?php endif ?>
                    <?php if ($this->setting['url_youtube'] != ''): ?>
                    <li class="list-inline-item"><a target="_blank" href="<?php echo $this->setting['url_youtube'] ?>"><i class="fa fa-youtube"></i></a></li>
                    <?php endif ?>
                </ul>
            </div>
        </div>
        <div class="py-2 mb-2"></div>
        <div class="lines-white"></div>
        <div class="py-2 mb-2"></div>
        <div class="row">
            <div class="col-md-35">
                <ul class="list-inline lgo-certif m-0">
                    <li class="list-inline-item firsts"><img src="<?php echo $this->assetBaseurl; ?>new/lefts_sub-certific.jpg" alt="Bird nest drink - fit with Realfood" class=""></li>
                    <li class="list-inline-item"><img src="<?php echo $this->assetBaseurl; ?>new/lgo_sub_certif_1.jpg" alt="Bird nest drink - fit with Realfood" class="img-fluid"></li>
                    <li class="list-inline-item"><img src="<?php echo $this->assetBaseurl; ?>new/lgo_sub_certif_2.jpg" alt="Bird nest drink - fit with Realfood" class="img-fluid"></li>
                </ul>
            </div>
            <div class="col-md-25">
                <div class="py-2"></div>
                <div class="t-copyrights text-right">
                    <p>&copy; COPYRIGHT REALFOOD WINTA ASIA,  INDONESIA 2019. <br>
                    WEBSITE DESIGN BY <a title="Website Design Surabaya" target="_blank" href="https://www.markdesign.net/">MARKDESIGN</a>.</p>
                </div>
            </div>
        </div>
    </div>
    <div class="py-4"></div>
</section>