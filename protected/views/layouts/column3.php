<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>

<?php echo $this->renderPartial('//layouts/_header', array()); ?>

<!-- Start fcs -->

<div class="fcs-wrapper outers_fcs_wrapper prelatife wrapper-slide faq">
    <!-- <img class="w-100" style="height:100%;" src="<?php echo Yii::app()->baseUrl; ?>/asset/images/new/FAQ_02.jpg" alt="First slide"> -->
    <!-- <img class="w-100 d-block d-sm-none" src="<?php //echo Yii::app()->baseUrl.ImageHelper::thumb(600,980, '/images/slide/'. 'a94ea-fcs-1.jpg' , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="First slide"> -->
    <div id="myCarousel_home" class="carousel carousel-fade" data-ride="carousel" data-interval="4500">
            <div class="carousel-inner">
                <div class="carousel-item home-slider-new active">
                    <img class="w-100 d-none d-sm-block" src="<?php echo Yii::app()->baseUrl; ?>/asset/images/new/FAQ_02.jpg" alt="First slide">
                    <img class="w-100 d-block d-sm-none" src="<?php echo Yii::app()->baseUrl; ?>/asset/images/new/testtest.jpg" alt="First slide">
                    <!-- <img class="w-100 d-block d-sm-none" src="" alt=""> -->
                    <div class="carousel-caption caption-slider-home mx-auto">
                        <div class="prelative container mx-auto">
                            <div class="bxsl_tx_fcs">
                                <div class="row no-gutters">
                                    <div class="col-md-60">
                                        <p>Frequently <br><b>ASKED QUESTIONS</b></p>
                                        <div class="pt-3"></div>
                                        <div class="line-insides"></div>
                                        <div class="subtitle py-4">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris magna est, venenatis eget sodales a, egestas et ligula. </p>
                                            <!-- <div class="pt-5"></div> -->
                                            <div class="pt-5"></div>
                                            <!-- <a href="#">LEARN HOME</a> -->
                                        </div>
                                    </div>
                                    <div class="col-md-27"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- <ol class="carousel-indicators">
                <li data-target="#myCarousel_home" data-slide-to="" class="active"></li>
            </ol> -->
    </div>
</div>
<!-- End fcs -->

<?php echo $content; ?>

<?php echo $this->renderPartial('//layouts/_footer', array()); ?>

<script type="text/javascript">
    $(document).ready(function(){
        
        if ($(window).width() >= 768) {
            var $item = $('#myCarousel_home.carousel .carousel-item'); 
            var $wHeight = $(window).height();
            $item.eq(0).addClass('active');
            $item.height($wHeight); 
            $item.addClass('full-screen');

            $('#myCarousel_home.carousel img.d-none.d-sm-block').each(function() {
              var $src = $(this).attr('src');
              var $color = $(this).attr('data-color');
              $(this).parent().css({
                'background-image' : 'url(' + $src + ')',
                'background-color' : $color
              });
              $(this).remove();
            });

            $(window).on('resize', function (){
              $wHeight = $(window).height();
              $item.height($wHeight);
            });

            $('#myCarousel_home.carousel').carousel({
              interval: 4000,
              pause: "false"
            });
        }else{
            var snmob_height = $('#myCarousel_home.carousel .carousel-item img.w-100.d-block.d-sm-none').height(); 
            $('.outers_fcs_wrapper.fcs-wrapper #myCarousel_home, .fcs-wrapper.outers_fcs_wrapper').css('min-height', snmob_height+"px");
        }

    });
</script>
<?php $this->endContent(); ?>