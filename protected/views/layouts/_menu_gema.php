<?php
$share = Share::model()->findAll();
?>
<div class="box-green">
    <ul class="list-inline">
        <li <?php echo ($active == 'gema')? 'class="active"':''; ?>><a href="<?php echo CHtml::normalizeUrl(array('home/gema', 'tab'=>'index')); ?>">IMPROVING LIVES</a></li>
        <li class="separator"></li>
        <li class="dropdown <?php echo ($active == 'share_vision' || $active == 'share_solution')? 'active':''; ?>">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
            WHAT WE SHARE
          </a>
          <ul class="dropdown-menu">
            <?php foreach ($share as $key => $value): ?>
              <li><a href="<?php echo CHtml::normalizeUrl(array('home/gema', 'tab'=>'share', 'id_m'=>$value->id)); ?>"><?php echo $value->title ?></a></li>
            <?php endforeach ?>
          </ul>
        </li>
        <li class="separator"></li>
        <li <?php echo ($active == 'whre_we_give')? 'class="active"':''; ?>><a href="<?php echo CHtml::normalizeUrl(array('home/gema', 'tab'=>'whre_we_give')); ?>">WHERE WE GIVE</a></li>
        
        <?php /*
        <li class="separator"></li>
        <li <?php echo ($active == 'how_we_start')? 'class="active"':''; ?>><a href="<?php echo CHtml::normalizeUrl(array('home/gema', 'tab'=>'how_we_start')); ?>">HOW WE START</a></li>
         */
         ?>

        <li class="separator"></li>
        <li <?php echo ($active == 'our_next_mission')? 'class="active"':''; ?>><a href="<?php echo CHtml::normalizeUrl(array('home/gema', 'tab'=>'our_next_mission')); ?>">OUR NEXT MISSION</a></li>
    </ul>
</div>