<?php 
    $e_activemenu = $this->action->id;
    $controllers_ac = $this->id;
    $session=new CHttpSession;
    $session->open();
    $login_member = $session['login_member'];

    $active_menu_pg = $controllers_ac.'/'.$e_activemenu;
?>
<!-- <header class="header sticky-top <?php if ($active_menu_pg == 'home/index'): ?>homepages<?php endif ?>">
  <div class="prelative container d-none d-sm-none d-md-block d-lg-block">
  </div>
</header> -->

<header class="head <?php if ($active_menu_pg != 'home/index'): ?>insides-head<?php endif ?> <?php if ($productpg === true): ?>heads-products<?php endif ?>">
  <div class="prelative container container-head">
    <nav class="navbar">
      <div class="col-20">
        <div class="lefts_logo">
          <a class="navbar-brand" href="<?php echo CHtml::normalizeUrl(array('/home/index', 'lang'=>Yii::app()->language)); ?>"><img class="img img-fluid" src="<?php echo Yii::app()->baseUrl; ?>/asset/images/new/logo-realf.png" alt="Bird nest drink - fit with Realfood"></a>
          <?php if ($active_menu_pg == 'home/index'): ?>
          <h1 class="d-none"><?php echo $this->pageTitle ?></h1>
          <?php endif ?>
        </div>
      </div>
      <div class="col-40">
        <?php /*<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
          <a href="javacript:;" class="showmenu_barresponsive">
            <?php if ($active_menu_pg != 'home/produk'): ?>
              <img src="<?php echo Yii::app()->baseUrl; ?>/asset/images/new/navbar.png" alt="Bird nest drink - fit with Realfood">
            <?php endif ?>
            <?php if ($active_menu_pg == 'home/produk'): ?>
              <img src="<?php echo Yii::app()->baseUrl; ?>/asset/images/new/navbar_barr.jpg" alt="Bird nest drink - fit with Realfood">
            <?php endif ?>
          </a>
        </button>*/ ?>
        <div class="blocks_top_menu text-right">
         <div class="py-1 text-right rights_headlanguage">
           <ul class="list-inline menus_language">
             <?php
              $get = $_GET; $get['lang'] = 'en';
              ?>
              <li class="list-inline-item <?php if (Yii::app()->language == 'en'): ?>active<?php endif ?>"><a href="<?php echo $this->createUrl($this->route, $get) ?>" title="ENG" hreflang="en-US">English</a></li>
              <li class="list-inline-item">|</li>
               <?php
                $get = $_GET; $get['lang'] = 'id';
                ?>
              <li class="list-inline-item <?php if (Yii::app()->language == 'id'): ?>active<?php endif ?>"><a href="<?php echo $this->createUrl($this->route, $get) ?>" title="Bahasa" hreflang="id-ID">Bahasa</a></li>
           </ul>
         </div>
         <div class="clear clearfix"></div>
         <ul class="list-inline">
            <li class="list-inline-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/index', 'lang'=>Yii::app()->language)); ?>">Home</a></li>
            <!-- <li class="list-inline-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/about', 'lang'=>Yii::app()->language)); ?>">Our Story</a></li> -->
            <li class="list-inline-item"><a href="<?php echo CHtml::normalizeUrl(array('/product/landing', 'lang'=>Yii::app()->language)); ?>">Products</a></li>
            <li class="list-inline-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/business', 'lang'=>Yii::app()->language)); ?>">Business Opportunity</a></li>
            <li class="list-inline-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/gema', 'lang'=>Yii::app()->language)); ?>">Impact</a></li>
            <li class="list-inline-item"><a href="<?php echo CHtml::normalizeUrl(array('/blog/indexs', 'lang'=>Yii::app()->language)); ?>">Blog</a></li>
            <li class="list-inline-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/contact', 'lang'=>Yii::app()->language)); ?>">Contact Us</a></li>
          </ul>
        </div>
      </div>
      <div class="collapse navbar-collapse" id="navbarCollapse">
        <!-- <ul class="navbar-nav mr-auto">
          <li class="nav-item active">
            <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">Link</a>
          </li>
          <li class="nav-item">
            <a class="nav-link disabled" href="#">Disabled</a>
          </li>
        </ul> -->
      </div>
    </nav>
  </div>
</header>


<section id="myAffix" class="header-affixs affix-top">
  <!-- <div class="clear height-15"></div> -->
  <div class="prelative container">
    <div class="row">
      <div class="col-md-15 col-sm-15">
        <div class="lgo_web_headrs_wb">
          <a href="<?php echo CHtml::normalizeUrl(array('/home/index', 'lang'=>Yii::app()->language)); ?>">
            <img src="<?php echo Yii::app()->baseUrl; ?>/asset/images/new/logo-realf.png" alt="Bird nest drink - fit with Realfood" class="img img-fluid">
          </a>
        </div>
      </div>
      <div class="col-md-45 col-sm-45">
        <div class="text-right"> 
          <div class="menu-taffix">
            <ul class="list-inline d-inline">
              <li class="list-inline-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/index', 'lang'=>Yii::app()->language)); ?>">Home</a></li>
              <!-- <li class="list-inline-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/about', 'lang'=>Yii::app()->language)); ?>">Our Story</a></li> -->
              <li class="list-inline-item"><a href="<?php echo CHtml::normalizeUrl(array('/product/landing', 'lang'=>Yii::app()->language)); ?>">Products</a></li>
              <li class="list-inline-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/business', 'lang'=>Yii::app()->language)); ?>">Business Opportunity</a></li>
              <li class="list-inline-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/gema', 'lang'=>Yii::app()->language)); ?>">Impact</a></li>
              <!-- <li class="list-inline-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/career', 'lang'=>Yii::app()->language)); ?>">Career</a></li> -->
              <li class="list-inline-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/contact', 'lang'=>Yii::app()->language)); ?>">Contact Us</a></li>
            </ul>
            <ul class="list-inline d-inline smalls_affix_lang">
              <?php
              $get = $_GET; $get['lang'] = 'en';
              ?>
              <li class="list-inline-item <?php if (Yii::app()->language == 'en'): ?>active<?php endif ?>"><a href="<?php echo $this->createUrl($this->route, $get) ?>" title="ENG" hreflang="en-US">English</a></li>
              <li class="list-inline-item">|</li>
               <?php
                $get = $_GET; $get['lang'] = 'id';
                ?>
              <li class="list-inline-item <?php if (Yii::app()->language == 'id'): ?>active<?php endif ?>"><a href="<?php echo $this->createUrl($this->route, $get) ?>" title="Bahasa" hreflang="id-ID">Bahasa</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <div class="clear"></div>
  </div>
</section>

<header class="header-mobile homepage_head">
  <nav class="navbar navbar-expand-lg navbar-light fixed-top">
  <a class="navbar-brand" href="<?php echo CHtml::normalizeUrl(array('/home/index', 'lang'=>Yii::app()->language)); ?>"><img src="<?php echo Yii::app()->baseUrl; ?>/asset/images/new/logo-realf.png" alt="Bird nest drink - fit with Realfood" class="img img-fluid"></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
    <li class="nav-item"><a class="nav-link" href="<?php echo CHtml::normalizeUrl(array('/home/index', 'lang'=>Yii::app()->language)); ?>">Home</a></li>
    <!-- <li class="nav-item"><a class=nav-link" href="<?php echo CHtml::normalizeUrl(array('/home/about', 'lang'=>Yii::app()->language)); ?>">Our Story</a></li> -->
    <li class="nav-item"><a class="nav-link" href="<?php echo CHtml::normalizeUrl(array('/product/landing', 'lang'=>Yii::app()->language)); ?>">Products</a></li>
    <li class="nav-item"><a class="nav-link" href="<?php echo CHtml::normalizeUrl(array('/home/business', 'lang'=>Yii::app()->language)); ?>">Business Opportunity</a></li>
    <li class="nav-item"><a class="nav-link" href="<?php echo CHtml::normalizeUrl(array('/home/gema', 'lang'=>Yii::app()->language)); ?>">Impact</a></li>
    <!-- <li class="nav-item"><a class=nav-link" href="<?php echo CHtml::normalizeUrl(array('/home/career', 'lang'=>Yii::app()->language)); ?>">Career</a></li> -->
    <li class="nav-item"><a class="nav-link" href="<?php echo CHtml::normalizeUrl(array('/home/contact', 'lang'=>Yii::app()->language)); ?>">Contact Us</a></li>
    </ul>
    <div class="py-1"></div>
    <ul class="list-inline">
    <?php
    $get = $_GET; $get['lang'] = 'en';
    ?>
    <li class="list-inline-item <?php if (Yii::app()->language == 'en'): ?>active<?php endif ?>"><a href="<?php echo $this->createUrl($this->route, $get) ?>" title="ENG" hreflang="en-US">English</a></li>
    <li class="list-inline-item">|</li>
     <?php
      $get = $_GET; $get['lang'] = 'id';
      ?>
    <li class="list-inline-item <?php if (Yii::app()->language == 'id'): ?>active<?php endif ?>"><a href="<?php echo $this->createUrl($this->route, $get) ?>" title="Bahasa" hreflang="id-ID">Bahasa</a></li>
    </ul>
  </div>
  </nav>
</header>

<div class="outer-blok-black-menuresponss-hides" style="overflow: hidden;">
	<div class="prelatife container">
		<div class="clear height-45"></div>
		<div class="fright">
			<div class="hidesmenu-frightd"><a href="#" class="closemrespobtn"><img src="<?php echo Yii::app()->baseUrl; ?>/asset/images/new/closen-btn (1).png" alt="Bird nest drink - fit with Realfood"></a></div>
		</div>
		<div class="blocksn_logo-centers">
			<img src="<?php echo Yii::app()->baseUrl; ?>/asset/images/new/realfood-header.png" alt="logo chendracahyadi" class="img-responsive center-block">
		</div>
		<div class="height-30 hidden-xs"></div>
		<div class="menu-sheader-datals">
			<ul class="list-unstyled">
				<li class="list-inline-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/index', 'lang'=>Yii::app()->language)); ?>">Home</a></li>
        <li class="separate"></li>
        <li class="list-inline-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/about', 'lang'=>Yii::app()->language)); ?>">Our Story</a></li>
        <li class="separate"></li>
        <li class="list-inline-item"><a href="<?php echo CHtml::normalizeUrl(array('/product/landing', 'lang'=>Yii::app()->language)); ?>">Products</a></li>
        <li class="separate"></li>
        <li class="list-inline-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/business', 'lang'=>Yii::app()->language)); ?>">Business Opportunity</a></li>
        <li class="separate"></li>
        <li class="list-inline-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/gema', 'lang'=>Yii::app()->language)); ?>">Impact</a></li>
        <li class="separate"></li>
        <li class="list-inline-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/career', 'lang'=>Yii::app()->language)); ?>">Career</a></li>
        <li class="separate"></li>
        <li class="list-inline-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/contact', 'lang'=>Yii::app()->language)); ?>">Contact Us</a></li>
			</ul>
		</div>

		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>

<script type="text/javascript">
	$(function(){
		// show and hide menu responsive
		$('a.showmenu_barresponsive').on('click', function() {
			$('.outer-blok-black-menuresponss-hides').slideToggle('slow');
			return false;
		});
		$('a.closemrespobtn').on('click', function() {
			$('.outer-blok-black-menuresponss-hides').slideUp('slow');
			return false;
		});

	})
</script>


<script type="text/javascript">
  $(document).ready(function() {
  var sn_width = $(window).width();
  if (sn_width > 1150) {
      $('html, body').scroll( function(){

        var sntop1 = $(this).scrollTop();
        
        if(sntop1 > 115){
          $('.header-affixs').removeClass('affix-top').addClass('affix');
        }else{
          $('.header-affixs.affix').removeClass('affix').addClass('affix-top');
        }
      });

    }
  });

  // $('html, body').ready(function() {

  //   $('html, body').scroll(function() {
  //     console.log('Scrolled to ' + $(this).scrollTop());
  //   })

  // });

</script>


