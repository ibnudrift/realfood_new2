<section class="back-white block-outers-product-category py-5">
	<div class="prelative container">
		<div class="inner-section py-5 text-left content-text">
			<div class="middles-inner outer_blocks_detail_products">

				<div class="tops pb-5 text-center">
					<h2 class="title-page-products">BLOG / ARTICLES</h2>
				</div>
				
				<div class="py-2"></div>
				<div class="middles-productsec py-2">
					<div class="tops_filters_category pb-5 blogs text-center">
						<ul class="list-inline filter_cat justify-content-center">
						  <li class="list-inline-item <?php if (!isset($_GET['tags']) or $_GET['tags'] == 1): ?>active<?php endif ?>"><a href="<?php echo CHtml::normalizeUrl(array('/blog/indexs', 'tags'=>1)); ?>">ARTICLES</a></li>
						  <li class="list-inline-item <?php if (isset($_GET['tags']) AND $_GET['tags'] == 2): ?>active<?php endif ?>"><a href="<?php echo CHtml::normalizeUrl(array('/blog/indexs', 'tags'=>2)); ?>">TIPS</a></li>
						</ul>
					</div>

					<div class="py-3"></div>
					<div class="lists-blogs-item text-center">
						<div class="row">

							<?php foreach ($data->getData() as $key => $value): ?>
							<div class="col-md-20">
								<div class="items pb-5 mb-3">
									<div class="picture">
										<a href="<?php echo CHtml::normalizeUrl(array('/blog/details', 'id'=> $value->id, 'slug'=> Slug::Create($value->description->title), 'lang'=>Yii::app()->language)); ?>">
											<img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(397, 318, '/images/blog/'.$value->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img-fluid d-block mx-auto">
										</a>
									</div>
									<div class="info pt-4">
										<h6 class="title-prouct"><?php echo $value->description->title ?></h6>
										<div class="py-3"></div>
										<p><?php echo substr(strip_tags($value->description->content), 0, 60) ?></p>
										<div class="py-2"></div>
										<a href="<?php echo CHtml::normalizeUrl(array('/blog/details', 'id'=> $value->id, 'lang'=>Yii::app()->language)); ?>" class="btn btn-defaults_set">VIEW DETAILS</a>
									</div>
								</div>
							</div>
							<?php endforeach ?>

						</div>
						<div class="py-2"></div>
				        <div class="textaboveheader-landing page d-block py-4 text-center mx-auto">
				            <nav aria-label="Page navigation example" class="nav_pagin">
				             <?php 
				                 $this->widget('CLinkPager', array(
				                    'pages' => $data->getPagination(),
				                    'header'=>'',
				                    'footer'=>'',
				                    'lastPageCssClass' => 'd-none',
				                    'firstPageCssClass' => 'd-none',
				                    'nextPageCssClass' => 'd-none',
				                    'previousPageCssClass' => 'd-none',
				                    'itemCount'=> $dataBlog->totalItemCount,
				                    'htmlOptions'=>array('class'=>'pagination m-0 justify-content-center'),
				                    'selectedPageCssClass'=>'active',
				                ));
				             ?>
				         </nav>
				        </div>
						<div class="clear clearfix"></div>
					</div>
					
				</div>

				<div class="clear"></div>
			</div>


			<div class="clear clearfix"></div>
		</div>
		<!-- End inner section -->
		<div class="clear"></div>
	</div>
</section>