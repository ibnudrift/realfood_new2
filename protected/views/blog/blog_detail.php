<section class="back-white block-outers-product-category py-5">
	<div class="prelative container">
		<div class="inner-section py-5 text-left content-text">
			<div class="middles-inner outer_blocks_detail_products">

				<div class="tops pb-5 text-center tops_blog">
					<h2 class="title-page-products">BLOG / ARTICLES</h2>
					<div class="clear"></div>
					<div class="lines-grey d-block mx-auto"></div>
				</div>
				
				<div class="py-4"></div>
				<div class="middles-productsec py-2">
					<div class="inner-blog-details">
						<div class="n-top-titles">
							<div class="row">
								<div class="col-md-47">
									<span class="dates"><i class="fa fa-calendar"></i> <?php echo date('d F Y', strtotime($model->date_input)); ?></span>
									<div class="py-2"></div>
									<h1><?php echo $model->desc->title; ?></h1>
								</div>
								<div class="col-md-13">
									<div class="text-right backs_blog">
										<a href="<?php echo CHtml::normalizeUrl(array('/blog/indexs', 'lang'=>Yii::app()->language)); ?>" class="btn btn-link"><i class="fa fa-chevron-left"></i> Back</a>
									</div>
								</div>
							</div>
						</div>
						<div class="pictures">
							<img src="<?php echo Yii::app()->baseUrl.'/images/blog/'. $model->image; ?>" alt="" class="img img-fluid w-100">
						</div>
						<div class="py-3"></div>
						
						<?php echo $model->desc->content; ?>
						<div class="clear"></div>
					</div>
					<div class="py-4 my-2"></div>

					<?php if ( count($blogs) > 0 ): ?>
					<div class="outers_other_blog">
						<h4 class="other-title">OTHER BLOGS</h4>
						<div class="py-3"></div>

							<div class="lists-blogs-item text-center">
							<div class="row">
								<?php foreach ($blogs as $key => $value): ?>
								<div class="col-md-20">
									<div class="items pb-5 mb-3">
										<div class="picture">
											<a href="<?php echo CHtml::normalizeUrl(array('/blog/details', 'id'=> $value->id)); ?>">
												<img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(397, 318, '/images/blog/'.$value->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img-fluid d-block mx-auto">
											</a>
										</div>
										<div class="info pt-4">
											<h6 class="title-prouct"><?php echo $value->desc->title ?></h6>
											<div class="py-3"></div>
											<p><?php echo substr(strip_tags($value->desc->content), 0, 60) ?></p>
											<div class="py-2"></div>
											<a href="<?php echo CHtml::normalizeUrl(array('/blog/details', 'id'=> $value->id, 'slug'=> Slug::Create($value->description->title), 'lang'=>Yii::app()->language)); ?>" class="btn btn-defaults_set">VIEW DETAILS</a>
										</div>
									</div>
								</div>
								<?php endforeach ?>
							</div>
							<div class="clear clearfix"></div>
						</div>

						<div class="clear"></div>
					</div>
					<?php endif ?>

					<div class="clear"></div>
				</div>

				<div class="clear"></div>
			</div>


			<div class="clear clearfix"></div>
		</div>
		<!-- End inner section -->
		<div class="clear"></div>
	</div>
</section>