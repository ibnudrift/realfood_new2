<section class="full-banner-top">
    <div class="tops-cont-insidepage back-pgema" style="background: none; height: auto; position: relative;"> 
         <div class="pict-fulls hidden-xs">
            <?php
            $slide = ViewSlide::model()->findAll('language_id = :language_id AND topik_id = 1 ORDER BY sort ASC', array(':language_id'=>$this->languageID));
            ?>
            <div id="carousel-examp-topPage" class="carousel fade" data-ride="carousel">
              <div class="carousel-inner" role="listbox">
                <?php foreach ($slide as $ke => $value): ?>
                <div class="item <?php if ($ke == 0): ?>active<?php endif ?>">
                  <!-- <img src="<?php // echo Yii::app()->baseUrl.ImageHelper::thumb(1450,680, '/images/slide/'.$value->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="<?php echo $value->title; ?>"> -->
                  <div class="fill" style="background-image:url(<?php echo Yii::app()->baseUrl.ImageHelper::thumb(1450,680, '/images/slide/'.$value->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>);"></div>
                </div>
                <?php endforeach ?>
              </div>
            </div>
            <div class="clear"></div>
        </div>
        <div class="visible-xs">
          <div id="carousel_ex_phone" class="carousel fade" data-ride="carousel">
              <div class="carousel-inner" role="listbox">
                <?php foreach ($slide as $ke => $value): ?>
                <div class="item <?php if ($ke == 0): ?>active<?php endif ?>">
                  <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(1450,680, '/images/slide/'.$value->image2 , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="<?php echo $value->title; ?>">                  
                </div>
                <?php endforeach ?>
              </div>
            </div>
          <div class="clear"></div>
        </div>
        <?php
        /*
        <div class="pabs-ins-middle-topim">
            <div class="tengah insd-container mw930 text-center content-up visible-lg">
                <div class="visible-lg">
                    <div class="clear height-50"></div>
                    <div class="height-50"></div>
                    <div class="height-50"></div>
                    <div class="height-25"></div>
                </div>
                  <?php if ($this->setting['career_hide'] == 0): ?>
                  <h1 class="title-pages"><?php echo $this->setting['career_title'] ?></h1> <div class="clear height-10"></div>
                  <div class="lines-chld-bgrey tengah"></div> <div class="clear height-25"></div>
                  <?php endif ?>
                  <!-- <span class="bigs">EMPOWER THE GENERATION</span> <div class="clear"></div> -->
                  <div class="insd-container tengah text-center">
                    <p><?php echo nl2br($this->setting['career_content']) ?></p>
                  </div>
                  <div class="clear"></div>
            </div>
        </div>*/ ?>
    </div> 
</section>
<section class="outer-inside-middle-content back-white">
  <div class="prelatife container">
    <?php
    /*
    <?php if ($this->setting['about_header_hide'] == 0 || $this->setting['about_header_title'] != ''): ?>
    <div class='pindahan_text-heroimage hidden-lg'>
         <?php if ($this->setting['about_header_hide'] == 0): ?>
          <h1 class="title-pages">ABOUT</h1> <div class="clear height-10"></div>
          <div class="lines-chld-bgrey tengah"></div> <div class="clear height-25"></div>
          <?php endif ?>
          <?php if ($this->setting['about_header_title'] != ''): ?>
          <span class="bigs"><?php echo $this->setting['about_header_title'] ?></span> <div class="clear"></div>
          <?php endif ?>
          <?php if ($this->setting['about_header_subtitle'] != ''): ?>
          <p><?php echo nl2br($this->setting['about_header_subtitle']) ?></p>
          <?php endif ?>
          <div class="clear"></div>
    </div>
    <?php endif ?>
    */ ?>
   
    <div class="clear"></div>
  </div>

  <div class="back-grey mh500">
    <div class="prelatife container">
        <div class="clear height-50 hidden-xs"></div>
        <div class="clear height-20 visible-xs"></div>
        <div class="outers-cont-bottom-abouts">
          <div class="topscont text-center">
            <ul class="list-inline">
              <li><a class="ajaxCall firstChik" href="javascript:;" data-id="1"><?php echo $this->setting['about_whoweare_menu'] ?></a></li>
              <!-- <li><a class="ajaxCall" href="javascript:;" data-id="2">our team</a></li> -->
              <li><a class="ajaxCall" href="javascript:;" data-id="3"><?php echo $this->setting['about_visimisi_menu'] ?></a></li>
              <li><a class="ajaxCall" href="javascript:;" data-id="4"><?php echo $this->setting['about_workwithus_menu'] ?></a></li>
            </ul>
          </div>
          <div class="clear height-50 hidden-xs"></div>
          <div class="clear height-0 visible-xs"></div>
          <div class="height-35"></div>
          <div class="middles">
            
            <div class="mw920 tengah">
                <div class="ajax_content data-1">
                  <h2 class="text-center"><?php echo $this->setting['about_whoweare_title'] ?></h2> <div class="clear height-30"></div>
                  <?php echo $this->setting['about_whoweare_content'] ?>
                </div>
                
               <!--  <div class="ajax_content data-2">
                  <h2><?php echo $this->setting['about_ourteam_title'] ?></h2> <div class="clear height-30"></div>
                  <?php echo $this->setting['about_ourteam_content'] ?>
                </div> -->
                <div class="ajax_content data-3">
                  <h2 class="text-center"><?php echo $this->setting['about_visimisi_title'] ?></h2> <div class="clear height-30"></div>
                  <div class="hidden">
                    <p>To bring real smile, real life, and real change to the world around us.</p>
                    <p><strong style="color: #276f46;">Real Smile</strong>:<br />
                    Striving to create happiness for everyone in the business cycle from the employees, suppliers, distributors to consumers</p>
                    <p><strong style="color: #276f46;">Real Life</strong>:<br />
                    Leading &amp; empowering people to live a healthy life.</p>
                    <p><strong style="color: #276f46;">Real Change</strong>:<br />
                    Making impacts and differences in someone else&rsquo;s life through our products.</p>
                    <div class="clear height-10"></div>
                    <div class="row mw500">
                      <div class="col-sm-6">
                        <p><strong style="color: #276f46;">Core Values:</strong><br />
                            a.  Integrity  <br>
                            b.  Generosity <br>
                            c.  Compassion <br>
                            d.  Togetherness <br>
                            e.  Excellence</p>
                      </div>
                      <div class="col-sm-6">
                        <p><strong style="color: #276f46;">Company Cultures:</strong>:<br />
                          f.  Dun <br>
                          g.  Creative <br>
                          h.  Direct <br>
                          i.  Open
                          </p>
                      </div>
                    </div>
                    <div class="clear height-30"></div>
                    <p><b style="color: #276f46;">Perspective</b></p>
                    <h4 class="green">What we don’t believe</h4>
                    <p class="black-btm"><strong>Profit is everything</strong><br />
                    &ldquo;Success isn&rsquo;t about how much money a company make, but it&rsquo;s about the difference a company make in people&rsquo;s lives&rdquo;</p>

                    <p class="black-btm"><strong>Maximizing shareholder value is the most important thing</strong><br />
                    &ldquo; Creating value for all stakeholders is the most important priority for the company. The value of customers, suppliers, employees, shareholder, and community has to be maximized&rdquo;</p>

                    <div class="clear height-20"></div>
                    <h4 class="green">What we believe & embrace</h4>
                    <p class="black-btm"><strong>Business should make a positive impact.</strong><br />
                    &ldquo;The reason why we start the business is to make meaning; to create a product to make the world a better place&rdquo;</p>

                    <p class="black-btm"><strong>Giving is good for companies</strong><br />
                    &ldquo;Goodness is the only investment which never fails&rdquo;</p>

                    <p class="black-btm"><strong>We are more than just a company</strong><br />
                    &ldquo;Our company is more than just an office. It is more than just a culture. It is more than just an organization. We are a movement of the young generations.&rdquo;</p>


                    <div class="clear height-50"></div><div class="height-5"></div>
                    </div>
                  
                  <?php echo $this->setting['about_visimisi_content'] ?>
                </div>
                <div class="ajax_content data-4">
                  <h2 class="text-center"><?php echo $this->setting['about_workwithus_title'] ?></h2> <div class="clear height-30"></div>
                  <div class="hidden">
                    <p>Sarang burung walet adalah ingredients asli Indonesia yang tidak banyak diketahui oleh orang. Sebagai ingredients asli Indonesia, sarang burung memiliki benefit yang sangat luar biasa dari segi kesehatan dan historical value. Sarang burung walet awalnya hanya dikonsumsi oleh raja-raja china dan keluarga bangsawan china yang dipercaya dapat memerpenjang umur. Selama beribu-ribu tahun sarnag burung walet sudah dijadikan tradisi oleh kerajaan china. Sbw sendiri memiliki content kesehatan yang luar biasa dimana di dalamnya mengandung siliac acid, antioxidant, dan glycoprotein yang baik untuk kesehatan kulit, paru-paru, ginjal, dan kesehatan organ dalam lainnya. Hal itulah yang membuat sarang burung walet sangat digemari oleh keluarga kerajaan dan bangsawan china karena dipercaya ratusan tahun dapat memperpanjang umur. Kami di Realfood memiliki tujuan untuk mengenalkan ulang ingredient asli Indonesia yang jarang didengar ini melalui pengenalan lifestyle sehat diawali dari generasi muda kita.</p>
                    <div class="row">
                      <div class="col-sm-6">
                        <p style="padding: 0 30px;"><strong style="color: #276f46;">Stevia</strong><br />
                        Stevia adalah pemanis alami yang mengandung kalori yang sangat rendah dibandingkan dengan gula, yang jauh lebih sehat dan dapat digunakan oleh kalangan manapun.</p>
                      </div>
                      <div class="col-sm-6">
                        <p style="padding: 0 18px;"><strong style="color: #276f46;">Realfit</strong><br />
                          Realfit adalah minuman kesehatan yang berbahan dasar sarang burung walet Indonesia yang memiliki kandungan glycoprotein, siliac acid, antioxidant yang baik untuk anak kecil, remaja, dan orang dewasa. RealFit dimaniskan oleh gula asli dan daun ekstrak stevia yang rendah calorie. RealFit siap menjadi pendamping lifestyle anda untuk mencapai hidup yang baik. Minuman yang bukan saja baik untuk tubuh kita namun juga memberikan dampak positif terhadap orang yang kurang beruntung. (dielaborate)</p>
                      </div>
                    </div>
                  </div>

                  <!-- <p>&nbsp;</p> -->

                  <?php echo $this->setting['about_workwithus_content'] ?>
                </div>

                <!-- <div class="clear height-35"></div> -->
                <div class="clear height-5"></div>
            <?php
                $image = AboutImage::model()->findAll();
                ?>
                <?php if ($image): ?>
                  <?php foreach ($image as $key => $value): ?>
                      <div class="ill-abouts"><img src="<?php echo Yii::app()->baseUrl; ?>/images/about/<?php echo $value->image ?>" alt="" class="img-responsive"></div>
                      <div class="clear height-35"></div>
                      <div class="height-5"></div>
                  <?php endforeach ?>  
                <?php endif ?>

                <div class="shares-text" style="padding: 0 60px;">
                  <span class="inline-t">SHARE</span>&nbsp;&nbsp; / &nbsp;&nbsp;<a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode(Yii::app()->request->hostInfo.Yii::app()->request->url) ?>">FACEBOOK</a>&nbsp;&nbsp; / &nbsp;&nbsp;<a target="_blank" href="https://plus.google.com/share?url=<?php echo urlencode(Yii::app()->request->hostInfo.Yii::app()->request->url) ?>">GOOGLE PLUS</a>&nbsp;&nbsp; / &nbsp;&nbsp;<a target="_blank" href="https://twitter.com/home?status=<?php echo urlencode(Yii::app()->request->hostInfo.Yii::app()->request->url) ?>">TWITTER</a>
                </div>

                <div class="clear"></div>
            </div>

            <div class="clear"></div>
          </div>

          <div class="clear"></div>
        </div>
      
      <div class="clear"></div> 
      <!-- <div class="height-50"></div><div class="height-20"></div> -->
    </div>
  </div>
</section>
<script type="text/javascript">
      function getCAjax(urlmy){
          $.ajax({
              url: urlmy,
              // data: data,
              dataType: 'html',
              type: 'get',
              success: function(msg) {
                        alert(msg);
                    return false;
              },
              error: function (msg) {
                      console.log(msg);
              },
          });
      }

  $(document).ready(function() {
    // $('.ajaxCall').live('click', function(){
    //       getCAjax( $(this).attr('href') );
    //       return false;
    //  });

     // cepet cepatan
     // sementara hide
     $('.ajaxCall').live('click', function(){
          $('.ajax_content').addClass('hidden');
          $('.ajax_content').addClass('hidden');
          $('.topscont ul li a').removeClass('active');
          $(this).addClass('active');

          var dataActive = $(this).attr('data-id');
          $('.data-'+dataActive).removeClass('hidden');
          return false;
     });
    });

    // sementara hide
     $(window).load(function(){
        $('.ajax_content').addClass('hidden');
        $('.firstChik').addClass('active');

        $('.data-1').removeClass('hidden');
     });
</script>
<style type="text/css">
  .ajax_content.data-4 h5,
  .ajax_content.data-4 p{
      text-align: left;
  }
  .ajax_content.data-4 h5{ padding: 0px 60px; }
</style>