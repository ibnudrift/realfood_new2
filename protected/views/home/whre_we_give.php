<section class="outer-inside-middle-content back-white PageGema_section">
  <div class="prelatife container">
    <div class="tops-cont-insidepage back-pgema" style="background: none; height: auto; position: relative;">

      <div class="pict-fulls">
            <?php
            // $slide = ViewSlide::model()->findAll('language_id = :language_id AND topik_id = 4 ORDER BY sort ASC', array(':language_id'=>$this->languageID));
            ?>
            <div id="carousel-examp-topPage" class="carousel slide" data-ride="carousel">
              <div class="carousel-inner" role="listbox">
                <?php // foreach ($slide as $ke => $value): ?>
                <div class="item <?php if ($ke == 0): ?>active<?php endif ?>">
                  <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(1450,680, '/asset/images/gema/hero-pict-where-we-give.jpg' , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="<?php echo $value->title; ?>">
                </div>
                <?php // endforeach ?>
              </div>
            </div>

        </div>

      <div class="pabs-ins-middle-topim">
        <div class="tengah insd-container mw930 text-center content-up hidden-xs">
          <div class="hidden-xs">
              <?php // if ($this->setting['gema_hide'] == 0): ?>
              <h1 class="title-pages white">GE-MA</h1> <div class="clear height-10"></div>
              <div class="lines-chld-bgrey tengah white"></div>
              <div class="clear height-25"></div>
              <?php if ($text_hero): ?>
                <span class="bigs white"><?php echo $text_hero ?></span> 
              <?php endif ?>
              <div class="clear"></div>
              <?php if ($text_hero2): ?>
              <span class="subs-bigs"><?php echo $text_hero2 ?></span>
              <?php endif ?>
              <div class="clear"></div>
          </div>
        </div>
        <div class="clear"></div>
      </div>
    </div>
    <div class='pindahan_text-heroimage visible-xs'>
         <h1 class="title-pages">GE-MA</h1> <div class="clear height-10"></div>
          <div class="lines-chld-bgrey tengah"></div>
          <div class="clear height-25"></div>
          <?php if ($text_hero): ?>
            <span class="bigs white"><?php echo $text_hero ?></span> 
          <?php endif ?>
          <div class="clear"></div>
          <?php if ($text_hero2): ?>
          <span class="subs-bigs"><?php echo $text_hero2 ?></span>
          <?php endif ?>
          <div class="clear"></div>
    </div>

        <div class="clear"></div>
      </div>
      <div class="prelatife">
        <div class="sub-headerData-menu">
            <div class="prelatife mw-995 tengah">
              <?php echo $this->renderPartial('//layouts/_menu_gema', array('active'=>$active)); ?>
              <div class="box-shadow"></div>
              <div class="clear"></div>
            </div>
        </div>

        <section class="section_1 default-section-gema prelatife custom_section_whereGive">
            <div class="left_cont prelatife">
                <div class="pict-full"><img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(1300,750, '/images/static/'.$this->setting['give_image'] , array('method' => 'resize', 'quality' => '90')) ?>" alt=""></div>

                <div class="pos-abs-full">
                  <div class="table-out prelatife">
                    <div class="table-in">
                        <div class="prelatife container gema_container">
                          <div class="row">
                            <div class="col-md-12">
                              <div class="content-txt text-center grey tengah tx-where_we_give">
                                <img style="max-width:300px;" src="<?php echo $this->assetBaseurl ?>gema/pict-grey-wecurrently.png" alt="" class="pc-center img-responsive">
                                <div class="clear height-40"></div>
                                <span class="t-big1"><?php echo $this->setting['give_current'] ?></span>
                                <div class="clear"></div>
                                <img style="max-width:117px; max-height:77px;" src="<?php echo $this->assetBaseurl ?>gema/pict-t-kids-grey.png" alt="" class="pc-center img-responsive">
                                <div class="clear"></div>
                                <img style="max-width:300px;" src="<?php echo $this->assetBaseurl ?>gema/link-in-gema-where-give.png" alt="" class="pc-center img-responsive">
                                <div class="clear height-25"></div>
                                <span class="t-big1"><?php echo $this->setting['give_in'] ?></span>
                                <div class="clear"></div>
                                <img style="max-width:300px;" src="<?php echo $this->assetBaseurl ?>gema/pict-in-villages-bojonegoro.png" alt="" class="pc-center img-responsive">

                                <div class="celar"></div>
                              </div>
                            </div>
                          </div>
                          <div class="clear"></div>
                        </div>
                      <div class="clear"></div>
                    </div>
                  </div>

                  <div class="clear"></div>
                </div>
                <!-- ends pos abs -->
            </div>
        </section>
        <div class="clear"></div>
        <section class="default-section-gema prelatife">
            <div class="left_cont prelatife">
                <div class="pict-full"><img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(1300,750, '/images/static/'.$this->setting['give_map'] , array('method' => 'resize', 'quality' => '90')) ?>" alt=""></div>
            </div>
        </section>
        <div class="clear"></div>
      </div>
  <!-- </div> -->
</section>

<script type="text/javascript">
    $(document).ready(function(){
        var wbody = $(window).width();

        var heightHero = $('#carousel-examp-topPage img').height();
        if (wbody >= 768){
            $('.PageGema_section .pabs-ins-middle-topim').css("height", heightHero+"px");
        };

         // set w
         if (wbody > 1180){
            var hsec1 = $('.section_1 img').height();
            $('.section_1 .table-out').css("height", hsec1+"px");
          }
    });

    $(window).load(function(){
        var wbody = $(window).width();

        var heightHero = $('#carousel-examp-topPage img').height();
        if (wbody >= 768){
            $('.PageGema_section .pabs-ins-middle-topim').css("height", heightHero+"px");
        };

         // set w
         if (wbody > 1180){
            var hsec1 = $('.section_1 img').height();
            $('.section_1 .table-out').css("height", hsec1+"px");
        }
    });
</script>