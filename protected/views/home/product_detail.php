<section class="middle-content back-white back-birds-ontheback min-h500">
    <div class="prelatife container">
        <div class="clear height-50"></div><div class="height-50"></div><div class="height-35"></div>
        <div class="insides-cont text-left content-text">
            <h1 class="title-pages text-center">PRODUCT</h1>
            <div class="clear height-5"></div><div class="height-4"></div>
            <div class="lines-back-greycenter-w"></div>
            <div class="clear height-45"></div>
            <div class="row">
                <div class="col-md-12">
                    <div class="breadcrumbs">
                        <ol class="breadcrumb">
                          <li><a href="#">Home</a></li>
                          <li><a href="#">Products</a></li>
                          <li><a href="#">Dining and Bar Chairs</a></li>
                          <li><a href="#">Monterey Dining Chairs</a></li>
                          <li class="active">Monterey Dining Armchair</li>
                        </ol>
                    </div>
                </div>
            </div>
            <div class="clear height-45"></div>
            <div class="row">
                <div class="col-md-3 col-lg-3">
                    
                    <div class="left-menu-category">
                        <ul class="list-unstyled">
                            <li><a href="#"><b>Deep Seating Collections</b></a></li>
                            <li><a href="#">Bella Collection</a></li>
                            <li><a href="#">Ciera Collection</a></li>
                            <li><a href="#">Monterey Collection</a></li>
                        </ul>

                        <ul class="list-unstyled">
                            <li><a href="#"><b>Dining and Bar Chairs</b></a></li>
                            <li><a href="#">Bella Dining Chairs</a></li>
                            <li><a href="#">Braxton Folding Chairs</a></li>
                            <li><a href="#">Monterey Dining Chairs</a></li>
                            <li><a href="#">Monterey Bar Chairs</a></li>
                            <li><a href="#">Riviera Folding Sling Chairs</a></li>
                            <li><a href="#">Riviera Stacking Sling Chairs</a></li>
                            <li><a href="#">Sedona Stacking Chairs</a></li>
                            <li><a href="#">SoHo Stacking and Counter Height Chairs</a></li>
                            <li><a href="#">Vienna Dining Chair</a></li>
                        </ul>

                        <ul class="list-unstyled">
                            <li><a href="#"><b>Dining and Bar Tables</b></a></li>
                            <li><a href="#">Brunswick Square and Rectangle Tables</a></li>
                            <li><a href="#">Cambridge Round Folding Cafe Tables</a></li>
                            <li><a href="#">Camden Oval Extension and Fixed Tables</a></li>
                            <li><a href="#">Chelsea Round Extension Table</a></li>
                            <li><a href="#">Chelsea Oval and Rectangular Extension Tables</a></li>
                            <li><a href="#">Newport Square Dining Table</a></li>
                            <li><a href="#">Oxford Round Dining and Bar Tables</a></li>
                            <li><a href="#">Shelburne Dining Tables</a></li>
                            <li><a href="#">SoHo Dining, Extension and Counter Height Tables</a></li>
                        </ul>

                        <ul class="list-unstyled">
                            <li><a href="#"><b>Benches</b></a></li>
                            <li><a href="#">Charleston Backless Benches</a></li>
                            <li><a href="#">Classic Benches and Armchair</a></li>
                            <li><a href="#">Victoria Garden Benches, Armchair and Swing</a></li>
                        </ul>

                        <ul class="list-unstyled">
                            <li><a href="#"><b>Loungers and Occasional Seating</b></a></li>
                            <li><a href="#">Classic Lounger</a></li>
                            <li><a href="#">Riviera Sling Lounger</a></li>
                            <li><a href="#">Adirondack Chair and Footstool</a></li>
                            <li><a href="#">Brittany Rocker</a></li>
                        </ul>

                        <ul class="list-unstyled">
                            <li><a href="#"><b>Occasional Tables</b></a></li>
                            <li><a href="#">Canterbury Round Tables</a></li>
                            <li><a href="#">Ciera Square and Rectangular Tables</a></li>
                            <li><a href="#">Newport Square and Rectangular Tables</a></li>
                        </ul>

                        <ul class="list-unstyled">
                            <li><a href="#"><b>Accessories</b></a></li>
                            <li><a href="#">Flower Boxes</a></li>
                            <li><a href="#">Cushion Box</a></li>
                            <li><a href="#">Umbrellas</a></li>
                            <li><a href="#">Umbrella Bases</a></li>
                            <li><a href="#">Umbrella Reducer Rings</a></li>
                        </ul>
                        <div class="clear"></div>
                            <div class="lines-grey"></div>
                        <div class="clear height-25"></div>
                        <ul class="list-unstyled">
                            <li><a href="#"><b>Collections</b></a></li>
                            <li><a href="#">Ciera</a></li>
                            <li><a href="#">Monterey</a></li>
                            <li><a href="#">Riviera</a></li>
                            <li><a href="#">SoHo</a></li>
                        </ul>

                        <div class="clear"></div>
                    </div>

                </div>
                <!-- End Left content product -->

                <div class="col-md-9 col-lg-9">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="titles-category-tops">Monterey Dining Armchair</div>
                        </div>
                        <div class="col-md-6">
                            <div class="text-right tright-alcategory"> <div class="clear height-5"></div><div class="height-4"></div>
                                <a href="#">back to Monterey Dining Chairs</a>
                            </div>
                        </div>
                    </div> <div class="clear height-15"></div><div class="height-3"></div>
                    <div class="clear"></div>

                    <div class="cont-box-detail-products">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="big-pictures padding-right-5"><img src="<?php echo $this->assetBaseurl; ?>big-picture-details.jpg" alt="" class="img-responsive"></div>
                            </div>
                            <div class="col-md-4">

                                <div class="padding-left-10 description-detail-rproduct">

                                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                      <div class="panel panel-default">
                                        <div class="panel-heading" role="tab" id="headingOne">
                                          <h4 class="panel-title">
                                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                              <b>Description</b>
                                            </a>
                                          </h4>
                                        </div>
                                        <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                          <div class="panel-body">
                                            <p>The monterey deep seating collection was inspired by a love for family and friends. Its combines perfect comfort with versatility, durability and enduring style. The monterey dining collection features a beautiful contoured seat backrest and lumbar support that enhances comfort and creates an impressive stylish design.</p>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="panel panel-default">
                                        <div class="panel-heading" role="tab" id="headingTwo">
                                          <h4 class="panel-title">
                                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                              <b>Specification</b>
                                            </a>
                                          </h4>
                                        </div>
                                        <div id="collapseTwo" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo">
                                          <div class="panel-body">
                                           <table class="table custom-detailpro">
                                              <tr>
                                              <td>Item Code</td>
                                              <td>MT07</td>
                                              </tr>
                                              <tr>
                                              <td>Material</td>
                                              <td>Teak</td>
                                              </tr>
                                              <tr>
                                              <td>Width</td>
                                              <td>22.25"</td>
                                              </tr>
                                              <tr>
                                              <td>Height</td>
                                              <td>38.75"</td>
                                              </tr>
                                              <tr>
                                              <td>Depth</td>
                                              <td>23.5"</td>
                                              </tr>
                                              <tr>
                                              <td>Seat Height</td>
                                              <td>17”</td>
                                              </tr>
                                              <tr>
                                              <td>Arm Height</td>
                                              <td>26"</td>
                                              </tr>
                                              <tr>
                                              <td>Net Weight</td>
                                              <td></td>
                                              </tr>
                                              <tr>
                                              <td>Gross Weight</td>
                                              <td></td>
                                              </tr>
                                              <tr>
                                              <td>Box Dimension</td>
                                              <td></td>
                                              </tr>
                                              <tr>
                                              <td>Packaging</td>
                                              <td>Knock Down</td>
                                              </tr>
                                            </table>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="clear"></div>
                                    <a href="#" class="back-btn-bluedetail"><i class="fa fa-fax"></i>&nbsp;&nbsp;Print this page</a>
                                    <div class="clear"></div>
                                    <a href="#" class="back-btn-bluedetail"><i class="fa fa-paper-plane"></i>&nbsp;&nbsp;Email this page</a>
                                    <div class="clear"></div>
                                    <a href="#" class="back-btn-bluedetail"><i class="fa fa-share-alt"></i>&nbsp;&nbsp;Share this page</a>
                                    <div class="celar"></div>
                                </div>
                                <!-- End description product -->
                            </div>
                        </div>
                        <div class="clear"></div>
                    </div>

                    <div class="clear"></div>
                </div>
                <!-- End right cont product -->
            </div>



            <div class="clear"></div>
        </div>

        <div class="clear"></div>
    </div>
</section>
<div class="clear height-30"></div>