<div class="py-5"></div>
<div class="py-3"></div>
<section class="produk-sec-1 pt-5">
    <div class="prelative container">
        <div class="row">
            <div class="col-md-15">
                <div class="img-produk">
                    <img class="img img-fluid" src="<?php echo Yii::app()->baseUrl; ?>/images/product/<?php echo $data->image ?>" alt="">
                </div>
            </div>
            <div class="col-md-25">
                <div class="row">
                    <div class="col-md-25">
                        <div class="logo-produk">
                            <img class="img img-fluid" src="<?php echo Yii::app()->baseUrl; ?>/images/category/<?php echo $data_category->image ?>" alt="">
                            <?php 
                            $datas = unserialize($data->data);
                            $prds_color = '#e9bc84';
                            if (isset($datas['color_product'])) {
                                $prds_color = $datas['color_product'];
                            }
                            ?>
                            <h5 style="color: <?php echo $prds_color; ?>"><?php echo strtoupper( $data->description->name ) ?></h5>
                        </div>
                    </div>
                    <div class="col-md-35">
                        <div class="harga text-right">
                            <p>IDR <span>38.000,-</span></p>
                        </div>
                    </div>
                </div>
                <img class="py-3" src="<?php echo Yii::app()->baseUrl; ?>/asset/images/new/bawah-produk-sec-1.png" alt="">
                <p class="atas-cont1">
                    <?php echo nl2br($data->description->intro_desc); ?>
                </p>
                <div class="box-content detail-products">   
                    <div class="block-nutrition">
                        <div class="nutri pt-4">
                            <p> Nutrition Information</p>
                        </div>
                        <hr class="produk">
                        <?php echo $data->description->desc; ?>

                        <div class="clear clearfix"></div>
                    </div> 
                    <hr class="produk">
                    <div class="table-nutrition">
                        <?php echo $data->description->nutrisi; ?>

                        <div class="clear"></div>
                    </div>

                </div>
            </div>
            <div class="col-md-20">
                <div class="box-harga">
                    <div class="row">
                        <div class="col-md-60">
                            <div class="title">
                                <p>Shopping Bag</p>
                            </div>
                        </div>
                        <div class="col-md-20">
                            <div class="subtotal pt-4">
                                <p>Subtotal</p>
                            </div>
                        </div>
                        <div class="col-md-40">
                            <div class="barang pt-4">
                                <p><?php echo $data->description->sajian; ?></p>
                            </div>
                            <div class="pt-5"></div>
                            <div class="pt-5"></div>
                            <!-- <div class="pt-5"></div>
                            <div class="pt-5"></div> -->
                            <!-- <div class="pt-5"></div> -->
                        </div>
                        <div class="col-md-60">
                            <hr class="box-harga2">
                        </div>
                        <div class="col-md-20">
                            <div class="subtotal">
                                <p>Total</p>
                            </div>
                        </div>
                        <div class="col-md-40">
                            <div class="harga-kanan">
                                <p>IDR <?php echo $datas->prices ?>,-</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="pembayaran pt-4">
                    <div class="row">
                        <div class="col-md-60">
                            <div class="title pt-3 pb-4">
                                <p>Checkout dengan pembayaran melalui:</p>
                            </div>
                        </div>
                        <div class="col-md-20 col-20">
                            <div class="box-payment pb-3">
                                <img class="img img-fluid pb-4 mb-2" src="<?php echo Yii::app()->baseUrl; ?>/asset/images/new/tokped.png" alt="">
                                <a target="_blank" href="<?php echo $datas['tokopedia'] ?>">BUY NOW</a>
                            </div>
                        </div>
                        <div class="col-md-20 col-20">
                            <div class="box-payment pb-3">
                                <img class="img img-fluid pb-4 mb-2" src="<?php echo Yii::app()->baseUrl; ?>/asset/images/new/shopee_03.jpg" alt="">
                                <a target="_blank" href="<?php echo $datas['lazada'] ?>">BUY NOW</a>
                            </div>
                        </div>
                        <div class="col-md-20 col-20">
                            <div class="box-payment pb-3">
                                <img class="img img-fluid pb-4 mb-2" src="<?php echo Yii::app()->baseUrl; ?>/asset/images/new/LANDING-PAGE-3-01_07.jpg" alt="">
                                <a target="_blank" href="<?php echo $datas['jdid'] ?>">BUY NOW</a>
                            </div>
                        </div>
                        <div class="col-md-20 col-20">
                            <div class="box-payment pb-3">
                                <img class="img img-fluid pb-4 mb-2" src="<?php echo Yii::app()->baseUrl; ?>/asset/images/new/btns-wa-callb.png" alt="">
                                <a target="_blank" href="https://wa.me/<?php echo $this->setting['contact_wa_order'] ?>?text=I'm%20interested%20Order%20Realfood">BUY NOW</a>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="produk-sec-2 pt-5">
    <div class="prelative container pt-3">
        <div class="row">
            <div class="col-md-15">

            </div>
            <div class="col-md-35">
                <div class="row">
                    <div class="col-md-30">
                        <div class="title-recent">
                            <p>You might also like</p>
                        </div>
                    </div>
                    <div class="col-md-30">
                        <div class="title-recent">
                            <p>Recently viewed</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-10">

            </div>
        </div>
        <div class="row">
            <div class="col-md-15">

            </div>
            <div class="col-md-35">
                <div class="row">         
                    <?php 
                    // All products
                    $criteria=new CDbCriteria;
                    $criteria->with = array('description');

                    $criteria->order = 'date ASC';            
                    $criteria->addCondition('t.status = "1"');

                    $criteria->addCondition('description.language_id = :language_id');
                    $criteria->params[':language_id'] = $this->languageID;
                    $criteria->addCondition('t.id != :idn');
                    $criteria->params[':idn'] = $data->id;
                    $criteria->limit = 4;

                    $model_product = PrdProduct::model()->findAll($criteria);
                    ?>
                    <?php foreach ($model_product as $key => $value): ?>
                    <?php
                    $datas = unserialize($value->data);
                    $prds_color = '#e9bc84';
                    if (isset($datas['color_product'])) {
                        $prds_color = $datas['color_product'];
                    }
                    ?>        
                    <div class="col-md-15 col-30">
                        <div class="product pt-3">
                            <a href="<?php echo CHtml::normalizeUrl(array('/home/produk', 'id'=>$value->id, 'name'=> Slug::Create($value->description->name), 'lang'=>Yii::app()->language)); ?>">
                                <img class="img img-fluid" src="<?php echo Yii::app()->baseUrl; ?>/images/product/<?php echo $value->image2 ?>" alt="">
                            </a>
                            <h2 class="classic pt-3" style="color: <?php echo $prds_color; ?>"><?php echo $value->description->name ?></h2>
                            <div class="ml"><?php echo nl2br($value->description->subtitle) ?></div>
                            <!-- <div class="title">Bird’s Nest</div>
                            <div class="taste">+ Coconut Flavor</div> -->
                            <div class="shop">
                                <a href="<?php echo CHtml::normalizeUrl(array('/home/produk', 'id'=>$value->id, 'name'=> Slug::Create($value->description->name), 'lang'=>Yii::app()->language)); ?>">BUY NOW!</a>
                            </div>
                        </div>  
                    </div>
                    <?php endforeach ?>

                </div>
            </div>
            <div class="col-md-10">

            </div>
        </div>
    </div>
    <!-- <div class="pb-5"></div> -->
    <div class="pb-5 d-none d-sm-block"></div>
    <div class="pb-5"></div>
</section>

<hr class="garis-panjang">

<section class="produk-sec-3">
    <div class="prelative container">
        <div class="row">
            <div class="col-md-60 py-5">
                <div class="title pt-4">
                    <p>We are also available at</p>
                </div>
            </div>
            <?php
                $criteria=new CDbCriteria;
                $criteria->with = array('desc');
                $criteria->addCondition('desc.language_id = :language_id');
                $criteria->params[':language_id'] = $this->languageID;
                $criteria->order = 't.id DESC';

                $allbrand = Brand::model()->findAll($criteria);
            ?>
            <?php if (count($allbrand) > 0): ?>
                <?php foreach ($allbrand as $key => $value): ?>
                <div class="col-md-15 col-30">
                    <img class="img img-fluid" src="<?php echo Yii::app()->baseUrl; ?>/images/brand/<?php echo $value->image ?>" alt="<?php echo $value->desc->title ?>">
                </div>
                <?php endforeach ?>
            <?php endif ?>
        </div>
        <div class="py-3"></div>
    </div>
    <div class="pb-5"></div>
    <div class="pb-5"></div>
</section>