<?php 
if ($this->setting['seo_business_titles']) {
  $this->pageTitle = $this->setting['seo_business_titles'];
}
if ($this->setting['seo_business_keyword']) {
  $this->metaKey = $this->setting['seo_business_keyword'];
}
if ($this->setting['seo_business_description']) {
  $this->metaDesc = $this->setting['seo_business_description'];
}

?>
<?php
$slide = ViewSlide::model()->findAll('language_id = :language_id AND topik_id = 6 ORDER BY sort ASC', array(':language_id' => $this->languageID));
?>
<div class="fcs-wrapper outers_fcs_wrapper prelatife wrapper-slide home">
    <div id="myCarousel_home" class="carousel carousel-fade" data-ride="carousel" data-interval="4500">
            <div class="carousel-inner">
                <?php foreach ($slide as $key => $value): ?>
                <div class="carousel-item <?php if ($key == 0): ?>active<?php endif ?> home-slider-new">
                <img class="w-100 d-none d-sm-block" src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(1920, 1078, '/images/slide/'.$value->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="First slide">
                <img class="w-100 d-block d-sm-none" src="<?php echo Yii::app()->baseUrl; ?>/images/slide/<?php echo $value->image2 ?>" alt="First slide">
                    <div class="carousel-caption caption-slider-home mx-auto">
                        <div class="prelative container mx-auto">
                            <div class="bxsl_tx_fcs">
                                <div class="row no-gutters">
                                    <div class="col-md-60">
                                        <?php echo $value->subtitle ?>
                                        <div class="subtitle py-4">
                                            <?php echo $value->content ?>
                                            <div class="pt-5"></div>
                                            <!-- <a href="<?php echo $value->url ?>">LEARN MORE</a> -->
                                        </div>
                                    </div>
                                    <div class="col-md-27"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endforeach ?>
            </div>
            <a class="carousel-control-prev" href="#myCarousel_home" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
              </a>
              <a class="carousel-control-next" href="#myCarousel_home" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
              </a>
            <?php /*<ol class="carousel-indicators">
                <li data-target="#myCarousel_home" data-slide-to="" class="active"></li>
            </ol>*/ ?>
    </div>
    <div class="clear-both"></div>
</div>

<?php 
$url_string = parse_url($this->setting['business1_urlvideo'], PHP_URL_QUERY);
parse_str($url_string, $args);
$idn_video = isset($args['v']) ? $args['v'] : false;
?>
<section class="bussines-sec-1 py-5">
    <div class="prelative container my-4">
        <div class="row inners_section">
            <div class="col-md-60">
                <div class="box-title py-4 d-block mx-auto text-center">
                    <div class="title default_small_title py-3">
                        <p><?php echo $this->setting['business1_title'] ?></p>
                    </div>
                    <style>.embed-container { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; } .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }</style>
                    <div class='embed-container my-4'><iframe src='https://www.youtube.com/embed/<?php echo $idn_video; ?>' frameborder='0' allowfullscreen></iframe></div>
                    <div class="content py-3">
                        <?php echo $this->setting['business1_content'] ?>
                    </div>
                </div>
            </div>

            <div class="col-md-60 text-center">
                <div class="py-2"></div>
                <div class="title-howreal pt-4 py-3">
                    <p><?php echo $this->setting['business2_smalltitle'] ?></p>
                </div>
                <div class="py-2"></div>
            </div>
        </div>

        <div class="row text-center bnx_list_benefit">

            <?php for ($i=1; $i < 4; $i++) { ?>
            <div class="col-md-20">
                <div class="box-content">
                    <div class="image py-4">
                        <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(99,99, '/images/static/'. $this->setting['business2_sect3_pict_'. $i] , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="">
                    </div>
                    <div class="title">
                        <p><?php echo $this->setting['business2_sect3_title_'. $i] ?></p>
                    </div>
                    <div class="content">
                        <?php echo $this->setting['business2_sect3_content_'. $i] ?>
                    </div>
                </div>
            </div>
            <?php } ?>

            <!-- <div class="col-md-20">
                <div class="box-content">
                    <div class="image py-4">
                        <img src="<?php echo $this->assetBaseurl2 ?>testn_circle.png" alt="">
                    </div>
                    <div class="title">
                        <p>BEPERGIAN & PERAYAAN</p>
                    </div>
                    <div class="content">
                        <p>Penuhi kualifikasi untuk konferensi dan event internasional, dimana Anda akan mendapatkan rekognisi untuk pencapaian Anda.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-20">
                <div class="box-content">
                    <div class="image py-4">
                        <img src="<?php echo $this->assetBaseurl2 ?>testn_circle.png" alt="">
                    </div>
                    <div class="title">
                        <p>BERSAMA MENYEHATKAN MASYARAKAT</p>
                    </div>
                    <div class="content">
                        <p>Jadilah bagian dari sesuatu yang besar dengan melihat dari sudut pandang yang lebih tinggi, setiap kali anda mendistribusikan Realfood, anda menyebarkan kebaikan di dunia.</p>
                    </div>
                </div>
            </div>
             -->
        </div>
    </div>
</section>

<section class="bussines-sec-2 py-5">
    <div class="prelative container py-5">
        <div class="row inners_section py-4">
            <div class="col-md-60">
                <div class="title-head">
                    <div class="title">
                        <p><?php echo $this->setting['business3_title'] ?></p>
                    </div>
                    <div class="subtitle">
                        <?php echo $this->setting['business3_content'] ?>
                    </div>
                </div>
            </div>
            </div>
            <div class="py-3"></div>
            <?php 
            $criteria = new CDbCriteria;
            $criteria->with = array('description');
            $criteria->addCondition('status = "1"');
            $criteria->addCondition('description.language_id = :language_id');
            $criteria->params[':language_id'] = $this->languageID;
            $data_testimon = PgTestimonial::model()->findAll($criteria);
            ?>
            <div class="row boxsn_list_partnertestimonial">

                <?php foreach ($data_testimon as $key => $value): ?>
                <div class="col-md-20">
                    <div class="box-content">
                        <div class="image">
                            <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(370, 207, '/images/testimoni/'.$value->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" class="img img-fluid" alt="">
                        </div>
                        <div class="title">
                            <p><?php echo $value->description->content ?></p>
                        </div>
                        <div class="nama">
                            <p><?php echo $value->name ?></p>
                        </div>
                    </div>
                </div>
                <?php endforeach; ?>

                <!-- <div class="col-md-20">
                    <div class="box-content">
                        <div class="image">
                            <img src="http://placehold.it/370x207" class="img img-fluid" alt="">
                        </div>
                        <div class="title">
                            <p>“SEBUAH PRODUK YANG MEMBANGGAKAN KAMI SEBAGAI PENJUAL”</p>
                        </div>
                        <div class="nama">
                            <p>Andreas Siregar</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-20">
                    <div class="box-content">
                        <div class="image">
                            <img src="http://placehold.it/370x207" class="img img-fluid" alt="">
                        </div>
                        <div class="title">
                            <p>“SEBUAH KESEMPATAN UNTUK BERBUAT BAIK KEPADA MASYARAKAT”</p>
                        </div>
                        <div class="nama">
                            <p>Marsha Antoniette</p>
                        </div>
                    </div>
                </div> -->
            </div>
    </div>
</section>

<section class="business-sec-4">
    <div class="prelative container pt-5">
        <div class="row">
            <div class="col-md-30">
                
            </div>
            <div class="col-md-30">
                <div class="box-section pt-5">
                    <div class="title pt-2">
                        <p><?php echo $this->setting['business4_title'] ?></p>
                    </div>
                    <div class="subtitle pt-3 pb-5">
                        <p><?php echo $this->setting['business4_content'] ?></p>
                    </div>
                    <form enctype="multipart/form-data" class="form-" id="yw0" action="<?php echo CHtml::normalizeUrl(array('/home/business')); ?>" method="post" onsubmit="javascript: alert('underconstruction'); return false;">
                        <div class="row default">
                            <div class="col-md-30 col-sm-30">
                                <div class="form-group">
                                <label>NAME</label>
                                <input class="form-control" name="ContactForm[name]" id="ContactForm_name" type="text">                              </div>
                                </div>
                            <div class="col-md-30 col-sm-30">
                                <div class="form-group">
                                    <label>TELEPHONE</label>
                                    <input class="form-control" name="ContactForm[phone]" id="ContactForm_company" type="text">                             </div>
                                </div>
                        </div>

                        <div class="row default">
                        <div class="col-md-30 col-sm-30">
                            <div class="form-group">
                                <label>EMAIL</label>
                                <input class="form-control" name="ContactForm[email]" id="ContactForm_email" type="text">                           </div>
                        </div>
                        <div class="col-md-30 col-sm-30">
                            <div class="form-group">
                            <label>ADDRESS</label>
                            <input class="form-control" name="ContactForm[address]" id="ContactForm_phone" type="text">                           </div>
                        </div>
                        </div>

                        <div class="row default captcha-mobile">
                            <div class="col-md-60">
                                <div class="py-1"></div>
                                <div class="row default">
                                    <div class="col-md-30 col-sm-30">
                                        <div class="d-block picts_captcha" style="margin-left: -15px;">
                                            <div class="g-recaptcha" data-sitekey="6LfsiLUUAAAAANpsRfvWqg2c7woCVEjBD6srQzEP"></div>
                                          </div>
                                    </div>
                                    <div class="col-md-30 col-sm-30 hide-pc">
                                        <div class="text-right float-right submit">
                                            <button type="submit"></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row no-gutters">
                            <div class="col-md-60">
                                <div class="py-3"></div>
                                <p><?php echo $this->setting['business4_content2'] ?></p>
                                <p><a target="_blank" href="https://wa.me/<?php echo str_replace('08', '628', $this->setting['contact_wa']) ?>">Click to chat <img src="<?php echo $this->assetBaseurl2.'icn-wa.png'; ?>" alt="" class="d-inline-block px-1 img img-fluid"> <b><?php echo $this->setting['contact_wa'] ?></b></a></p>
                            </div>
                        </div>
                    </form>             
                </div>
            </div>
        </div>
    </div>

    <div class="d-block d-sm-none backs_frontbottoms">
        <img class="w-100" src="/asset/images/home-sec-4-mobile.jpg" alt="">
    </div>
</section>

<section class="outers-block-gemaa static_page">
    <div class="blocksn_top_grey py-5">
        <div class="prelative container py-5">
            <?php 
            $ndata_numbers = [
                            1 => [
                                'titles'=> $this->setting['business5_itm_title_1'],
                                'city'=> $this->setting['business5_itm_titlesmall_1'],
                                'desc'=>$this->setting['business5_itm_descs_1'],
                                ],
                                [
                                'titles'=> $this->setting['business5_itm_title_2'],
                                'city'=> $this->setting['business5_itm_titlesmall_2'],
                                'desc'=>$this->setting['business5_itm_descs_2'],
                                ],
                                [
                                'titles'=> $this->setting['business5_itm_title_3'],
                                'city'=> $this->setting['business5_itm_titlesmall_3'],
                                'desc'=>$this->setting['business5_itm_descs_3'],
                                ],
                            ];
            ?>
            <div class="inner-section text-center">
                <h5><?php echo $this->setting['business5_title'] ?></h5>
                <div class="py-4 mb-2"></div>
                <div class="row lists_top_gma">
                    <?php foreach($ndata_numbers as $key => $value): ?>
                    <div class="col-md-20">
                        <div class="items">
                            <div class="titles">
                                <span><?php echo $value['titles'] ?></span>
                                <div class="d-block clear clearfix"></div>
                                <small><?php echo $value['city'] ?></small>
                            </div>
                            <div class="desc py-3">
                                <p><?php echo $value['desc'] ?></p>
                            </div>
                        </div>
                    </div>
                    <?php endforeach; ?>
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </div>
</section>

<section class="blocks_faq_bottom back-white py-5" id="block-faq">
    <div class="prelatife container my-5">

        <div class="inner-sections text-center content-text">

            <h4 class="small-title default_small_title">FAQ</h4>
            <div class="py-4"></div>
            <?php 
            $data_faq = [
                            [
                            'question'=>'Berapa banyak waktu yang perlu dialokasikan untuk menjadi rekan Realfood?',
                            'answer'=>'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod',
                            ],
                            [
                            'question'=>'Bagaimana rasanya menjadi seorang rekan Realfood?',
                            'answer'=>'tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,',
                            ],
                            [
                            'question'=>'Bagaimana caranya untuk bergabung?',
                            'answer'=>'quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo',
                            ],
                            [
                            'question'=>'Bagaimana caranya untuk memulai?',
                            'answer'=>'consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse',
                            ],
                            [
                            'question'=>'Hal apa yang diperlukan untuk bisa menjadi agen Realfood?',
                            'answer'=>'cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non',
                            ],
                            [
                            'question'=>'Bagaimana caranya agar saya meningkatkan penjualan?',
                            'answer'=>'proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
                            ],
                            
                        ];

            $criteria = new CDbCriteria;
            $criteria->with = array('description');
            $criteria->addCondition('status = "1"');
            $criteria->addCondition('description.language_id = :language_id');
            $criteria->params[':language_id'] = $this->languageID;
            $criteria->order = 't.id DESC';
            $data_faq = Faq::model()->findAll($criteria);
            ?>
            <!-- start card -->
            <div id="accordion" class="ns_accordion d-block mx-auto text-left">
              <?php foreach ($data_faq as $key => $value) { ?>
              <div class="card">
                <div class="card-header" id="heading<?php echo $key ?>">
                  <h5 class="mb-0">
                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse<?php echo $key ?>" aria-expanded="true" aria-controls="collapse<?php echo $key ?>">
                      <?php echo $value->description->question; ?>
                    </button>
                  </h5>
                </div>
                <div id="collapse<?php echo $key ?>" class="collapse" aria-labelledby="heading<?php echo $key ?>" data-parent="#accordion">
                  <div class="card-body">
                    <?php echo $value->description->answer; ?>
                  </div>
                </div>
              </div>
            <?php } ?>
            </div>
            <!-- end card -->

            <div class="clear"></div>
        </div>

        <div class="clear"></div>
    </div>

    <div class="d-none d-sm-block">
    <div class="py-4 my-3"></div>
    <div class="lines_grey_def"></div>
    <div class="py-4 my-3"></div>

    <div class="prelatife container pb-4 pt-0">
        <div class="inner-sections text-center content-text">
            <h4 class="small-title default_small_title">CONNECT WITH US</h4>
            <div class="py-3"></div>
            <div class="blocks_social_btmnlink">
                <ul class="list-unstyled">
                    <?php if ($this->setting['url_facebook'] != ''): ?>
                    <li class="d-inline-block px-1">
                        <a target="_blank" href="<?php echo $this->setting['url_facebook'] ?>"><img src="<?php echo $this->assetBaseurl2; ?>sc-facebook.png" alt="" class="img img-fluid"></a></li>
                    <?php endif ?>
                    <?php if ($this->setting['url_instagram'] != ''): ?>
                    <li class="d-inline-block px-1"><a target="_blank" href="<?php echo $this->setting['url_instagram'] ?>"><img src="<?php echo $this->assetBaseurl2; ?>sc-instagram.png" alt="" class="img img-fluid"></a></li>
                    <?php endif ?>
                    <?php if ($this->setting['url_youtube'] != ''): ?>
                    <li class="d-inline-block px-1"><a target="_blank" href="<?php echo $this->setting['url_youtube'] ?>"><img src="<?php echo $this->assetBaseurl2; ?>sc-youtube.png" alt="" class="img img-fluid"></a></li>
                    <?php endif ?>
                    
                    <!-- <li class="d-inline-block px-1"><a href="#"><img src="<?php echo $this->assetBaseurl2; ?>sc-wa.png" alt="" class="img img-fluid"></a></li> -->
                </ul>
                <div class="clear"></div>
            </div>

            <div class="clear"></div>
        </div>
    </div>
    </div>

</section>

<script src="https://www.google.com/recaptcha/api.js" async defer></script>