<section class="outer-inside-middle-content back-white PageGema_section">
  <div class="prelatife container">
    <div class="tops-cont-insidepage back-pgema" style="background: none; height: auto; position: relative;">

      <div class="pict-fulls">
            <?php
            // $slide = ViewSlide::model()->findAll('language_id = :language_id AND topik_id = 4 ORDER BY sort ASC', array(':language_id'=>$this->languageID));
            ?>
            <div id="carousel-examp-topPage" class="carousel slide" data-ride="carousel">
              <div class="carousel-inner" role="listbox">
                <?php // foreach ($slide as $ke => $value): ?>
                <div class="item <?php if ($ke == 0): ?>active<?php endif ?>">
                  <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(1450,680, '/asset/images/gema/hero-pict-our-next-mission.jpg' , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="<?php echo $value->title; ?>">
                </div>
                <?php // endforeach ?>
              </div>
            </div>

        </div>

      <div class="pabs-ins-middle-topim">
        <div class="tengah insd-container mw930 text-center content-up hidden-xs">
          <div class="hidden-xs">
            <?php // if ($this->setting['gema_hide'] == 0): ?>
            <h1 class="title-pages">GE-MA</h1> <div class="clear height-10"></div>
            <div class="lines-chld-bgrey tengah"></div>
            <div class="clear height-25"></div>
              <span class="bigs brown"><?php echo $text_hero ?></span> 
            <div class="clear"></div>
            <?php if ($text_hero2): ?>
            <span class="subs-bigs"><?php echo $text_hero2 ?></span>
            <?php endif ?>
            <?php // endif ?>
            <div class="clear"></div>
          </div>
        </div>
        <div class="clear"></div>
      </div>
    </div>

    <div class='pindahan_text-heroimage visible-xs'>
         <h1 class="title-pages">GE-MA</h1> <div class="clear height-10"></div>
          <div class="lines-chld-bgrey tengah"></div>
          <div class="clear height-25"></div>
          <span class="bigs brown"><?php echo $text_hero ?></span> 
          <div class="clear"></div>
          <?php if ($text_hero2): ?>
          <span class="subs-bigs"><?php echo $text_hero2 ?></span>
          <?php endif ?>
          <div class="clear"></div>
    </div>

        <div class="clear"></div>
      </div>
      <div class="prelatife">
        <div class="sub-headerData-menu">
            <div class="prelatife mw-995 tengah">
              <?php echo $this->renderPartial('//layouts/_menu_gema', array('active'=>$active)); ?>
              <div class="box-shadow"></div>
              <div class="clear"></div>
            </div>
        </div>

        <section class="default-section-gema prelatife content-text text-center back-cream">
            <div class="clear height-50"></div>
            <div class="clear height-50"></div>
            <div class="clear height-5"></div>
            <div class="prelatife container gema_container">
              <div class="row">
                <div class="col-md-6">
                  <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(560,2000, '/images/static/'.$this->setting['start_image'] , array('method' => 'resize', 'quality' => '90')) ?>" alt="" class="img-responsive pc-center">
                </div>
                <div class="col-md-6">
                  
                  <div class="mw-505 tengah text-left txt-content padding-left-45">
                    <div class="clear height-40"></div>
                    <h6><?php echo $this->setting['start_title'] ?></h6>
                    <span><?php echo $this->setting['start_subtitle'] ?></span>
                    <?php echo $this->setting['start_content'] ?>
                    <div class="clear"></div>
                  </div>

                </div>
              </div>
            </div>
            <div class="clear height-50"></div>
            <div class="clear height-35"></div>
        </section>
        <div class="clear"></div>
        <section class="section_1 default-section-gema prelatife">
            <div class="left_cont prelatife">
                <div class="pict-full"><img src="<?php echo $this->assetBaseurl ?>gema/pict2-our-next-mission.jpg" alt=""></div>
            </div>
        </section>
        <div class="clear"></div>
      </div>
  <!-- </div> -->
</section>

<script type="text/javascript">
    $(document).ready(function(){
        var wbody = $(window).width();

        var heightHero = $('#carousel-examp-topPage img').height();
        if (wbody > 768){
            $('.PageGema_section .pabs-ins-middle-topim').css("height", heightHero+"px");
        };

        // set w
        var hsec1 = $('.section_1 img').height();
        $('.section_1 .table-out').css("height", hsec1+"px");
    });

    $(window).load(function(){
        var wbody = $(window).width();

        var heightHero = $('#carousel-examp-topPage img').height();
        if (wbody > 768){
            $('.PageGema_section .pabs-ins-middle-topim').css("height", heightHero+"px");
        };

        // set w
        var hsec1 = $('.section_1 img').height();
        $('.section_1 .table-out').css("height", hsec1+"px");
    });
</script>