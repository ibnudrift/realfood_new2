
<section class="middle-content">
        <div class="prelatife container">
            <div class="content-text">
                <div class="py-5 my-3"></div>
                <div class="row">
                    <div class="col-md-30">
                        <h1>ERROR <?php echo $error['code'] ?></h1>
                        <div class="py-4"></div>
                        <h3><?php echo $error['message'] ?></h3>

                            <div class="clearfix"></div>
                    </div>
                    <div class="col-md-30">
                        <div class="featured-image"><img class="img img-fluid" src="<?php echo Yii::app()->baseUrl; ?>/asset/images/new/CAREER-POP-UP-01_01.jpg" alt=""></div>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="py-5 my-3"></div>
            </div>
            <div class="clear"></div>
        </div>
    <div class="clear"></div>
</section>