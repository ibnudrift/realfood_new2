<!-- Start fcs -->
<?php
$slide = ViewSlide::model()->findAll('language_id = :language_id AND topik_id = 3 ORDER BY sort ASC', array(':language_id'=>$this->languageID));
?>
<div class="fcs-wrapper outers_fcs_wrapper prelatife wrapper-slide faq">
    <div id="myCarousel_home" class="carousel carousel-fade" data-ride="carousel" data-interval="4500">
            <div class="carousel-inner">
                <?php foreach ($slide as $key => $value): ?>
                <div class="carousel-item <?php if ($key == 0): ?>active<?php endif ?> home-slider-new">
                <img class="w-100 d-none d-sm-block" src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(1920, 1078, '/images/slide/'.$value->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="First slide">
                <img class="w-100 d-block d-sm-none" src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(375, 667, '/images/slide/'.$value->image2 , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="First slide">
                    <div class="carousel-caption caption-slider-home mx-auto">
                        <div class="prelative container mx-auto">
                            <div class="bxsl_tx_fcs">
                                <div class="row no-gutters">
                                    <div class="col-md-60">
                                        <div class="pt-3"></div>
                                        <p><?php echo $value->subtitle ?></p>
                                        <div class="pt-3"></div>
                                        <div class="line-insides"></div>
                                        <div class="subtitle py-4">
                                            <?php echo $value->content ?>
                                            <div class="pt-5"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-27"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endforeach ?>
            </div>
            <?php /*<ol class="carousel-indicators">
                <li data-target="#myCarousel_home" data-slide-to="" class="active"></li>
            </ol>*/ ?>
    </div>
    <div class="clear-both"></div>
</div>
<!-- End fcs -->

<section class="faq-sec-1">
    <div class="prelative container">
        <div class="sec-satu">
            <div class="row">
                <div class="col-md-30">
                    <div class="title">
                        <p>Frequently <br> <span>ASKED QUESTIONS</span> </p>
                    </div>
                </div>
                <div class="col-md-30">
                    <div class="image">
                        <img class="" src="<?php echo Yii::app()->baseUrl; ?>/asset/images/new/faq.png" alt="">
                    </div>
                </div>
            </div>
        </div>
        <div class="sec-dua">
            <div class="row">
                <div class="col-md-30">
                    <div class="box-topics">
                        <div class="title">
                            <p>Topics</p>
                        </div>
                        <ul>
                            <?php foreach ($model as $key => $value): ?>
                            <li><?php echo $value->question ?></li>
                            <?php endforeach ?>
                        </ul>
                    </div>
                </div>
                <div class="col-md-30">
                    <div class="box-answer">
                        <div class="title">
                            <p>Answer</p>
                        </div>
                        <?php foreach ($model as $key => $value): ?>
                        <div class="box-ask">
                            <div class="title">
                                <p>
                                    <?php echo $value->question ?>
                                </p>
                            </div>
                            <div class="content">
                                <?php echo $value->answer ?>
                            </div>
                        </div>
                        <?php endforeach ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="pb-5"></div>
    <div class="pb-5"></div>
    <!-- <div class="pb-5"></div> -->
</section>