  <section class="full-banner-top prelatife">
      <div class="tops-cont-insidepage back-pgema" style="background: none; height: auto; position: relative;"> 
         <div class="pict-fulls hidden-xs" id="carousel-example-fcs">
          <div id="carousel-examp-topPage" class="carousel fade" data-ride="carousel">
            <?php
            $slide = ViewSlide::model()->findAll('language_id = :language_id AND topik_id = 0 ORDER BY sort ASC', array(':language_id'=>$this->languageID));
            ?>
            <div class="carousel-inner" role="listbox">
              <?php foreach ($slide as $key => $value): ?>
              <div class="item <?php if ($key == 0): ?>active<?php endif ?>">
                <!-- <img src="<?php // echo Yii::app()->baseUrl.ImageHelper::thumb(1450,680, '/images/slide/'.$value->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="<?php echo $value->title; ?>"> -->
                <div class="fill" style="background-image:url(<?php echo Yii::app()->baseUrl.ImageHelper::thumb(1450,680, '/images/slide/'.$value->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>);"></div>
              </div>
              <?php endforeach ?>
            </div>

            <a class="left carousel-control" href="#carousel-examp-topPage" role="button" data-slide="prev">
              <i class="fa fa-chevron-left"></i>
            </a>
            <a class="right carousel-control" href="#carousel-examp-topPage" role="button" data-slide="next">
              <i class="fa fa-chevron-right"></i>
            </a>
          </div>

          <div class="clear"></div>
      </div>
      
      <!-- start view responsive -->
      <div class="visible-xs">
          <div id="carousel_ex_phone" class="carousel fade" data-ride="carousel">
              <div class="carousel-inner" role="listbox">
                <?php foreach ($slide as $ke => $value): ?>
                <div class="item <?php if ($ke == 0): ?>active<?php endif ?>">
                  <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(774,867, '/images/slide/'.$value->image2 , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="<?php echo $value->title; ?>">                  
                </div>
                <?php endforeach ?>
              </div>
            </div>
          <div class="clear"></div>
        </div>

        <div class="back-green-homes visible-xs">
          <div class="visible-xs clear height-15"></div>
          <div class="prelatife container">
            <div class="middle-top-con-home text-center">
              <div class="titles">
                <?php if ($this->setting['home_headline_product_status'] == 0): ?>
                <?php 
                $string1= preg_replace('/<p[^>]*>/i', '', $this->setting['home_headline_product']);
                $string1= preg_replace('/<\/p>/i', '', $string1);
                ?>
                <?php echo $string1 ?> 
                <?php endif ?>
              </div> <div class="clear height-25"></div>
            </div>
          </div>
        </div>
    </div>

    <div class="clear"></div>
    <div class="pos_abs_full_homegreen">
      <div class="back-green-homes hidden-xs prelatife">
        <div class="clear height-25 hidden-xs"></div>
        <div class="visible-xs clear height-15"></div>

        <div class="prelatife container">
          <div class="middle-top-con-home text-center">
            <div class="titles">
              <?php if ($this->setting['home_headline_product_status'] == 0): ?>
              <?php 
              $string1= preg_replace('/<p[^>]*>/i', '', $this->setting['home_headline_product']);
              $string1= preg_replace('/<\/p>/i', '', $string1);
              ?>
              <?php echo $string1 ?> 
              <?php endif ?>
            </div> <div class="clear height-25"></div>
          </div>
        </div>
      </div>
      <div class="clear"></div>
    </div>
  </section>
  
<?php 
$criteria = new CDbCriteria;
$criteria->with = array('description');
$criteria->addCondition('t.type = :type');
$criteria->params[':type'] = 'category';
$criteria->order = 'sort ASC';
$dataCategory = PrdCategory::model()->findAll($criteria);
?>

<div class="outers-middle-contents back-white">

    <div class="middle-top-con-home text-center">
      <div class="grey-middle-homesl">
      <div class="hidden-xs clear height-25"></div>
      <div class="prelatife container">
          <?php
          $gallery = ViewGallery::model()->findAll('language_id = :language_id ORDER BY date_input DESC LIMIT 3', array(':language_id'=>$this->languageID));
          ?>

          <div class="middle-conts">
            <div class="row">
              <?php foreach ($dataCategory as $key => $value): ?>
              <div class="col-md-4">
                <div class="itemss" style="background-color: #F1F1F2;">
                  <div class="clear height-45"></div>
                  <div class="picts">
                    <div class="hidden-xs">
                      <a href="<?php echo CHtml::normalizeUrl(array('/product/detail', 'id'=>$value->id)); ?>">
                        <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(455,455, '/images/category/'.$value->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img-responsive center-block">
                      </a>
                    </div>

                    <div class="visible-xs" style="max-width:125px; margin:0 auto;">
                      <a href="<?php echo CHtml::normalizeUrl(array('/product/detail', 'id'=>$value->id)); ?>">
                        <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(455,455, '/images/category/'.$value->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img-responsive center-block">
                      </a>
                    </div>  

                  </div>
                  <div class="descs">
                      <div class="h_indesc_top">
                          <div class="title">
                            <a href="<?php echo CHtml::normalizeUrl(array('/product/detail', 'id'=>$value->id)); ?>">
                               <?php echo $value->description->name ?>
                            </a>
                          </div> 
                      </div>
                      <div class="buttons_b_product">
                        <a href="<?php echo CHtml::normalizeUrl(array('/product/detail', 'id'=>$value->id)); ?>">
                          <img class="hidden-xs pc-center" src="<?php echo $this->assetBaseurl; ?>back-bt-view-products.png" alt="">
                          <img class="visible-xs pc-center" src="<?php echo $this->assetBaseurl; ?>back-bt-view-products-responsl.png" style="width: 122px; height: 22px;" alt="">
                        </a>
                      </div>
                  </div>
                  <div class="clear"></div>
                </div>
              </div>
              <?php endforeach ?>
              <div class="visible-xs clear height-45" style="background-color:rgb(241, 241, 242);"></div>
            </div>

            <div class="clear"></div>
          </div>

        
        <div class="clear"></div>
      </div>
      </div>

    </div> 
    <div class="hidden">
      <div class="clear height-25"></div><div class="height-2"></div>
    </div>
    <div class="clear"></div>
  </div>
  
   <section class="gema" style="background-image: url(<?php echo Yii::app()->baseUrl; ?>/images/static/<?php echo $this->setting['home_gema_image']; ?>)">
      <div class="prelatife container"> <div class="clear height-50"></div><div class="height-10"></div>
        <div class="row">
          <div class="col-md-6">
              <div class="pict-center leftgema"><img src="<?php echo $this->assetBaseurl; ?>pict-bt-gema.png" alt="" class="pc-center"></div>
              <div class="clear height-45"></div>
              <span class="leftdesc-d text-center">Bukan Kami, Tetapi Kita</span>
          </div>
          <div class="col-md-6">
            <div class="hidden-xs">
              <div class="clear height-50"></div>
              <div class="clear height-50"></div>
              <div class="clear height-10"></div>
            </div>
            <div class="tengah ts-center text-left">
                <p class="big"><?php echo nl2br($this->setting['home_gema_1']) ?></p>
                <p><?php echo nl2br($this->setting['home_gema_2']) ?></p>
                <div class="clear height-20"></div>
                <div class="text-center hidden-xs"><a href="<?php echo CHtml::normalizeUrl(array('home/gema')); ?>"><img src="<?php echo $this->assetBaseurl; ?>back-bt-default-readmore.png" alt=""></a></div>
                <div class="text-center visible-xs"><a href="<?php echo CHtml::normalizeUrl(array('home/gema')); ?>"><img src="<?php echo $this->assetBaseurl; ?>btns-readmores-respons.png" style="width:95px;" alt=""></a></div>
            </div>
          </div>
        </div>
        
        <div class="clear"></div>
      </div>
      <!-- // back title article -->
      <div class="back-green-homes blocks_sub_title_article_home bottom_home text-center margin-top-40">
        <div class="visible-xs clear height-20"></div>
        <div class="titles">
          <!-- IMPROVING YOUR HEALTH & <b>THEIRS’</b> -->
          <?php if ($this->setting['home_headline_article_status'] == 0): ?>
          <!-- CHOOSE THE ONE FOR YOU <br>
          <b>AND FOR THEM</b> -->
          <?php 
          $string2= preg_replace('/<p[^>]*>/i', '', $this->setting['home_headline_article']);
          $string2= preg_replace('/<\/p>/i', '', $string2);
          ?>
          <?php echo $string2 ?>
          <?php endif ?>
        </div>
        <div class="clear visible-xs height-25"></div>
      </div>
  </section>
<?php
$blog = ViewBlog::model()->findAll('active = 1 ORDER BY date_input DESC LIMIT 5');
?>
  <div class="back-grey mh500">
      

      <div class="prelatife container outers-cont-bottom-grey">
        <div class="listing">
          <div class="row">
            <div class="col-md-8 col-sm-12">
                <div class="data-ch back-white mw-823">
                  <div class="pict"><img class="img-responsive" src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(823,288, '/images/blog/'.$blog[0]->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt=""></div>
                  <div class="desc h95 prelatife">
                    <span class="title"><?php echo $blog[0]->title ?></span> <div class="clear"></div>
                    <p><?php echo substr(strip_tags($blog[0]->quote), 0, 150) ?></p>
                    <div class="pos-abs-rightbt hide"><a href="<?php echo CHtml::normalizeUrl(array('/artikel/detail', 'id'=>$blog[0]->id)); ?>"><img src="<?php echo $this->assetBaseurl; ?>back-btn-df-read.png" alt=""></a></div>
                    <div class="pos-abs-rightbt"><a href="<?php echo CHtml::normalizeUrl(array('/artikel/detail', 'id'=>$blog[0]->id)); ?>"><img src="<?php echo $this->assetBaseurl; ?>back-btn-reads-respons.png" style="width: 57px; height: 11px;" alt=""></a></div>
                  </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="data-ch back-white">
                  <div class="pict"><img class="img-responsive" src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(407,288, '/images/blog/'.$blog[1]->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt=""></div>
                  <div class="desc h95 prelatife">
                    <span class="title"><?php echo $blog[1]->title ?></span> <div class="clear"></div>
                    <p><?php echo substr(strip_tags($blog[1]->quote), 0, 70) ?></p>

                    <div class="pos-abs-rightbt hide"><a href="<?php echo CHtml::normalizeUrl(array('/artikel/detail', 'id'=>$blog[1]->id)); ?>"><img src="<?php echo $this->assetBaseurl; ?>back-btn-df-read.png" alt=""></a></div>
                    <div class="pos-abs-rightbt"><a href="<?php echo CHtml::normalizeUrl(array('/artikel/detail', 'id'=>$blog[1]->id)); ?>"><img src="<?php echo $this->assetBaseurl; ?>back-btn-reads-respons.png" style="width: 57px; height: 11px;" alt=""></a></div>
                  </div>
                </div>
            </div>

         <!--  </div>
          <div class="clear height-10"></div><div class="height-4"></div>
          <div class="row"> -->
            <?php foreach ($blog as $key => $value): ?>
              <?php if ($key > 1): ?>
                <div class="col-md-4 col-sm-6">
                    <div class="data-ch back-white">
                      <div class="pict"><img class="img-responsive" src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(407,288, '/images/blog/'.$value->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt=""></div>
                      <div class="desc h95 prelatife">
                        <span class="title"><?php echo $value->title ?></span> <div class="clear"></div>
                        <p><?php echo substr(strip_tags($value->quote), 0, 70) ?></p>
                        <div class="pos-abs-rightbt hide"><a href="<?php echo CHtml::normalizeUrl(array('/artikel/detail', 'id'=>$value->id)); ?>"><img src="<?php echo $this->assetBaseurl; ?>back-btn-df-read.png" alt=""></a></div>
                        <div class="pos-abs-rightbt"><a href="<?php echo CHtml::normalizeUrl(array('/artikel/detail', 'id'=>$value->id)); ?>"><img src="<?php echo $this->assetBaseurl; ?>back-btn-reads-respons.png" style="width: 57px; height: 11px;" alt=""></a></div>
                      </div>
                    </div>
                </div>
                
              <?php endif ?>
            <?php endforeach ?>
          </div>
          <div class="clear"></div>
        </div>
        <div class="clear height-25 visible-lg"></div>
        <div class="visible-xs clear height-0"></div>
        <div class="hidden hide">
          <div class="text-center bts-readmorethis">
            <span class="text-center">Read more stories?</span> <div class="clear height-5"></div>
            <a href="<?php echo CHtml::normalizeUrl(array('artikel/index')); ?>">
              <span class="hidden-xs tengah text-center">
                <img src="<?php echo $this->assetBaseurl; ?>back-btn-why.png" alt="">
              </span>
              <span class="viewblocks-greenbtn-homb visible-xs">Why not make your own</span>
            </a>
          </div> 
          <div class="clear height-50 visible-lg"></div>
          <div class="height-15 visible-xs"></div>
          <div class="height-10"></div>
        </div>

        <div class="clear"></div>
      </div>
  </div>
<script type="text/javascript">
$('.fancy').fancybox({
        // type: 'image',
});
</script>
