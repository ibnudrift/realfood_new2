<section class="outer-inside-middle-content back-white PageGema_section">
  <div class="prelatife container">
    <div class="tops-cont-insidepage back-pgema" style="background: none; height: auto; position: relative;">

      <div class="pict-fulls">
            <?php
            // $slide = ViewSlide::model()->findAll('language_id = :language_id AND topik_id = 4 ORDER BY sort ASC', array(':language_id'=>$this->languageID));
            ?>
            <div id="carousel-examp-topPage" class="carousel slide" data-ride="carousel">
              <div class="carousel-inner" role="listbox">
                <?php // foreach ($slide as $ke => $value): ?>
                <div class="item <?php if ($ke == 0): ?>active<?php endif ?>">
                  <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(1450,680, '/asset/images/gema/hero-pict-our-next-mission.jpg' , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="<?php echo $value->title; ?>">
                </div>
                <?php // endforeach ?>
              </div>
            </div>

        </div>

      <div class="pabs-ins-middle-topim">
        <div class="tengah insd-container mw930 text-center content-up">
          <div class="hidden-xs">
              <?php // if ($this->setting['gema_hide'] == 0): ?>
              <h1 class="title-pages">GE-MA</h1> <div class="clear height-10"></div>
              <div class="lines-chld-bgrey tengah"></div>
              <div class="clear height-25"></div>
                <span class="bigs brown"><?php echo $text_hero ?></span> 
              <div class="clear"></div>
              <?php if ($text_hero2): ?>
              <span class="subs-bigs"><?php echo $text_hero2 ?></span>
              <?php endif ?>
              <?php // endif ?>
              <div class="clear"></div>
          </div>
        </div>
        <div class="clear"></div>
      </div>
    </div>
    <div class='pindahan_text-heroimage visible-xs'>
        <div class="hidden-lg hidden-xs">
            <div class="clear height-50"></div>
          </div>
          <div class="visible-xs clear height-5"></div>
              <?php //if ($this->setting['gema_hide'] == 0): ?>
                <h1 class="title-pages">GE-MA</h1> <div class="clear height-10"></div>
              <div class="lines-chld-bgrey tengah"></div>
                <div class="clear height-25"></div>
              <?php // endif ?>
              <span class="bigs"><?php echo $text_hero ?></span> 
              <div class="clear"></div>
        </div>
        <div class="clear"></div>
      </div>
      <div class="prelatife">
        <div class="sub-headerData-menu">
            <div class="prelatife mw-995 tengah">
              <?php echo $this->renderPartial('//layouts/_menu_gema', array('active'=>$active)); ?>
              <div class="box-shadow"></div>
              <div class="clear"></div>
            </div>
        </div>

        <section class="section_1 default-section-gema prelatife back-greyc our-next-mission">
            <div class="left_cont prelatife">
                <div class="back-white h70"></div>
                <div class="clear height-35"></div>
                <div class="container prelatife gema_container">
                    <div class="list-hold-bol-txt">
                      <div class="row">
                        <div class="col-lg-3 col-md-6 col-sm-6">
                          <div class="items">
                              <span class="bold"><?php echo $this->setting['mission_number_1'] ?></span>
                              <div class="clear"></div>
                              <p><?php echo $this->setting['mission_text_1'] ?></p>
                              <div class="clear height-5"></div>
                          </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-6">
                          <div class="items">
                              <span class="bold"><?php echo $this->setting['mission_number_2'] ?></span>
                              <div class="clear"></div>
                              <p><?php echo $this->setting['mission_text_2'] ?></p>
                              <div class="clear height-5"></div>
                          </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-6">
                          <div class="items">
                              <span class="bold"><?php echo $this->setting['mission_number_3'] ?></span>
                              <div class="clear"></div>
                              <p><?php echo $this->setting['mission_text_3'] ?></p>
                              <div class="clear height-5"></div>
                          </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-6">
                          <div class="items">
                              <span class="bold"><?php echo $this->setting['mission_number_4'] ?></span>
                              <div class="clear"></div>
                              <p><?php echo $this->setting['mission_text_4'] ?></p>
                              <div class="clear height-5"></div>
                          </div>
                        </div>
                      </div>
                      <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="clear height-50"></div>
                <div class="clear height-45"></div>
                <div class="clear height-5"></div>
            </div>
        </section>
        <div class="clear"></div>
        <section class="section_1 default-section-gema prelatife back-white our-next-mission">
            <div class="left_cont prelatife">
                <div class="container prelatife gema_container">

                    <div class="list-ourt-next-generation">
                      <div class="clear height-40"></div>
                      <div class="clear height-35"></div>
                      <h3>OUR NEXT DESTINATION</h3>
                      <div class="clear height-50"></div><div class="height-15"></div>
                      <div class="prelatife">
                          <div class="row">
                            <div class="transparan-left"></div>
                            <div id="owl-demo" class="owl-carousel owl-theme list-h-gallery">
<?php 
$destination = Destination::model()->findAll();
?>
                              <?php foreach ($destination as $key => $value): ?>
                                
                              <div class="item">
                                  <!-- <div class="col-md-3 col-sm-6"> -->
                                  <div class="col-md-12 col-sm-12">
                                    <div class="items">
                                        <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(187,187, '/images/destination/'.$value->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="pc-center img-responsive">
                                        <div class="clear height-15"></div>
                                        <p><?php echo $value->content ?></p>
                                        <div class="clear height-25"></div>
                                    </div>
                                  </div>
                              </div>
                              <?php endforeach ?>

                            </div>
                            <div class="transparan-right"></div>
                          </div>
                          <div class="btns-control">
                            <a href="javascript:;" class="chevron-left"><img src="<?php echo $this->assetBaseurl ?>chevron-left-gema.png" alt=""></a>
                            <a href="javascript:;" class="chevron-right"><img src="<?php echo $this->assetBaseurl ?>chevron-right-gema.png" alt=""></a>
                          </div>
                          <div class="clear"></div>
                      </div>

                      <div class="clear"></div>
                    </div>

                    <div class="clear"></div>
                </div>

                <div class="clear height-50"></div>
                <div class="clear height-40"></div>
            </div>
        </section>
        <div class="clear"></div>
        <section class="section_1 default-section-gema prelatife">
            <div class="left_cont prelatife">
                <div class="pict-full"><img src="<?php echo $this->assetBaseurl ?>gema/pict2-our-next-mission.jpg" alt=""></div>
            </div>
        </section>
        <div class="clear"></div>
      </div>
  <!-- </div> -->
</section>

<script type="text/javascript">
    $(document).ready(function(){
        var wbody = $(window).width();

        var heightHero = $('#carousel-examp-topPage img').height();
        if (wbody > 768){
            $('.PageGema_section .pabs-ins-middle-topim').css("height", heightHero+"px");
        };

        // set w
        var hsec1 = $('.section_1 img').height();
        $('.section_1 .table-out').css("height", hsec1+"px");
    });

    $(window).load(function(){
        var wbody = $(window).width();

        var heightHero = $('#carousel-examp-topPage img').height();
        if (wbody > 768){
            $('.PageGema_section .pabs-ins-middle-topim').css("height", heightHero+"px");
        };

        // set w
        var hsec1 = $('.section_1 img').height();
        $('.section_1 .table-out').css("height", hsec1+"px");
    });
</script>

<!-- Owl Carousel Assets -->
    <link href="<?php echo Yii::app()->baseUrl; ?>/asset/js/owl-carousel/owl.carousel.css" rel="stylesheet">
    <link href="<?php echo Yii::app()->baseUrl; ?>/asset/js/owl-carousel/owl.theme.css" rel="stylesheet">
    <script src="<?php echo Yii::app()->baseUrl; ?>/asset/js/owl-carousel/owl.carousel.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
     
      var owl = $("#owl-demo");
       
      owl.owlCarousel({
      itemsCustom : [
      [0, 1],
      [360, 1],
      [450, 1],
      [600, 2],
      [700, 2],
      [1000, 3],
      [1200, 3],
      [1400, 4],
      [1600, 4]
    ],
      itemsMobile : false, // itemsMobile disabled - inherit from itemsTablet option
      pagination: false,
      autoPlay: true,
      rewindSpeed: 1500,
      });
       
      // Custom Navigation Events
      $("a.chevron-right").click(function(){
      owl.trigger('owl.next');
      })
      $("a.chevron-left").click(function(){
      owl.trigger('owl.prev');
      })
     
    });
  </script>