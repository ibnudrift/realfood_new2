<section class="full-banner-top">
    <div class="tops-cont-insidepage back-pgema" style="background: none; height: auto; position: relative;"> 
         <div class="pict-fulls hidden-xs">
            <?php
            $slide = ViewSlide::model()->findAll('language_id = :language_id AND topik_id = 5 ORDER BY sort ASC', array(':language_id'=>$this->languageID));
            ?>
            <div id="carousel-examp-topPage" class="carousel fade" data-ride="carousel">
              <div class="carousel-inner" role="listbox">
                <?php foreach ($slide as $ke => $value): ?>
                <div class="item <?php if ($ke == 0): ?>active<?php endif ?>">
                  <!-- <img src="<?php // echo Yii::app()->baseUrl.ImageHelper::thumb(1450,680, '/images/slide/'.$value->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="<?php echo $value->title; ?>"> -->
                  <div class="fill" style="background-image:url(<?php echo Yii::app()->baseUrl.ImageHelper::thumb(1450,680, '/images/slide/'.$value->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>);"></div>
                </div>
                <?php endforeach ?>
              </div>
            </div>
            <div class="clear"></div>
        </div>
        <div class="visible-xs">
          <div id="carousel_ex_phone" class="carousel fade" data-ride="carousel">
              <div class="carousel-inner" role="listbox">
                <?php foreach ($slide as $ke => $value): ?>
                <div class="item <?php if ($ke == 0): ?>active<?php endif ?>">
                  <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(1450,680, '/images/slide/'.$value->image2 , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="<?php echo $value->title; ?>">                  
                </div>
                <?php endforeach ?>
              </div>
            </div>
          <div class="clear"></div>
        </div>
    </div> 
</section>
<section class="default_sc insides_middleDefaultpages" style="background:#EFEEE8;">
  <div class="prelatife container">
    <div class="clear height-50"></div>
    <div class="clear height-50"></div><div class="height-5"></div>
    <div class="content-text text-center">
        <p>Realfood.com customers may purchase our products in free quantity at <br> the dealer / distributor address below</p>

          <form action="" method="get">
            <label for="">Select City</label>
            <div class="row">
              <div class="col-md-4">
              </div>
              <div class="col-md-4">
              <select name="kota" id="select-kota" class="form-control">
                <option value="">Select City</option>
                <?php foreach ($listKota as $key => $value): ?>
                  <option value="<?php echo $value->kota ?>"><?php echo $value->kota ?></option>
                <?php endforeach ?>
              </select>
              <script type="text/javascript">
                $('#select-kota').val('<?php echo $_GET['kota'] ?>');
              </script>
              <div class="height-10"></div>
              <button type="submit" class="btn btn-primary">Submit</button>
              </div>
              <div class="col-md-4">
              </div>
            </div>
          </form>
        <div class="clear height-20"></div>

<?php if (count($dataAddress) > 0): ?>
        <div class="list_locaion_defaults_d">
<?php foreach ($dataAddress as $key => $value): ?>
          <div class="items">
            <div class="titles"><?php echo $key ?></div>
            <div class="clear height-20"></div>
<?php
$count_loc = count($value);
$val = array_chunk($value, 3);
?>
          <?php foreach ($val as $data_chunk): ?>
          <div class="row">
            <?php if ($count_loc == 2): ?>
            <div class="col-md-2"></div>
            <div class="col-md-8">
            <?php endif; ?>
            <?php foreach ($data_chunk as $k => $v): ?>
              <?php if ($count_loc == 1): ?>
              <div class="col-md-12 col-sm-12">
              <?php elseif($count_loc == 2): ?>
              <div class="col-md-6 col-sm-6">
              <?php else: ?>
              <div class="col-md-4 col-sm-4">
              <?php endif ?>
                <div class="item">
                  <p><b><?php echo $v->nama ?></b> <br>
                    <?php echo $v->address_1 ?><br />
                    <?php if ($v->address_2 != ''): ?>
                      <?php echo nl2br($v->address_2) ?><br />
                    <?php endif ?>
                  <?php if ($v->telp != ''): ?>
                  P. <?php echo $v->telp ?><br />
                  <?php endif ?>
                  <?php if ($v->fax != ''): ?>
                  F. <?php echo $v->fax ?> <br>
                  <?php endif ?>
                  <?php if ($v->email != ''): ?>
                  E. <?php echo $v->email ?>
                  <?php endif ?>
                  </p>
                  <div class="clear"></div>
                </div>
              </div>


            <?php endforeach ?>
              <?php if ($count_loc == 2): ?>
              </div>
              <div class="col-md-2"></div>
              <?php endif ?>
          </div>
          <?php endforeach ?>
            <div class="clear"></div>
          </div>
<?php endforeach ?>

          <div class="clear"></div>
        </div>
<?php endif ?>
        <!-- end list download item -->
        <div class="clear height-50"></div>

        <div class="clear height-30"></div>
      <div class="clear"></div>
    </div>

    <div class="clear"></div>
  </div>
</section>
<?php /*
<section class="outer-inside-middle-content back-white" style="background:#EFEEE8;">
  <div class="prelatife container">
    <?php /*
    <div class="tops-cont-insidepage back-contacthero" style="background: none; height: auto; position: relative;"> <!-- style="background-image: url(<?php // echo Yii::app()->baseUrl; ?>/images/static/<?php echo $this->setting['contact_hero_image']; ?>)" -->
        <div class="pict-fulls">
            <?php
            $slide = ViewSlide::model()->findAll('language_id = :language_id AND topik_id = 5 ORDER BY sort ASC', array(':language_id'=>$this->languageID));
            ?>
            <div id="carousel-examp-topPage" class="carousel slide" data-ride="carousel">
              <div class="carousel-inner" role="listbox">
                <?php foreach ($slide as $ke => $value): ?>
                <div class="item <?php if ($ke == 0): ?>active<?php endif ?>">
                  <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(1450,680, '/images/slide/'.$value->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="<?php echo $value->title; ?>">
                </div>
                <?php endforeach ?>
              </div>
            </div>

        </div>

        <div class="pabs-ins-middle-topim">
          <div class="tengah insd-container text-center content-up visible-lg">
              <div class="visible-lg">
                <div class="clear height-50"></div>
                <div class="height-50"></div>
                <div class="height-50"></div>
                <div class="height-25"></div>
              </div>
              <?php if ($this->setting['contact_hide'] == 0): ?>
              <h1 class="title-pages"><?php echo $this->setting['contact_title'] ?></h1> <div class="clear height-10"></div>
              <div class="lines-chld-bgrey tengah"></div> <div class="clear height-25"></div>
              <?php endif ?>
              <span class="bigs"><?php echo $this->setting['contact_big_title'] ?></span> <div class="clear"></div>
              <p><?php echo nl2br($this->setting['contact_hero_content']) ?></p>
              <div class="clear"></div>
          </div>
          <div class="clear"></div>
        </div>
    </div>
    * / ?>

    <div class='pindahan_text-heroimage hidden-lg'>
        <div class="clear height-50"></div>
      <?php if ($this->setting['contact_hide'] == 0): ?>
          <h1 class="title-pages"><?php echo $this->setting['contact_title'] ?></h1> <div class="clear height-10"></div>
          <div class="lines-chld-bgrey tengah"></div> <div class="clear height-25"></div>
          <?php endif ?>
          <span class="bigs"><?php echo $this->setting['contact_big_title'] ?></span> <div class="clear"></div>
          <p><?php echo nl2br($this->setting['contact_hero_content']) ?></p>
          <div class="clear"></div>
    </div>
    <div class="prelatife container containerfix">
        <div class="clear height-30 visible-lg"></div>
        <?php 
        $jumlah_peta = count(Factory::model()->findAll()) - 1;
        ?>
        <?php foreach (Factory::model()->findAll() as $key => $value): ?>
        <div class="list-cont-maps h389" <?php if ($jumlah_peta == $key): ?>style="margin-bottom: 0px;"<?php endif ?>>
          <div class="inside">
                <div class="row">
                  <div class="col-md-3">
                      <div class="inset">
                        <h3><?php echo $value->nama ?></h3> <div class="clear"></div>
                        <div class="lines-green"></div>
                        <div class="clear height-20"></div>
                        <p><?php echo nl2br($value->alamat) ?></p>
                        <p class="d-info-telp"><img src="<?php echo $this->assetBaseurl; ?>icon-phone-ct.png" alt="" class="inline-pict">&nbsp;&nbsp;&nbsp;<?php echo $value->telp ?> <br>
                        <img src="<?php echo $this->assetBaseurl; ?>icon-contact-ct.png" alt="" class="inline-pict">&nbsp;&nbsp;&nbsp;<a href="mailto:<?php echo $value->email ?>"><?php echo $value->email ?></a>
                        <?php if ( $value->wa ): ?>
                        <br>
                        <img src="<?php echo $this->assetBaseurl; ?>icon-wa-ct.png" alt="" class="inline-pict">&nbsp;&nbsp;&nbsp;<?php echo $value->wa ?><!-- +62 812-3208-5678 -->
                        <?php endif ?>
                      </p>
                        <div class="clear"></div>
                      </div>
                  </div>
                  <div class="col-md-9">
                      <div class="back-viewmapss-location">
                        <iframe src="<?php echo $value->peta ?>" width="930" height="388" frameborder="0" style="border:0" allowfullscreen></iframe>
                        <div class="clear"></div>
                      </div>
                  </div>
                  <div class="clear"></div>
                </div>
            <div class="clear"></div>
          </div>
          <div class="clear"></div>
        </div>
        <?php if ($jumlah_peta == $key): ?>
          <div class="clear height-0"></div>
        <?php else: ?>
          <div class="clear height-30"></div>
        <?php endif ?>
        <?php endforeach ?>

        <!-- <div class="clear height-50"></div><div class="height-20"></div> -->
        <div class="clear height-5"></div>
        <div class="shares-text text-center">
            <span class="inline-t">SHARE</span>&nbsp;&nbsp; / &nbsp;&nbsp;<a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode(Yii::app()->request->hostInfo.Yii::app()->request->url) ?>">FACEBOOK</a>&nbsp;&nbsp; / &nbsp;&nbsp;<a target="_blank" href="https://plus.google.com/share?url=<?php echo urlencode(Yii::app()->request->hostInfo.Yii::app()->request->url) ?>">GOOGLE PLUS</a>&nbsp;&nbsp; / &nbsp;&nbsp;<a target="_blank" href="https://twitter.com/home?status=<?php echo urlencode(Yii::app()->request->hostInfo.Yii::app()->request->url) ?>">TWITTER</a>
          </div>
          <div class="clear"></div>
    </div>

    <!-- <div class="clear height-20"></div>
    <div class="clear height-50"></div> -->
  </div>
</section>
*/ ?>