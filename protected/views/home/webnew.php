<!-- Start fcs -->
<?php
$slide = ViewSlide::model()->findAll('language_id = :language_id AND topik_id = 0 ORDER BY sort ASC', array(':language_id' => $this->languageID));
?>
<div class="fcs-wrapper outers_fcs_wrapper prelatife wrapper-slide home">
    <div id="myCarousel_home" class="carousel carousel-fade" data-ride="carousel" data-interval="6000">
        <div class="carousel-inner">
            <?php foreach ($slide as $key => $value) : ?>
                <div class="carousel-item <?php if ($key == 0) : ?>active<?php endif ?> home-slider-new">
                    <img class="w-100 d-none d-sm-block" src="<?php echo Yii::app()->baseUrl . ImageHelper::thumb(1920, 1078, '/images/slide/' . $value->image, array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="<?php echo $value->title ?>">
                    <img class="w-100 d-block d-sm-none" src="<?php echo Yii::app()->baseUrl; ?>/images/slide/<?php echo $value->image2 ?>" alt="<?php echo $value->title ?>">
                    <?php if ($value->url != ''): ?>
                    <div class="to_bannerclick" onclick="window.open('<?php echo $value->url ?>', '_blank');"></div>
                    <?php endif ?>

                    <?php if ($value->subtitle != ''): ?>
                    <div class="carousel-caption caption-slider-home mx-auto">
                        <div class="prelative container mx-auto">
                            <div class="bxsl_tx_fcs">
                                <div class="row no-gutters">
                                    <div class="col-md-60">
                                        <?php echo $value->subtitle ?>
                                        <div class="subtitle py-4">
                                            <?php echo $value->content ?>
                                            <div class="pt-5"></div>
                                            <a href="<?php echo $value->url ?>">LEARN MORE</a>
                                        </div>
                                    </div>
                                    <div class="col-md-27"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php endif ?>

                </div>
            <?php endforeach ?>
        </div>
        <a class="carousel-control-prev" href="#myCarousel_home" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control-next" href="#myCarousel_home" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        <?php /*<ol class="carousel-indicators">
                <li data-target="#myCarousel_home" data-slide-to="" class="active"></li>
            </ol>*/ ?>
    </div>
    <div class="clear-both"></div>
</div>
<!-- End fcs -->

<div class="pb-5"></div>
<div class="pb-5"></div>

<section class="home-sec-baru">
    <div class="prelative container">
        <div class="row">
            <?php
            $criteria = new CDbCriteria;
            $criteria->with = array('description');
            $criteria->addCondition('t.cover_home_prd = "1"');
            $criteria->addCondition('description.language_id = :language_id');
            $criteria->params[':language_id'] = $this->languageID;
            $criteria->limit = 3;
            $criteria->order = 't.sort ASC';
            $data_category = PrdCategory::model()->findAll($criteria);
            ?>
            <?php if (count($data_category) > 0) : ?>
                <?php foreach ($data_category as $key => $value) : ?>
                    <!-- // landscape -->
                    <div class="col-md-60 d-none d-sm-block">
                        <div class="image">
                            <a href="<?php echo CHtml::normalizeUrl(array('/product/index', 'category' => $value->id, 'slug'=> Slug::Create($value->description->name),  'lang'=>Yii::app()->language)); ?>">
                                <img class="w-100" src="<?php echo Yii::app()->baseUrl; ?>/images/category/<?php echo $value->benefit_image ?>" alt="fit with realfood">
                            </a>
                        </div>
                        <div class="pb-5"></div>
                    </div>
                    <div class="col-md-30 d-block d-sm-none">
                        <div class="image">
                            <a href="<?php echo CHtml::normalizeUrl(array('/product/index', 'category' => $value->id, 'slug'=> Slug::Create($value->description->name), 'lang'=>Yii::app()->language)); ?>">
                                <img class="w-100" src="<?php echo Yii::app()->baseUrl; ?>/images/category/<?php echo $value->benefit_image_1 ?>" alt="<?php echo $value->description->name ?>">
                            </a>
                        </div>
                        <div class="pb-5"></div>
                    </div>
                <?php endforeach ?>
            <?php endif ?>
        </div>
    </div>
    <div class="pb-5 d-none d-sm-block"></div>
    <div class="pb-5 d-none d-sm-block"></div>
</section>

<section class="home-sec-5">
    <div class="prelative container">
        <div class="pt-5"></div>
        <!-- <div class="pt-5"></div> -->

        <div class="row">
            <div class="col-md-60">
                <div class="header-sec">
                    <?php echo $this->setting['home3_title'] ?>
                    <div class="pt-3"></div>
                    <?php echo $this->setting['home3_content'] ?>
                    <div class="pb-5 d-none d-sm-block"></div>
                </div>
            </div>
        </div>
        <div class="pt-5 d-none d-sm-block"></div>
        <div class="row">
            <?php for ($i = 1; $i < 5; $i++) { ?>
                <div class="col-md-15 box-shadow-new">
                    <img class="mx-auto text-center d-block img img-fluid" src="<?php echo Yii::app()->baseUrl; ?>/images/static/<?php echo $this->setting['homes_ig_data_picture_' . $i] ?>" alt="fit with realfood">
                    <div class="box-sec-5">
                        <div class="text">
                            <p><?php echo $this->setting['homes_ig_data_text_' . $i] ?></p>
                        </div>
                        <div class="ig pt-3">
                            <p><?php echo $this->setting['homes_ig_data_iguser_' . $i] ?></p>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
        <div class="pt-5"></div>
        <!-- <div class="pt-5"></div> -->
        <!-- <div class="row">
            <div class="col-md-60">
                <div class="shop">
                    <a target="_blank" href="<?php echo $this->setting['url_instagram'] ?>">VISIT OUR INSTAGRAM</a>
                </div>
            </div>
        </div> -->
        <!-- <div class="pb-5"></div> -->
        <!-- <div class="pb-5"></div> -->
    </div>
    <div class="pb-5 d-none d-sm-block"></div>
    <div class="pb-5"></div>
</section>

<section class="home-sec-6">
    <div class="row no-gutters">
        <?php
        $criteria = new CDbCriteria;
        $criteria->with = array('desc');
        $criteria->addCondition('desc.language_id = :language_id');
        $criteria->params[':language_id'] = $this->languageID;
        $criteria->order = 't.id DESC';
        $criteria->limit = 6;

        $allblog = Blog::model()->findAll($criteria);
        ?>
        <?php foreach ($allblog as $key => $value) : ?>
            <div class="col-md-20">
                <div class="container-box">
                    <img class="img img-fluid" src="<?php echo Yii::app()->baseUrl . ImageHelper::thumb(640, 341, '/images/blog/' . $value->image, array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="<?php echo $value->desc->title ?>">
                    <div class="middle">
                        <div class="text pl-0 pr-0 py-2"><?php echo $value->desc->title ?></div>
                        <a href="<?php echo CHtml::normalizeUrl(array('/blog/details', 'id' => $value->id, 'slug'=> Slug::Create($value->desc->title), 'lang'=>Yii::app()->language)); ?>">Read Article</a>
                        <?php // echo CHtml::normalizeUrl(array('/blog/detail', 'id'=> $value->id, 'name'=>Slug::Create($value->desc->title) )); 
                        ?>
                    </div>
                    <div class="kotakhijau">

                    </div>
                </div>
            </div>
        <?php endforeach ?>

    </div>
</section>