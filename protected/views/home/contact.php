<?php 
if ($this->setting['seo_contact_titles']) {
  $this->pageTitle = $this->setting['seo_contact_titles'];
}
if ($this->setting['seo_contact_keyword']) {
  $this->metaKey = $this->setting['seo_contact_keyword'];
}
if ($this->setting['seo_contact_description']) {
  $this->metaDesc = $this->setting['seo_contact_description'];
}

?>
<!-- Start fcs -->
<?php
$slide = ViewSlide::model()->findAll('language_id = :language_id AND topik_id = 4 ORDER BY sort ASC', array(':language_id'=>$this->languageID));
?>
<div class="fcs-wrapper outers_fcs_wrapper prelatife wrapper-slide faq">
    <div id="myCarousel_home" class="carousel carousel-fade" data-ride="carousel" data-interval="4500">
            <div class="carousel-inner">
                <?php foreach ($slide as $key => $value): ?>
                <div class="carousel-item <?php if ($key == 0): ?>active<?php endif ?> home-slider-new">
                <img class="w-100 d-none d-sm-block" src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(1920, 1078, '/images/slide/'.$value->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="First slide">
                <img class="w-100 d-block d-sm-none" src="<?php echo Yii::app()->baseUrl; ?>/images/slide/<?php echo $value->image2 ?>" alt="First slide">
                    <div class="carousel-caption caption-slider-home mx-auto">
                        <div class="prelative container mx-auto">
                            <div class="bxsl_tx_fcs">
                                <div class="row no-gutters">
                                    <div class="col-md-60">
                                        <div class="pt-3"></div>
                                        <p><?php echo $value->subtitle ?></p>
                                        <div class="pt-3"></div>
                                        <div class="line-insides"></div>
                                        <div class="subtitle py-4">
                                            <?php echo $value->content ?>
                                            <div class="pt-5"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-27"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endforeach ?>
            </div>
            <a class="carousel-control-prev" href="#myCarousel_home" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control-next" href="#myCarousel_home" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
            <?php /*<ol class="carousel-indicators">
                <li data-target="#myCarousel_home" data-slide-to="" class="active"></li>
            </ol>*/ ?>
    </div>
    <div class="clear-both"></div>
</div>
<!-- End fcs -->

<section class="contact-sec-1" <?php if (($this->setting['contact1_back'])): ?>style="background-image: url(<?php echo Yii::app()->baseUrl; ?>/images/static/<?php echo $this->setting['contact1_back']; ?>)"<?php endif ?>>
    <div class="prelative container">
        <div class="row">
            <div class="col-md-30">
                <div class="content">
                    <div class="title">
                        <!-- <p>onio</p> -->
                        <?php echo $this->setting['contact1_title'] ?>
                        <?php echo $this->setting['contact1_content'] ?>
                        <div class="py-2"></div>
                        <!-- Call Us Our Hotline. <br> -->
                        <p>
                        Whatsapp. <?php echo $this->setting['contact_wa'] ?><br>
                        Line.  <?php echo $this->setting['contact_lines'] ?>
                        </p>
                    </div>
                    <form method="post" action="<?php echo CHtml::normalizeUrl(array('/home/contact')); ?>">
                        <div class="form-group">
                            <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Full Name">
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Email">
                        </div>
                        <div class="form-group">
                            <textarea class="form-control" name="" id="" cols="" rows="6" placeholder="Message"></textarea>
                        </div>
                        <div class="g-recaptcha" data-sitekey="6LfsiLUUAAAAANpsRfvWqg2c7woCVEjBD6srQzEP"></div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
            <div class="col-md-30">
                
            </div>
        </div>
    </div>
</section>

<section class="contact-sec-2">
    <div class="inners-table">
        <div class="prelative container">
            <div class="row">
                <div class="col-md-30">
                    <div class="content p-5 py-3">
                        <div class="title">
                            <p>GET IN TOUCH</p>
                        </div>
                        <div class="subtitle">
                            <p>Discover our Realfood headquarter ofﬁce at Bojonegoro, Indonesia.</p>
                        </div>
                        <div class="py-5 d-none d-sm-block"></div>
                        <div class="py-3"></div>
                        <div class="row">
                            <div class="col-md-38">
                                <div class="bawah-kotak">
                                    <p>
                                        <?php echo nl2br($this->setting['contact_address1']) ?>
                                    </p>
                                </div>
                                <div class="py-2"></div>
                            </div>
                            <div class="col-md-22">
                                <div class="view"><a target="_blank" href="<?php echo $this->setting['contact_address_map'] ?>">View Map</a></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-30">

                </div>
            </div>
        </div>
    </div>
</section>

<section class="contacts_branchf py-5">
    <div class="prelative container my-5">
        
        <div class="text-center inners_section ">
            <h3 class="m-0">REAL FOOD BRANCHES</h3>
            <div class="py-4"></div>
            <div class="py-2"></div>
            <div class="d-block mx-auto mw760 lists_block_address">
                <div class="row">
                    <div class="col-md-30">
                        <div class="texts">
                            <h4>SURABAYA</h4>
                            <p><?php echo nl2br($this->setting['contact_address_sby']) ?></p>
                        </div>
                    </div>
                    <div class="col-md-30">
                        <div class="texts">
                            <h4>JABODETABEK</h4>
                            <p><?php echo nl2br($this->setting['contact_address_jabotabek']) ?></p>
                        </div>
                    </div>
                    
                </div>
                <div class="clear clearfix"></div>
            </div>

            <div class="clear"></div>
        </div>

        <div class="clear"></div>
    </div>
</section>

<script src="https://www.google.com/recaptcha/api.js" async defer></script>