<!-- Start fcs -->
<?php
$slide = ViewSlide::model()->findAll('language_id = :language_id AND topik_id = 1 ORDER BY sort ASC', array(':language_id'=>$this->languageID));
?>
<div class="fcs-wrapper outers_fcs_wrapper prelatife wrapper-slide home">
    <div id="myCarousel_home" class="carousel carousel-fade" data-ride="carousel" data-interval="4500">
            <div class="carousel-inner">
                <?php foreach ($slide as $key => $value): ?>
                <div class="carousel-item <?php if ($key == 0): ?>active<?php endif ?> home-slider-new">
                <img class="w-100 d-none d-sm-block" src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(1920, 1078, '/images/slide/'.$value->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="First slide">
                <img class="w-100 d-block d-sm-none" src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(375, 667, '/images/slide/'.$value->image2 , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="First slide">
                    <div class="carousel-caption caption-slider-home mx-auto">
                        <div class="prelative container mx-auto">
                            <div class="bxsl_tx_fcs">
                                <div class="row no-gutters">
                                    <div class="col-md-60">
                                        <?php echo $value->subtitle ?>
                                        <div class="subtitle py-4">
                                            <?php echo $value->content ?>
                                            <div class="pt-5"></div>
                                            <a href="<?php echo $value->url ?>">LEARN MORE</a>
                                        </div>
                                    </div>
                                    <div class="col-md-27"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endforeach ?>
            </div>
            <?php /*<ol class="carousel-indicators">
                <li data-target="#myCarousel_home" data-slide-to="" class="active"></li>
            </ol>*/ ?>
    </div>
    <div class="clear-both"></div>
</div>
<!-- End fcs -->

<section class="story-sec-1" style="background-image: url(<?php echo Yii::app()->baseUrl; ?>/images/static/<?php echo $this->setting['about1_image']; ?>)">
	<div class="row">
		<div class="col-md-30">
			
		</div>
		<div class="col-md-30">
			<div class="content">
				<div class="title">
					<?php echo $this->setting['about1_title'] ?>
				</div>
				<div class="inside">
					<?php echo $this->setting['about1_content'] ?>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="story-sec-2">
	<div class="prelative container">
		<div class="row">
			<div class="col-md-60">
				<div class="image"><img src="<?php echo Yii::app()->baseUrl; ?>/asset/images/new/(Clip-Group).png" alt=""></div>
			</div>
			<div class="col-md-30">
				<div class="title">
					<p>Vision & Mission</p>
				</div>
				<?php for ($i=1; $i < 4; $i++) { ?>
				<div class="content">
					<div class="judul">
						<p><?php echo $this->setting['about2_vision_con_title_'. $i] ?></p>
					</div>
					<div class="isi">
						<p><?php echo $this->setting['about2_vision_con_text_'. $i] ?></p>
					</div>
				</div>
				<?php } ?>

			</div>
			<div class="col-md-30">
				<div class="title">
					<p>Our Values</p>
				</div>
				<?php for ($i=1; $i < 5; $i++) { ?>
				<div class="content">
					<div class="judul">
						<p><?php echo $this->setting['about2_vision_valu_title_'. $i] ?></p>
					</div>
					<div class="isi">
						<p><?php echo $this->setting['about2_vision_valu_text_'. $i] ?></p>
					</div>
				</div>
				<?php } ?>

			</div>
		</div>
	</div>
</section>

<section class="story-sec-3" style="background-image: url(<?php echo Yii::app()->baseUrl; ?>/images/static/<?php echo $this->setting['about3_picture']; ?>)">
	<div class="content">
		<?php echo $this->setting['about3_content'] ?>
	</div>
</section>

<section class="story-sec-4">
	<div class="row no-gutters">
		<div class="col-md-30">
			<div class="content-outer">
				<div class="content-inner">
					<div class="title">
						<p><?php echo $this->setting['about4_nx_titles_1'] ?></p>
					</div>
					<div class="insides">
						<?php echo $this->setting['about4_nx_content_1'] ?>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-30">
			<div class="gambar">
				<img class="w-100 img img-fluid" src="<?php echo Yii::app()->baseUrl; ?>/images/static/<?php echo $this->setting['about4_nx_picture_1'] ?>" alt="">
			</div>
		</div>
	</div>

	<div class="row no-gutters">
		<div class="col-md-30">
			<div class="gambar">
				<img class="w-100 img img-fluid" src="<?php echo Yii::app()->baseUrl; ?>/images/static/<?php echo $this->setting['about4_nx_picture_2'] ?>" alt="">
			</div>
		</div>
		<div class="col-md-30">
			<div class="content-outer">
				<div class="content-inner">
					<div class="title">
						<p><?php echo $this->setting['about4_nx_titles_2'] ?></p>
					</div>
					<div class="insides">
						<?php echo $this->setting['about4_nx_content_2'] ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="story-sec-5">
	<div class="prelative container">
		<div class="row">
			<div class="col-md-30">
				<div class="content inners_bloc">
					<!-- <div class="inners_bloc">
						<p>MEET OUR FOUNDER IN CHIEF</p>
					</div> -->
					<div class="nama">
						<?php echo $this->setting['about4_bottom_titles'] ?>
					</div>
					<!-- <div class="founder">
						<p>Founder in Chief</p>
					</div> -->
					<div class="py-3"></div>
					<div class="inside">
						<?php echo $this->setting['about4_bottom_content'] ?>
					</div>
				</div>
			</div>
			<div class="col-md-30">
				
			</div>
		</div>
	</div>
</section>

<section class="story-sec-6">
	<div class="row no-gutters">
		<div class="col-md-30">
			<div class="gambar">
				<img class="w-100" src="<?php echo Yii::app()->baseUrl; ?>/images/static/<?php echo $this->setting['about5_nx_picture_1'] ?>" alt="">
			</div>
		</div>
		<div class="col-md-30">
			<div class="content-outer">
				<div class="content-inner">
					<div class="image-title text-right">
						<img class="" src="<?php echo Yii::app()->baseUrl; ?>/images/static/<?php echo $this->setting['about5_nx_sublogo_1'] ?>" alt="">
					</div>
					<div class="insides text-center">
						<img src="<?php echo Yii::app()->baseUrl; ?>/images/static/<?php echo $this->setting['about5_nx_subimage_1'] ?>" alt="">
						<b><?php echo $this->setting['about5_nx_titles_1'] ?></b>
						<?php echo $this->setting['about5_nx_content_1'] ?>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row no-gutters">
		<div class="col-md-30">
			<div class="content-outer">
				<div class="content-inner">
					<div class="image-title text-left">
						<img class="" src="<?php echo Yii::app()->baseUrl; ?>/images/static/<?php echo $this->setting['about5_nx_sublogo_2'] ?>" alt="">
					</div>
					<div class="insides text-center">
						<img src="<?php echo Yii::app()->baseUrl; ?>/images/static/<?php echo $this->setting['about5_nx_subimage_2'] ?>" alt="">
						<b><?php echo $this->setting['about5_nx_titles_2'] ?></b>
						<?php echo $this->setting['about5_nx_content_2'] ?>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-30">
			<div class="gambar">
				<img class="w-100" src="<?php echo Yii::app()->baseUrl; ?>/images/static/<?php echo $this->setting['about5_nx_picture_2'] ?>" alt="">
			</div>
		</div>
	</div>

	<div class="row no-gutters">
		<div class="col-md-30">
			<div class="gambar">
				<img class="w-100" src="<?php echo Yii::app()->baseUrl; ?>/images/static/<?php echo $this->setting['about5_nx_picture_3'] ?>" alt="">
			</div>
		</div>
		<div class="col-md-30">
			<div class="content-outer">
				<div class="content-inner">
					<div class="image-title text-right">
						<img class="" src="<?php echo Yii::app()->baseUrl; ?>/images/static/<?php echo $this->setting['about5_nx_sublogo_3'] ?>" alt="">
					</div>
					<div class="insides text-center">
						<img src="<?php echo Yii::app()->baseUrl; ?>/images/static/<?php echo $this->setting['about5_nx_subimage_3'] ?>" alt="">
						<b><?php echo $this->setting['about5_nx_titles_3'] ?></b>
						<?php echo $this->setting['about5_nx_content_3'] ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>