<section class="outer-inside-middle-content back-white">
  <div class="prelatife container">
    <div class="tops-cont-insidepage back-products prelatife"> <div class="clear height-50"></div>
        <div class="height-50"></div>
        <div class="height-50"></div>
        <div class="height-25"></div>
        <div class="tengah insd-container mw930 text-center content-up">
          <h1 class="title-pages">PRODUCTS</h1> <div class="clear height-10"></div>
          <div class="lines-chld-bgrey tengah"></div> <div class="clear height-25"></div>
          <span class="bigs">SAVE PEOPLE WHILE BEING HEALTHY</span> <div class="clear"></div>
        <div class="clear"></div>
      </div>
      <!-- <div class="pos-abs-bottmful">
        <div class="pic-center"><img src="<?php echo $this->assetBaseurl; ?>back-product-ptrn-topinside.png" alt="" class="img-responsive"></div>
      </div> -->
      
      <div class="clear"></div>
    </div>

    <div class="clear"></div>
  </div>

  <div class="back-grey mh500">
    <div class="prelatife container">
        <div class="clear height-50"></div>
        <div class="height-40"></div>

        <div class="outers-listing-dataproducts">
          <div class="row">
            <div class="col-md-4">
              <div class="itemss">
                <div class="picts"><img src="<?php echo $this->assetBaseurl; ?>pri-product-clasic.png" alt=""></div>
                <div class="clear height-30"></div>
                <div class="descs">
                  <div class="title">classic</div> <div class="clear height-15"></div><div class="height-2"></div>
                    <p>ENRICH YOUR LIFE <br> <b>PROPER EDUCATION FOR THEM</b></p>
                    <p>For every can of Classic Realfit, you will contribute IDR 2,500 for educational funds of children in remote villages of Indonesia.</p>
                    <div class="clear"></div>
                    <div class="blocks-middleprice">
                      <span>IDR 25,000</span>
                    </div> <div class="clear height-25"></div>
                </div>
              </div> <!-- end items -->
            </div>
            <div class="col-md-4">
              <div class="itemss">
                <div class="picts"><img src="<?php echo $this->assetBaseurl; ?>pri-product-beauty.png" alt=""></div>
                <div class="clear height-30"></div>
                <div class="descs">
                  <div class="title">beauty</div> <div class="clear height-15"></div><div class="height-2"></div>
                    <p>YOUR ANTI-AGING SUPPLEMENT<br> <b>PROPER CLOTHING FOR THEM</b></p>
                    <p>For every can of Classic Realfit, you will contribute IDR 2,500 for educational funds of children in remote villages of Indonesia.</p>
                    <div class="clear"></div>
                    <div class="blocks-middleprice">
                      <span>IDR 25,000</span>
                    </div> <div class="clear height-25"></div>
                </div>
              </div> <!-- end items -->
            </div>
            <div class="col-md-4">
              <div class="itemss">
                <div class="picts"><img src="<?php echo $this->assetBaseurl; ?>pri-product-immune.png" alt=""></div>
                <div class="clear height-30"></div>
                <div class="descs">
                  <div class="title">immune</div> <div class="clear height-15"></div><div class="height-2"></div>
                    <p>PROTECT YOUR BODY<br> <b>CLEAN WATER FOR THEM</b></p>
                    <p>For every can of Classic Realfit, you will contribute IDR 2,500 for educational funds of children in remote villages of Indonesia.</p>
                    <div class="clear"></div>
                    <div class="blocks-middleprice">
                      <span>IDR 25,000</span>
                    </div> <div class="clear height-25"></div>
                </div>
              </div> <!-- end items -->
            </div>
          </div>

        </div>
        <!-- end outer products --> <div class="clear height-50"></div><div class="height-50"></div>
        <div class="mw-783 tengah text-center content-text">
          <div class="lines-grey h3"></div>
            <div class="clear height-35"></div><div class="height-5"></div>
            <h3>about Realfood&rsquo;s product contribution</h3>
            <p>Curabitur viverra odio velit, in tempor diam semper quis. In porttitor ipsum sed nisi bibendum, non convallis enim dignissim. Maecenas id eros at purus facilisis interdum eget sed mauris. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec scelerisque fermentum justo, elementum porttitor purus ultrices non. Duis consectetur in libero at dapibus. Quisque vel nisi et tortor sit eget risus. Donec accumsan sem eu fermentum suscipit.</p>
            <div class="clear height-25"></div>
            <div class="shares-text">
                  <span class="inline-t">SHARE</span>&nbsp;&nbsp; / &nbsp;&nbsp;<a href="#">FACEBOOK</a>&nbsp;&nbsp; / &nbsp;&nbsp;<a href="#">GOOGLE PLUS</a>&nbsp;&nbsp; / &nbsp;&nbsp;<a href="#">TWITTER</a>
                </div>
          
            <div class="clear"></div>
        </div>
      
      <div class="clear"></div> 
      <div class="height-50"></div>
      <div class="height-50"></div>
      <div class="height-20"></div>
    </div>
  </div>
</section>