<?php 
if ($this->setting['seo_gema_titles']) {
  $this->pageTitle = $this->setting['seo_gema_titles'];
}
if ($this->setting['seo_gema_keyword']) {
  $this->metaKey = $this->setting['seo_gema_keyword'];
}
if ($this->setting['seo_gema_description']) {
  $this->metaDesc = $this->setting['seo_gema_description'];
}

?>
<!-- Start fcs -->
<?php
$slide = ViewSlide::model()->findAll('language_id = :language_id AND topik_id = 7 ORDER BY sort ASC', array(':language_id' => $this->languageID));
?>
<div class="fcs-wrapper outers_fcs_wrapper prelatife wrapper-slide home">
    <div id="myCarousel_home" class="carousel carousel-fade" data-ride="carousel" data-interval="4500">
            <div class="carousel-inner">
                <?php foreach ($slide as $key => $value): ?>
                <div class="carousel-item <?php if ($key == 0): ?>active<?php endif ?> home-slider-new">
                <img class="w-100 d-none d-sm-block" src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(1920, 1078, '/images/slide/'.$value->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="First slide">
                <img class="w-100 d-block d-sm-none" src="<?php echo Yii::app()->baseUrl; ?>/images/slide/<?php echo $value->image2 ?>" alt="First slide">
                    <div class="carousel-caption caption-slider-home mx-auto">
                        <div class="prelative container mx-auto">
                            <div class="bxsl_tx_fcs">
                                <div class="row no-gutters">
                                    <div class="col-md-60">
                                        <?php echo $value->subtitle ?>
                                        <div class="subtitle py-4">
                                            <?php echo $value->content ?>
                                            <div class="pt-5"></div>
                                            <!-- <a href="<?php echo $value->url ?>">LEARN MORE</a> -->
                                        </div>
                                    </div>
                                    <div class="col-md-27"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endforeach ?>
            </div>
            <a class="carousel-control-prev" href="#myCarousel_home" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
              </a>
              <a class="carousel-control-next" href="#myCarousel_home" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
              </a>
            <?php /*<ol class="carousel-indicators">
                <li data-target="#myCarousel_home" data-slide-to="" class="active"></li>
            </ol>*/ ?>
    </div>
    <div class="clear-both"></div>
</div>
<!-- End fcs -->

<section class="outers-block-gemaa static_page">
    <div class="blocksn_top_grey py-5">
        <div class="prelative container py-5">
            <?php 
            $ndata_numbers = [
                            1 => [
                                'titles'=>$this->setting['gema1_itm_title_1'],
                                'city'=>$this->setting['gema1_itm_titlesmall_1'],
                                'desc'=>$this->setting['gema1_itm_descs_1'],
                                ],
                                [
                                'titles'=>$this->setting['gema1_itm_title_2'],
                                'city'=>$this->setting['gema1_itm_titlesmall_2'],
                                'desc'=>$this->setting['gema1_itm_descs_2'],
                                ],
                                [
                                'titles'=>$this->setting['gema1_itm_title_3'],
                                'city'=>$this->setting['gema1_itm_titlesmall_3'],
                                'desc'=>$this->setting['gema1_itm_descs_3'],
                                ],
                            ];
            ?>
            <div class="inner-section text-center">
                <h5><?php echo $this->setting['gema1_title'] ?></h5>
                <div class="py-4 mb-2"></div>
                <div class="row lists_top_gma">

                    <?php foreach($ndata_numbers as $key => $value): ?>
                    <div class="col-md-20">
                        <div class="items">
                            <div class="titles">
                                <span><?php echo $value['titles'] ?></span>
                                <div class="d-block clear clearfix"></div>
                                <small><?php echo $value['city'] ?></small>
                            </div>
                            <div class="desc py-3">
                                <p><?php echo $value['desc'] ?></p>
                            </div>
                        </div>
                    </div>
                    <?php endforeach; ?>

                </div>
                <div class="clear"></div>
            </div>
        </div>
    </div>

    <div class="blocks_inners back-white py-5">
        <div class="prelative container py-5 my-2">
            <?php 
            $datas = [
                    1 => [
                        'picture'=>Yii::app()->baseUrl.ImageHelper::thumb(561,339, '/images/static/'. $this->setting['gema_sect_pict_1'] , array('method' => 'adaptiveResize', 'quality' => '90')),
                        'title'=>$this->setting['gema_sect_title_1'],
                        'desc'=>$this->setting['gema_sect_content_1'],
                        ],
                        [
                        'picture'=>Yii::app()->baseUrl.ImageHelper::thumb(561,339, '/images/static/'. $this->setting['gema_sect_pict_2'] , array('method' => 'adaptiveResize', 'quality' => '90')),
                        'title'=>$this->setting['gema_sect_title_2'],
                        'desc'=>$this->setting['gema_sect_content_2'],
                        ],
                        [
                        'picture'=>Yii::app()->baseUrl.ImageHelper::thumb(561,339, '/images/static/'. $this->setting['gema_sect_pict_3'] , array('method' => 'adaptiveResize', 'quality' => '90')),
                        'title'=>$this->setting['gema_sect_title_3'],
                        'desc'=>$this->setting['gema_sect_content_3'],
                        ],
                    ];

                    $n_data = array_chunk($datas, 2);
            ?>
            <div class="inners">

                <div class="list-rown">
                    <?php foreach ($n_data as $key => $n_value): ?>
                        <?php foreach ($n_value as $ke => $val): ?>
                        <?php if ($ke == 0): ?>
                        <div class="items mb-5 py-4">
                            <div class="row">
                                <div class="col-md-30 my-auto">
                                    <div class="desc text-center px-3 py-5">
                                        <h4><?php echo $val['title'] ?></h4>
                                        <div class="py-2 my-1"></div>
                                        <p><?php echo $val['desc'] ?></p>
                                    </div>
                                </div>
                                <div class="col-md-30">
                                    <div class="pictures">
                                        <img src="<?php echo $val['picture'] ?>" alt="" class="img img-responsive">
                                    </div>
                                </div>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <?php else: ?>
                        <div class="items mb-5 py-4">
                            <div class="row">
                                <div class="col-md-30">
                                    <div class="pictures">
                                        <img src="<?php echo $val['picture'] ?>" alt="" class="img img-responsive">
                                    </div>
                                </div>
                                <div class="col-md-30 my-auto">
                                    <div class="desc text-center px-3 py-5">
                                        <h4><?php echo $val['title'] ?></h4>
                                        <div class="py-2 my-1"></div>
                                        <p><?php echo $val['desc'] ?></p>
                                    </div>
                                </div>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <?php endif ?>
                        <?php endforeach ?>
                    <?php endforeach ?>

                </div>

                <div class="clear"></div>
            </div>
            
            <div class="clear"></div>
        </div>
    </div>
    <?php 
    $url_string = parse_url($this->setting['gema_trip_video'], PHP_URL_QUERY);
    parse_str($url_string, $args);
    $idn_video = isset($args['v']) ? $args['v'] : false;
    ?>
    <div class="blocks_bottom_gema pt-4">
        <div class="prelatife container">
            <div class="inners text-center d-block mx-auto maw765 pt-5">
                <h3>GE-MA TRIP</h3>
                <div class="py-4 mb-2"></div>
                <style>.embed-container { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; } .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }</style>
                <div class='embed-container'><iframe src='https://www.youtube.com/embed/<?php echo $idn_video ?>' frameborder='0' allowfullscreen></iframe></div>
                <div class="clear"></div>
            </div>
        </div>
    </div>
</section>






<?php /*
<section class="career-sec-1" style="background-image: url(<?php echo Yii::app()->baseUrl; ?>/images/static/<?php echo $this->setting['career_2_image']; ?>)">
    <div class="prelative container">
        <div class="row">
            <div class="col-md-30"></div>
            <div class="col-md-30">
                <div class="box-content">
                    <div class="company">
                        <p><?php echo $this->setting['career_2_title'] ?></p>
                    </div>
                    <div class="title">
                        <p><?php echo $this->setting['career_2_subtitle'] ?></p>
                    </div>
                    <div class="content">
                        <?php echo $this->setting['career_2_content'] ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="career-sec-2">
    <div class="prelative container">
        <div class="row">
            <?php foreach ($categorys as $key_car => $value_car): ?>
                <div class="col-md-20">
                    <div class="box-career">
                        <img class="w-100 img img-fluid" src="<?php echo Yii::app()->baseUrl; ?>/images/career_category/<?php echo $value_car->image ?>" alt="">
                        <div class="box-content">
                            <a href="<?php echo CHtml::normalizeUrl(array('/home/career', 'category'=> $value_car->id, 'lang'=>Yii::app()->language)); ?>">
                                <div class="title">
                                    <p><?php echo $value_car->nama ?></p>
                                </div>
                                <div class="isinya">
                                    <p><?php echo $value_car->subtitle ?></p>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="py-3"></div>
                </div>
            <?php endforeach ?>
        </div>
    </div>
    <div class="garis-career d-block d-sm-none">
    </div>
    <div class="pb-5 d-none d-sm-block"></div>
    <!-- <div class="pb-5"></div> -->
    <div class="pb-5"></div>
</section>

<section class="career-sec-3">
    <div class="prelative container">
        <div class="row">
            <div class="col-md-60">
                <div class="heading">
                    <div class="title">
                        <p>LATEST POST</p>
                    </div>
                    <div class="subtitle">
                        <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  <br>tempor incididunt ut labore et dolore magna aliqua.</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <?php foreach ($data as $key => $value): ?>
            <div class="col-md-20">
                <div class="box-post">
                    <a href="<?php echo CHtml::normalizeUrl(array('/home/career_detail', 'id'=> $value->id, 'lang'=>Yii::app()->language)); ?>"><img class="w-100 img img-fluid" src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(357, 233, '/images/career/'.$value->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt=""></a>
                    <div class="rec py-2 pt-2">
                        <p>Recruitment</p>
                    </div>
                    <div class="by pb-2">
                        <p>By Admin <?php echo date('d F Y', strtotime($value->dates_input)); ?></p>
                    </div>
                    <div class="content mt-2 pb-3">
                        <?php echo substr($value->content, 0, 70); ?>
                    </div>
                    <a href="<?php echo CHtml::normalizeUrl(array('/home/career_detail', 'id'=> $value->id, 'lang'=>Yii::app()->language)); ?>">Read More</a>
                </div>
                <div class="py-3"></div>
            </div>
            <?php endforeach ?>

        </div>
    </div>
    <div class="pb-5 d-none d-sm-block"></div>
    <div class="pb-5"></div>
    <div class="pb-5"></div>
</section>
*/ ?>