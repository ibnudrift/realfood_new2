<!-- Start fcs -->
<?php
$slide = ViewSlide::model()->findAll('language_id = :language_id AND topik_id = 2 ORDER BY sort ASC', array(':language_id'=>$this->languageID));
?>
<div class="fcs-wrapper outers_fcs_wrapper prelatife wrapper-slide home">
    <div id="myCarousel_home" class="carousel carousel-fade" data-ride="carousel" data-interval="4500">
            <div class="carousel-inner">
                <?php foreach ($slide as $key => $value): ?>
                <div class="carousel-item <?php if ($key == 0): ?>active<?php endif ?> home-slider-new">
                <img class="w-100 d-none d-sm-block" src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(1920, 1078, '/images/slide/'.$value->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="First slide">
                <img class="w-100 d-block d-sm-none" src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(375, 667, '/images/slide/'.$value->image2 , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="First slide">
                    <div class="carousel-caption caption-slider-home mx-auto">
                        <div class="prelative container mx-auto">
                            <div class="bxsl_tx_fcs">
                                <div class="row no-gutters">
                                    <div class="col-md-60">
                                        <?php echo $value->subtitle ?>
                                        <div class="subtitle py-4">
                                            <?php echo $value->content ?>
                                            <div class="pt-5"></div>
                                            <a href="<?php echo $value->url ?>">LEARN MORE</a>
                                        </div>
                                    </div>
                                    <div class="col-md-27"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endforeach ?>
            </div>
            <?php /*<ol class="carousel-indicators">
                <li data-target="#myCarousel_home" data-slide-to="" class="active"></li>
            </ol>*/ ?>
    </div>
    <div class="clear-both"></div>
</div>
<!-- End fcs -->
<div class="pt-5"></div>
<div class="pt-5"></div>
<div class="pt-5"></div>
<section class="career-detail">
    <div class="row">
        <div class="col-md-30">
            <div class="picture">
                <img class="w-100 img img-fluid" src="<?php echo Yii::app()->baseUrl; ?>/images/career/<?php echo $data->image ?>" alt="">
            </div>
        </div>
        <div class="col-md-30">
            <div class="box-content">
                <div class="title">
                    <p><?php echo $categorys->nama; ?><br>
                    <span><?php echo $data->nama ?></span></p>
                </div>
                <div class="job-qua">
                    <div class="title">
                        <p>Jobdesc</p>
                    </div>
                    <div class="content">
                        <?php echo $data->content ?>
                    </div>
                </div>
                <div class="job-qua">
                    <div class="title">
                        <p>Qualiﬁcation</p>
                    </div>
                    <div class="content">
                        <?php echo $data->qualification ?>
                        <div class="clear"></div>
                    </div>
                </div>
                <div class="pt-5"></div>
                <div class="pt-3"></div>
                <div class="read">
                    <a href="#" data-toggle="modal" data-target="#myModal">APPLY NOW</a>
                </div>
            </div>
        </div>
    </div>
    <div class="pb-5"></div>
    <div class="pb-5"></div>
    <div class="pb-5"></div>
</section>

<section class="career-detail-sec-2">
    <div class="prelative container">
        <div class="title">
            <p>Check More Job Vacancies</p>
        </div>
        <div class="row pt-5">
            <?php 
            $criteria=new CDbCriteria;
            $criteria->addCondition('t.id != :ids');
            $criteria->params[':ids'] = $data->id;

            $criteria->addCondition('actives = 1');
            $criteria->limit = 3;
            $data_more = ListCareer::model()->findAll($criteria);
            ?>
            <?php foreach ($data_more as $key => $value): ?>
            <div class="col-md-20">
                <div class="box-post">
                    <a href="<?php echo CHtml::normalizeUrl(array('/home/career_detail', 'id'=> $value->id, 'lang'=>Yii::app()->language)); ?>"><img class="w-100 img img-fluid" src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(357, 233, '/images/career/'.$value->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt=""></a>
                    <div class="rec py-2 pt-2">
                        <p>Recruitment</p>
                    </div>
                    <div class="by pb-2">
                        <p>By Admin <?php echo date('d F Y', strtotime($value->dates_input)); ?></p>
                    </div>
                    <div class="content mt-2 pb-3">
                        <?php echo substr($value->content, 0, 70); ?>
                    </div>
                    <a href="<?php echo CHtml::normalizeUrl(array('/home/career_detail', 'id'=> $value->id, 'lang'=>Yii::app()->language)); ?>">Read More</a>
                </div>
                <div class="py-3"></div>
            </div>
            <?php endforeach ?>
        </div>
    </div>
    <div class="pb-5"></div>
    <div class="py-3"></div>
</section>

<div class="modal" id="myModal">
    <div class="modal-dialog modal-dialog-centered modal-lg">
      <div class="modal-content">
        
        <!-- Modal body -->
        <div class="modal-body">
            <div class="row no-gutters">
                <div class="col-md-35">
                    <div class="picture"><img class="w-100 img img-fluid" src="<?php echo Yii::app()->baseUrl; ?>/asset/images/new/CAREER-POP-UP-01_02.jpg" alt=""></div>
                </div>
                <div class="col-md-25">
                    <div class="box-kanan">
                        <div class="thank">
                            <p>Thank You.</p>
                        </div>
                        <div class="sub">
                            <p>Your application is <br>successfully delivered to <br>our ofﬁce.</p>
                        </div>
                        <div class="shop"><a href="#">ok</a></div>
                    </div>
                </div>
            </div>
        </div>
        
      </div>
    </div>
  </div>