<section class="full-banner-top">
    <div class="tops-cont-insidepage back-pgema" style="background: none; height: auto; position: relative;"> 
         <div class="pict-fulls hidden-xs">
            <?php
            $slide = ViewSlide::model()->findAll('language_id = :language_id AND topik_id = 7 ORDER BY sort ASC', array(':language_id'=>$this->languageID));
            ?>
            <div id="carousel-examp-topPage" class="carousel fade" data-ride="carousel">
              <div class="carousel-inner" role="listbox">
                <?php foreach ($slide as $ke => $value): ?>
                <div class="item <?php if ($ke == 0): ?>active<?php endif ?>">
                  <!-- <img src="<?php // echo Yii::app()->baseUrl.ImageHelper::thumb(1450,680, '/images/slide/'.$value->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="<?php echo $value->title; ?>"> -->
                  <div class="fill" style="background-image:url(<?php echo Yii::app()->baseUrl.ImageHelper::thumb(1450,680, '/images/slide/'.$value->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>);"></div>
                </div>
                <?php endforeach ?>
              </div>
            </div>
            <div class="clear"></div>
        </div>
        <div class="visible-xs">
          <div id="carousel_ex_phone" class="carousel fade" data-ride="carousel">
              <div class="carousel-inner" role="listbox">
                <?php foreach ($slide as $ke => $value): ?>
                <div class="item <?php if ($ke == 0): ?>active<?php endif ?>">
                  <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(1450,680, '/images/slide/'.$value->image2 , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="<?php echo $value->title; ?>">                  
                </div>
                <?php endforeach ?>
              </div>
            </div>
          <div class="clear"></div>
        </div>
    </div> 
</section>
<section class="outer-inside-middle-content back-white PageGema_section">
  <div class="prelatife container">
    <?php /*
    <div class="tops-cont-insidepage back-pgema" style="background: none; height: auto; position: relative;">

      <div class="pict-fulls">
            <?php
            // $slide = ViewSlide::model()->findAll('language_id = :language_id AND topik_id = 4 ORDER BY sort ASC', array(':language_id'=>$this->languageID));
            ?>
            <div id="carousel-examp-topPage" class="carousel slide" data-ride="carousel">
              <div class="carousel-inner" role="listbox">
                <?php // foreach ($slide as $ke => $value): ?>
                <div class="item <?php if ($ke == 0): ?>active<?php endif ?>">
                  <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(1450,680, '/asset/images/gema/hero-pict-improving.jpg' , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="<?php echo $value->title; ?>">
                </div>
                <?php // endforeach ?>
              </div>
            </div>

        </div>

      <div class="pabs-ins-middle-topim">
        <div class="tengah insd-container mw930 text-center content-up">
          <div class="hidden-xs">
            <?php // if ($this->setting['gema_hide'] == 0): ?>
            <h1 class="title-pages">GE-MA</h1> <div class="clear height-10"></div>
            <div class="lines-chld-bgrey tengah"></div>
            <div class="clear height-25"></div>
            <span class="bigs">IMPROVING LIVES</span> <div class="clear"></div>
          </div>
          <?php // endif ?>
          <div class="clear"></div>
        </div>
        <div class="clear"></div>
      </div>
    </div>
    */
     ?>

    <div class='pindahan_text-heroimage visible-xs'>
         <h1 class="title-pages">GE-MA</h1> <div class="clear height-10"></div>
          <div class="lines-chld-bgrey tengah"></div>
          <div class="clear height-25"></div>
          <span class="bigs">IMPROVING LIVES</span> <div class="clear"></div>
          <div class="insd-container tengah text-center">
          </div>
          <div class="clear"></div>
    </div>
        <div class="clear"></div>
      </div>
      <div class="prelatife">
        <div class="sub-headerData-menu">
            <div class="prelatife mw-995 tengah">
              <?php echo $this->renderPartial('//layouts/_menu_gema', array('active'=>$active)); ?>
              <div class="box-shadow"></div>
              <div class="clear"></div>
            </div>
        </div>
<?php
$live = Live::model()->findAll();
?>
        <?php foreach ($live as $key => $value): ?>
          
        <section class="section_1 default-section-gema prelatife">
            <div class="left_cont prelatife">
                <div class="pict-full"><img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(1300,750, '/images/live/'.$value->image , array('method' => 'resize', 'quality' => '90')) ?>" alt=""></div>

                <div class="pos-abs-full">
                  <div class="table-out prelatife">
                    <div class="table-in">
                        <div class="prelatife container gema_container">
                          <div class="row">
                            <?php if ($value->alignment == 1): ?>
                            <div class="col-md-6 col-sm-12">
                            <?php elseif ($value->alignment == 2): ?>
                            <div class="col-md-6 col-sm-12"></div>
                            <div class="col-md-6 col-sm-12">
                            <?php else: ?>
                            <div class="col-md-3 col-sm-1"></div>
                            <div class="col-md-6 col-sm-10">
                            <?php endif ?>
                              <div class="content-txt text-center <?php echo $value->color ?>">
                                <h2><?php echo $value->title ?></h2>
                                <div class="clear height-30"></div>
                                <p><?php echo $value->content ?></p>
                                <div class="clear"></div>
                                <a href="<?php echo $value->url ?>" target="_blank" class="default"><?php echo $value->text_url ?></a>
                                <div class="celar"></div>
                              </div>
                            <?php if ($value->alignment == 1): ?>
                            </div>
                            <div class="col-md-6 col-sm-12"></div>
                            <?php elseif ($value->alignment == 2): ?>
                            </div>
                            <?php else: ?>
                            </div>
                            <div class="col-md-3 col-sm-1"></div>
                            <?php endif ?>
                          </div>
                          <div class="clear"></div>
                        </div>
                      <div class="clear"></div>
                    </div>
                  </div>

                  <div class="clear"></div>
                </div>
                <!-- ends pos abs -->
            </div>
        </section>
        <div class="clear"></div>
        <?php endforeach ?>
      </div>
  <!-- </div> -->
</section>

<script type="text/javascript">
    $(document).ready(function(){
        var wbody = $(window).width();

        var heightHero = $('#carousel-examp-topPage img').height();
        if (wbody >= 768){
            $('.PageGema_section .pabs-ins-middle-topim').css("height", heightHero+"px");
        };

        // set w
        if (wbody > 768){
          var hsec1 = $('.section_1 img').height();
          $('.section_1 .table-out').css("height", hsec1+"px");
        }
    });

    $(window).load(function(){
        var wbody = $(window).width();

        var heightHero = $('#carousel-examp-topPage img').height();
        if (wbody >= 768){
            $('.PageGema_section .pabs-ins-middle-topim').css("height", heightHero+"px");
        };

        // set w
        if (wbody > 768){
          var hsec1 = $('.section_1 img').height();
          $('.section_1 .table-out').css("height", hsec1+"px");
        }
    });
</script>