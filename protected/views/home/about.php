<?php 
if ($this->setting['seo_story_titles']) {
  $this->pageTitle = $this->setting['seo_story_titles'];
}
if ($this->setting['seo_story_keyword']) {
  $this->metaKey = $this->setting['seo_story_keyword'];
}
if ($this->setting['seo_story_description']) {
  $this->metaDesc = $this->setting['seo_story_description'];
}

?>
<!-- Start fcs -->
<?php
$slide = ViewSlide::model()->findAll('language_id = :language_id AND topik_id = 1 ORDER BY sort ASC', array(':language_id' => $this->languageID));
?>
<div class="fcs-wrapper outers_fcs_wrapper prelatife wrapper-slide home">
    <div id="myCarousel_home" class="carousel carousel-fade" data-ride="carousel" data-interval="4500">
            <div class="carousel-inner">
                <?php foreach ($slide as $key => $value): ?>
                <div class="carousel-item <?php if ($key == 0): ?>active<?php endif ?> home-slider-new">
                <img class="w-100 d-none d-sm-block" src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(1920, 1078, '/images/slide/'.$value->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="First slide">
                <img class="w-100 d-block d-sm-none" src="<?php echo Yii::app()->baseUrl; ?>/images/slide/<?php echo $value->image2 ?>" alt="First slide">
                    <div class="carousel-caption caption-slider-home mx-auto">
                        <div class="prelative container mx-auto">
                            <div class="bxsl_tx_fcs">
                                <div class="row no-gutters">
                                    <div class="col-md-60">
                                        <?php echo $value->subtitle ?>
                                        <div class="subtitle py-4">
                                            <?php echo $value->content ?>
                                            <div class="pt-5"></div>
                                            <a href="<?php echo $value->url ?>">LEARN MORE</a>
                                        </div>
                                    </div>
                                    <div class="col-md-27"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endforeach ?>
            </div>
            <a class="carousel-control-prev" href="#myCarousel_home" role="button" data-slide="prev">
              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#myCarousel_home" role="button" data-slide="next">
              <span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
            </a>
            <?php /*<ol class="carousel-indicators">
                <li data-target="#myCarousel_home" data-slide-to="" class="active"></li>
            </ol>*/ ?>
    </div>
    <div class="clear-both"></div>
</div>
<!-- End fcs -->

<section class="outers-block-gemaa static_page">

    <div class="blocks_inners back-grey-def py-5">
        <div class="prelative container py-5 my-2">
            <?php 
            $datas = [
                    1 => [
                        'picture'=> $this->setting['about_sect_pict_1'],
                        'title'=> $this->setting['about_sect_title_1'],
                        'desc'=> $this->setting['about_sect_content_1'],
                        ],
                        [
                        'picture'=> $this->setting['about_sect_pict_2'],
                        'title'=> $this->setting['about_sect_title_2'],
                        'desc'=> $this->setting['about_sect_content_2'],
                        ],
                        [
                        'picture'=> $this->setting['about_sect_pict_3'],
                        'title'=> $this->setting['about_sect_title_3'],
                        'desc'=> $this->setting['about_sect_content_3'],
                        ],
                    ];
            ?>
            <div class="inners">

                <div class="list-rown">
                    <?php foreach ($datas as $key => $val): ?>
                      <?php if ($key == 2): ?>
                        <div class="items mb-5 py-4">
                            <div class="row">
                                <div class="col-md-30 my-auto order-2 order-sm-1">
                                    <div class="desc text-left px-3 py-5">
                                        <h4><?php echo $val['title'] ?></h4>
                                        <div class="py-2 my-1"></div>
                                        <p><?php echo $val['desc'] ?></p>
                                    </div>
                                </div>
                                <div class="col-md-30 order-1 order-sm-2">
                                    <div class="pictures">
                                        <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(561,399, '/images/static/'. $val['picture'] , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img img-fluid w-100">
                                    </div>
                                </div>
                            </div>
                            <div class="clear"></div>
                        </div>
                      <?php else: ?>
                        <div class="items mb-5 py-4">
                            <div class="row">
                                <div class="col-md-30">
                                    <div class="pictures">
                                        <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(561,399, '/images/static/'. $val['picture'] , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img img-fluid w-100">
                                    </div>
                                </div>
                                <div class="col-md-30 my-auto">
                                    <div class="desc text-left px-3 py-5">
                                        <h4><?php echo $val['title'] ?></h4>
                                        <div class="py-2 my-1"></div>
                                        <p><?php echo $val['desc'] ?></p>
                                    </div>
                                </div>
                            </div>
                            <div class="clear"></div>
                        </div>
                      <?php endif ?>
                    <?php endforeach ?>

                    <div class="items mb-5 py-4">
                        <div class="row no-gutters back-white">
                            <div class="col-md-30 my-auto back-white order-2 order-sm-1">
                                <div class="desc text-left px-5 py-5">
                                    <h4><?php echo $this->setting['about_sect_title_4']  ?></h4>
                                    <div class="py-2 my-1"></div>
                                    <p><?php echo $this->setting['about_sect_content_4']  ?></p>
                                </div>
                            </div>
                            <div class="col-md-30 order-1 order-sm-2">
                                <div class="pictures">
                                    <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(561,399, '/images/static/'. $this->setting['about_sect_pict_4'] , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img img-fluid w-100">
                                </div>
                            </div>
                        </div>
                        <div class="clear"></div>
                    </div>

                </div>

                <div class="clear"></div>
            </div>
            
            <div class="clear"></div>
        </div>
    </div>

</section>

<section class="bottom-abouts-values py-5">
  <div class="prelative container my-5">
    <div class="inners text-center">
      <h3 class="text-center"><?php echo $this->setting['about_sect2_title'] ?></h3>
      <div class="py-4"></div>
  
      <div class="list-bullets_icon">
        <div class="row">
          <?php for ($i=1; $i < 4; $i++) { ?>
          <div class="col-md-20">
            <div class="items">
              <div class="pict"><img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(81,81, '/images/static/'. $this->setting['about_sect3_pict_'. $i] , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="d-block mx-auto img img-fluid"></div>
              <div class="info py-5">
                <h5><?php echo $this->setting['about_sect3_title_'. $i] ?></h5>
                <div class="py-3"></div>
                <?php echo $this->setting['about_sect3_content_'. $i] ?>
              </div>
            </div>
          </div>
          <?php } ?>
          
        </div>
      </div>

      <div class="clear"></div>
    </div>
  </div>
</section>

<section class="bottom-abouts-backsnwhite py-5">
  <div class="prelative container my-5">
    <div class="inners text-center py-4">

        <div class="row">
          <div class="col-md-30 borders-right py-5">
            <div class="boxs-item px-3">
              <span><?php echo $this->setting['about_sect4_smalltitle_1'] ?></span>
              <div class="py-2 pb-3"></div>
              <h4><?php echo $this->setting['about_sect4_title_1'] ?></h4>
              <div class="py-2 pb-3"></div>
              <?php echo $this->setting['about_sect4_desc_1'] ?>
            </div>
            <div class="py-3"></div>
            <a href="<?php echo CHtml::normalizeUrl(array('/home/gema', 'lang'=>Yii::app()->language)); ?>" class="btn-defaults_set">LEARN MORE</a>
          </div>
          <div class="col-md-30 py-5">
            <div class="boxs-item px-3">
              <span><?php echo $this->setting['about_sect4_smalltitle_2'] ?></span>
              <div class="py-2 pb-3"></div>
              <h4><?php echo $this->setting['about_sect4_title_2'] ?></h4>
              <div class="py-2 pb-3"></div>
              <?php echo $this->setting['about_sect4_desc_2'] ?>
            </div>
            <div class="py-3"></div>
            <a href="<?php echo CHtml::normalizeUrl(array('/home/career', 'lang'=>Yii::app()->language)); ?>" class="btn-defaults_set">LEARN MORE</a>
          </div>
        </div>

      <div class="clear"></div>
    </div>
  </div>
</section>



<?php /*
<section class="outer-inside-middle-content back-white">
  <div class="prelatife container">
    <div class="tops-cont-insidepage"> <div class="clear height-50"></div>
        <div class="height-50"></div>
        <div class="height-50"></div>
        <div class="height-25"></div>
        <div class="tengah insd-container text-center content-up">
          <?php if ($this->setting['about_header_hide'] == 0): ?>
          <h1 class="title-pages">ABOUT</h1> <div class="clear height-10"></div>
          <div class="lines-chld-bgrey tengah"></div> <div class="clear height-25"></div>
          <?php endif ?>
          <span class="bigs"><?php echo $this->setting['about_header_title'] ?></span> <div class="clear"></div>
          <p><?php echo nl2br($this->setting['about_header_subtitle']) ?></p>
          <div class="clear"></div>
        </div>
    </div>

    <div class="clear"></div>
  </div>

  <div class="back-grey mh500">
    <div class="prelatife container">
        <div class="clear height-50"></div>
        <div class="outers-cont-bottom-abouts">
          <div class="topscont text-center">
            <ul class="list-inline">
              <li><a class="ajaxCall" href="<?php echo CHtml::normalizeUrl(array('/about/who_we_are')); ?>">who we are</a></li>
              <li><a class="ajaxCall" href="<?php echo CHtml::normalizeUrl(array('/about/ourteam')); ?>">our team</a></li>
              <li><a class="ajaxCall" href="<?php echo CHtml::normalizeUrl(array('/about/visimisi')); ?>">vision & mission</a></li>
              <li><a class="ajaxCall" href="<?php echo CHtml::normalizeUrl(array('/about/workwithus')); ?>">work with us</a></li>
            </ul>
          </div>
          <div class="clear height-50"></div><div class="height-35"></div>
          <div class="middles">
            
            <div class="mw920 tengah text-center">
                <h2><?php echo $data['title'] ?></h2> <div class="clear height-30"></div>
                <?php echo $data['content'] ?>
                <div class="clear height-50"></div>

                <?php
                $image = AboutImage::model()->findAll();
                ?>
            <?php foreach ($image as $key => $value): ?>
                <div class="ill-abouts"><img src="<?php echo Yii::app()->baseUrl; ?>/images/about/<?php echo $value->image ?>" alt="" class="img-responsive"></div>
                <div class="clear height-50"></div><div class="height-5"></div>
            <?php endforeach ?>             
                <div class="shares-text">
                  <span class="inline-t">SHARE</span>&nbsp;&nbsp; / &nbsp;&nbsp;<a href="#">FACEBOOK</a>&nbsp;&nbsp; / &nbsp;&nbsp;<a href="#">GOOGLE PLUS</a>&nbsp;&nbsp; / &nbsp;&nbsp;<a href="#">TWITTER</a>
                </div>

                <div class="clear"></div>
            </div>

            <div class="clear"></div>
          </div>

          <div class="clear"></div>
        </div>
      
      <div class="clear"></div> <div class="height-50"></div><div class="height-20"></div>
    </div>
  </div>
</section>
<script type="text/javascript">
  $(document).ready(function() {
    $('.ajaxCall').live('click', function(){
      alert($(this).attr('href')); return false;

       $.ajax({
            type: "get",
            url: $(this).attr('href'),
            data: data,
            dataType: "json",
            success: function(msg) {
                    alert(msg); return false;
            },
            error: function (msg) {
                    alert("Fucking Bull Shit!");
            },
    });

    });
});
</script>
*/ ?>