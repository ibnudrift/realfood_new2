<!-- Start fcs -->
<?php
$slide = ViewSlide::model()->findAll('language_id = :language_id AND topik_id = 2 ORDER BY sort ASC', array(':language_id'=>$this->languageID));
?>
<div class="fcs-wrapper outers_fcs_wrapper prelatife wrapper-slide home">
    <div id="myCarousel_home" class="carousel carousel-fade" data-ride="carousel" data-interval="4500">
            <div class="carousel-inner">
                <?php foreach ($slide as $key => $value): ?>
                <div class="carousel-item <?php if ($key == 0): ?>active<?php endif ?> home-slider-new">
                <img class="w-100 d-none d-sm-block" src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(1920, 1078, '/images/slide/'.$value->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="First slide">
                <img class="w-100 d-block d-sm-none" src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(375, 667, '/images/slide/'.$value->image2 , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="First slide">
                    <div class="carousel-caption caption-slider-home mx-auto">
                        <div class="prelative container mx-auto">
                            <div class="bxsl_tx_fcs">
                                <div class="row no-gutters">
                                    <div class="col-md-60">
                                        <?php echo $value->subtitle ?>
                                        <div class="subtitle py-4">
                                            <?php echo $value->content ?>
                                            <div class="pt-5"></div>
                                            <a href="<?php echo $value->url ?>">LEARN MORE</a>
                                        </div>
                                    </div>
                                    <div class="col-md-27"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endforeach ?>
            </div>
            <a class="carousel-control-prev" href="#myCarousel_home" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
              </a>
              <a class="carousel-control-next" href="#myCarousel_home" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
              </a>
            <?php /*<ol class="carousel-indicators">
                <li data-target="#myCarousel_home" data-slide-to="" class="active"></li>
            </ol>*/ ?>
    </div>
    <div class="clear-both"></div>
</div>
<!-- End fcs -->

<section class="outers-block-careers static_page">
    <div class="block-career-grey">
        <div class="prelative container containers_career py-5">
            <div class="inners-content py-5">
                
                <?php 
                $n_career = ListCareer::model()->findAll();
                ?>
                <?php foreach ($n_career as $key => $value): ?>
                <div class="list-full-banner mb-5">
                    <a href="#">
                    <img src="<?php echo Yii::app()->baseUrl.'/images/career/'. $value->image; ?>" alt="" class="img img-fluid">
                    </a>
                </div>
                <?php endforeach ?>

                <div class="clear"></div>
            </div>
        </div>
    </div>
    <div class="block back-white">
        <div class="inners-content py-5">

            <div class="in_blockform form-careers d-block mx-auto py-5">
                <form action="#" method="post" onsubmit="javascript: alert('underconstruction');">
                    <div class="row no-gutters">
                        <div class="col-md-30">
                            <div class="form-group row">
                                <label for="Inptx" class="col-sm-15 col-form-label">Position</label>
                                <div class="col-sm-40">
                                  <input type="text" class="form-control" id="Inptx" value="">
                                </div>
                              </div>
                        </div>
                        <div class="col-md-30">
                            <div class="form-group row">
                                <label for="Inptx" class="col-sm-15 col-form-label">Name</label>
                                <div class="col-sm-40">
                                  <input type="text" class="form-control" id="Inptx" value="">
                                </div>
                              </div>
                        </div>
                    </div>
                    <div class="row no-gutters">
                        <div class="col-md-30">
                            <div class="form-group row">
                                <label for="Inptx" class="col-sm-15 col-form-label">Phone</label>
                                <div class="col-sm-40">
                                  <input type="text" class="form-control" id="Inptx" >
                                </div>
                              </div>
                        </div>
                        <div class="col-md-30">
                            <div class="form-group row">
                                <label for="Inptx" class="col-sm-15 col-form-label">Email</label>
                                <div class="col-sm-40">
                                  <input type="text" class="form-control" id="Inptx" value="">
                                </div>
                              </div>
                        </div>
                    </div>
                    <div class="row no-gutters">
                        <div class="col-md-30">
                            <div class="form-group row">
                                <label for="Inptx" class="col-sm-15 col-form-label">Address</label>
                                <div class="col-sm-40">
                                  <input type="text" class="form-control" id="Inptx" >
                                </div>
                              </div>
                        </div>
                        <div class="col-md-30">
                            <div class="form-group row">
                                <label for="Inptx" class="col-sm-15 col-form-label">City</label>
                                <div class="col-sm-40">
                                  <input type="text" class="form-control" id="Inptx" value="">
                                </div>
                              </div>
                        </div>
                    </div>
                    <div class="row no-gutters">
                        <div class="col-md-30">
                            <div class="form-group row">
                                <label for="Inptx" class="col-sm-15 col-form-label">Province</label>
                                <div class="col-sm-40">
                                  <input type="text" class="form-control" id="Inptx" value="">
                                </div>
                              </div>
                        </div>
                        <div class="col-md-30">
                            <div class="form-group row">
                                <label for="Inptx" class="col-sm-15 col-form-label">CV.</label>
                                <div class="col-sm-40">
                                  <input type="file" class="form-control">
                                </div>
                              </div>
                        </div>
                    </div>
                    <div class="row no-gutters">
                        <div class="col-md-60">
                            <div class="form-group row">
                                <label for="Inptx" class="col-sm-7 col-form-label">Message</label>
                                <div class="col-sm-50">
                                  <textarea name="" id="" rows="3" class="form-control"></textarea>
                                </div>
                              </div>
                        </div>
                    </div>
                    <div class="row no-gutters">
                        <div class="col-md-57">
                            <div class="text-right float-right mt-1">
                            <button class="btn btn-primary btns_submitn_frm"></button>
                            </div>
                        </div>
                    </div>                    
                    
                </form>
                <div class="clear"></div>
            </div>

            <div class="clear"></div>
        </div>
    </div>
</section>


<?php /*
<section class="career-sec-1" style="background-image: url(<?php echo Yii::app()->baseUrl; ?>/images/static/<?php echo $this->setting['career_2_image']; ?>)">
    <div class="prelative container">
        <div class="row">
            <div class="col-md-30"></div>
            <div class="col-md-30">
                <div class="box-content">
                    <div class="company">
                        <p><?php echo $this->setting['career_2_title'] ?></p>
                    </div>
                    <div class="title">
                        <p><?php echo $this->setting['career_2_subtitle'] ?></p>
                    </div>
                    <div class="content">
                        <?php echo $this->setting['career_2_content'] ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="career-sec-2">
    <div class="prelative container">
        <div class="row">
            <?php foreach ($categorys as $key_car => $value_car): ?>
                <div class="col-md-20">
                    <div class="box-career">
                        <img class="w-100 img img-fluid" src="<?php echo Yii::app()->baseUrl; ?>/images/career_category/<?php echo $value_car->image ?>" alt="">
                        <div class="box-content">
                            <a href="<?php echo CHtml::normalizeUrl(array('/home/career', 'category'=> $value_car->id, 'lang'=>Yii::app()->language)); ?>">
                                <div class="title">
                                    <p><?php echo $value_car->nama ?></p>
                                </div>
                                <div class="isinya">
                                    <p><?php echo $value_car->subtitle ?></p>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="py-3"></div>
                </div>
            <?php endforeach ?>
        </div>
    </div>
    <div class="garis-career d-block d-sm-none">
    </div>
    <div class="pb-5 d-none d-sm-block"></div>
    <!-- <div class="pb-5"></div> -->
    <div class="pb-5"></div>
</section>

<section class="career-sec-3">
    <div class="prelative container">
        <div class="row">
            <div class="col-md-60">
                <div class="heading">
                    <div class="title">
                        <p>LATEST POST</p>
                    </div>
                    <div class="subtitle">
                        <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  <br>tempor incididunt ut labore et dolore magna aliqua.</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <?php foreach ($data as $key => $value): ?>
            <div class="col-md-20">
                <div class="box-post">
                    <a href="<?php echo CHtml::normalizeUrl(array('/home/career_detail', 'id'=> $value->id, 'lang'=>Yii::app()->language)); ?>"><img class="w-100 img img-fluid" src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(357, 233, '/images/career/'.$value->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt=""></a>
                    <div class="rec py-2 pt-2">
                        <p>Recruitment</p>
                    </div>
                    <div class="by pb-2">
                        <p>By Admin <?php echo date('d F Y', strtotime($value->dates_input)); ?></p>
                    </div>
                    <div class="content mt-2 pb-3">
                        <?php echo substr($value->content, 0, 70); ?>
                    </div>
                    <a href="<?php echo CHtml::normalizeUrl(array('/home/career_detail', 'id'=> $value->id, 'lang'=>Yii::app()->language)); ?>">Read More</a>
                </div>
                <div class="py-3"></div>
            </div>
            <?php endforeach ?>

        </div>
    </div>
    <div class="pb-5 d-none d-sm-block"></div>
    <div class="pb-5"></div>
    <div class="pb-5"></div>
</section>
*/ ?>