<section class="full-banner-top">
    <div class="tops-cont-insidepage back-pgema" style="background: none; height: auto; position: relative;"> 
         <div class="pict-fulls hidden-xs">
            <?php
            $slide = ViewSlide::model()->findAll('language_id = :language_id AND topik_id = 2 ORDER BY sort ASC', array(':language_id'=>$this->languageID));
            ?>
            <div id="carousel-examp-topPage" class="carousel fade" data-ride="carousel">
              <div class="carousel-inner" role="listbox">
                <?php foreach ($slide as $ke => $value): ?>
                <div class="item <?php if ($ke == 0): ?>active<?php endif ?>">
                  <!-- <img src="<?php // echo Yii::app()->baseUrl.ImageHelper::thumb(1450,680, '/images/slide/'.$value->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="<?php echo $value->title; ?>"> -->
                  <div class="fill" style="background-image:url(<?php echo Yii::app()->baseUrl.ImageHelper::thumb(1450,680, '/images/slide/'.$value->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>);"></div>
                </div>
                <?php endforeach ?>
              </div>
            </div>
            <div class="clear"></div>
        </div>
        <div class="visible-xs">
          <div id="carousel_ex_phone" class="carousel fade" data-ride="carousel">
              <div class="carousel-inner" role="listbox">
                <?php foreach ($slide as $ke => $value): ?>
                <div class="item <?php if ($ke == 0): ?>active<?php endif ?>">
                  <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(1450,680, '/images/slide/'.$value->image2 , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="<?php echo $value->title; ?>">                  
                </div>
                <?php endforeach ?>
              </div>
            </div>
          <div class="clear"></div>
        </div>
        <?php
        /*
        <div class="pabs-ins-middle-topim">
            <div class="tengah insd-container mw930 text-center content-up visible-lg">
                <div class="visible-lg">
                    <div class="clear height-50"></div>
                    <div class="height-50"></div>
                    <div class="height-50"></div>
                    <div class="height-25"></div>
                </div>
                  <?php if ($this->setting['career_hide'] == 0): ?>
                  <h1 class="title-pages"><?php echo $this->setting['career_title'] ?></h1> <div class="clear height-10"></div>
                  <div class="lines-chld-bgrey tengah"></div> <div class="clear height-25"></div>
                  <?php endif ?>
                  <!-- <span class="bigs">EMPOWER THE GENERATION</span> <div class="clear"></div> -->
                  <div class="insd-container tengah text-center">
                    <p><?php echo nl2br($this->setting['career_content']) ?></p>
                  </div>
                  <div class="clear"></div>
            </div>
        </div>*/ ?>
    </div> 
</section>
<section class="outer-inside-middle-content back-white">
  <div class="prelatife container">
    <?php
        /*
    <div class='pindahan_text-heroimage visible-xs'>
      <?php if ($this->setting['product_hero_hide'] == ''): ?>
          <h1 class="title-pages"><?php echo $this->setting['product_hero_title'] ?></h1> <div class="clear height-10"></div>
          <div class="lines-chld-bgrey tengah"></div> <div class="clear height-25"></div>
          <?php endif ?>
          <?php if ($this->setting['product_big_title'] != ''): ?>
          <span class="bigs"><?php echo $this->setting['product_big_title'] ?></span> <div class="clear"></div>
          <?php endif ?>
        <div class="clear"></div>
    </div>*/ ?>
    
    <div class="clear"></div>
  </div>

  <div class="back-grey mh500">
    <div class="prelatife container">
      <div class="hidden-xs">
        <div class="clear height-50"></div>
        <div class="height-40"></div>
        </div>
        <div class="visible-xs">
          <div class="clear height-35"></div>
        </div>
<?php
$gallery = ViewGallery::model()->findAll('language_id = :language_id ORDER BY date_input DESC', array(':language_id'=>$this->languageID));
?>
        <div class="outers-listing-dataproducts">
          <div class="row">
            <?php foreach ($gallery as $key => $value): ?>
            <div class="col-md-4">
              <div class="itemss">
                <?php if ($value->title != ''): ?>
                <div class="picts hidden-xs"><img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(245,282, '/images/gallery/'.$value->image , array('method' => 'resize', 'quality' => '90')) ?>" alt=""></div>
                <div class="picts visible-xs" style="max-width:125px; margin:0 auto;">
                  <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(220,600, '/images/gallery/'.$value->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" style="max-width:100%;"></div>
                <div class="hidden-xs">
                  <div class="clear height-30"></div>
                </div>
                <?php else: ?>
                  <div class="hidden-xs">
                    <div class="clear height-50"></div> 
                    <div class="clear height-50"></div> 
                  </div>
                  <div class="picts"><img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(245,282, '/images/gallery/'.$value->image , array('method' => 'resize', 'quality' => '90')) ?>" alt=""></div>
                  <div class="clear height-0"></div>
                <?php endif ?>
                <div class="clear"></div>
                <?php if ($value->title != '' AND $value->sub_title != '' AND  $value->content != ''): ?>
                <div class="descs">
                      <div class="h_indesc_top">
                          <div class="title">
                            <a href="<?php echo CHtml::normalizeUrl(array('/product/detail', 'id'=>$value->id, 'lang'=>Yii::app()->language)); ?>">
                               <?php echo $value->title_bold ?> <?php echo $value->title ?>
                            </a>
                          </div> 
                          <div class="clear height-15"></div><div class="height-2"></div>
                          <p><?php echo $value->sub_title ?> <br> <b><?php echo $value->sub_title_2 ?></b></p>
                          <div class="clear height-15"></div><div class="height-2"></div>
                          <?php 
                          $output = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $value->features);
                          ?>
                          <?php echo $output ?>
                          <div class="clear height-50"></div>
                      </div>
                      <div class="buttons_b_product">
                        <?php if ($value->title != ''): ?>
                        <a href="<?php echo CHtml::normalizeUrl(array('/product/detail', 'id'=>$value->id, 'lang'=>Yii::app()->language)); ?>">
                          <img class="hidden-xs pc-center" src="<?php echo $this->assetBaseurl; ?>back-bt-view-products.png" alt="">
                          <img class="visible-xs pc-center" src="<?php echo $this->assetBaseurl; ?>back-bt-view-products-responsl.png" style="width: 122px; height: 22px;" alt="">
                        </a>
                        <?php endif ?>
                      </div>
                  </div>
                <?php endif ?>
              </div> <!-- end items -->
              
            </div>
            <?php endforeach ?>
          </div>

        </div>
        <!-- end outer products --> 
        <div class="clear height-30"></div>
        <div class="clear"></div>
        <div class="hidden">
          <?php if ($this->setting['product_catalog']): ?>
          <div class="text-center text-download-catalog">
              <a href="<?php echo Yii::app()->baseUrl.'/images/static/'.$this->setting['product_catalog'] ?>" target="_blank"><img src="<?php echo Yii::app()->baseUrl; ?>/asset/images/txt-icon-download-catalog.png" alt="" class="img-responsive"></a>
          </div>
          <?php endif ?>
          <div class="clear"></div>
        </div>
        <div class="view-logo-merchant">
          <?php
            $logo_m = ViewMerchant::model()->findAll('language_id = :language_id AND topik_id = 0 ORDER BY sort ASC', array(':language_id'=>$this->languageID));
            ?>
            <?php if (count($logo_m) > 0): ?>
            <div class="listing-logo_merchant">
              <p class="text-center"><b>Available at</b></p>
              <div class="clear height-15"></div>
              <div class="row">
                <?php foreach ($logo_m as $key => $value): ?>
                <div class="col-md-3 col-sm-4">
                  <div class="items">
                    <img class="img-responsive" src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(160, 120, '/images/merchant/'.$value->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="<?php echo $value->title; ?>">
                  </div>
                </div>
                <?php endforeach ?>
              </div>
              <div class="clear"></div>
            </div>
            <?php endif ?>

          <div class="clear"></div>
        </div>

        <div class="clear height-10"></div>
        <!-- <div class="height-20"></div> -->
        <div class="mw-783 tengah text-center content-text">
          <!-- <div class="lines-grey h3"></div> -->
            <!-- <div class="clear height-35"></div> -->
            <div class="height-5"></div>
            <h3><?php echo $this->setting['product_title'] ?></h3>
            <?php echo $this->setting['product_content'] ?>
            <div class="clear"></div>
            <a href="<?php echo CHtml::normalizeUrl(array('/home/gema', 'lang'=>Yii::app()->language)); ?>" class="custom-gema btn btn-link"><i class="fa fa-chevron-right"></i> &nbsp;GE-MA</a>
            <div class="clear height-15"></div>
            <!-- <div class="clear height-25"></div> -->
            <div class="clear"></div>
            <div class="shares-text">
                  <span class="inline-t">SHARE</span>&nbsp;&nbsp; / &nbsp;&nbsp;<a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode(Yii::app()->request->hostInfo.Yii::app()->request->url) ?>">FACEBOOK</a>&nbsp;&nbsp; /
                  &nbsp;&nbsp;<a target="_blank" href="https://plus.google.com/share?url=<?php echo urlencode(Yii::app()->request->hostInfo.Yii::app()->request->url) ?>">GOOGLE PLUS</a>&nbsp;&nbsp; /
                  &nbsp;&nbsp;<a target="_blank" href="https://twitter.com/home?status=<?php echo urlencode(Yii::app()->request->hostInfo.Yii::app()->request->url) ?>">TWITTER</a>
                </div>
          
            <div class="clear"></div>
        </div>
      
      <div class="clear"></div> 
      <!-- <div class="height-35"></div> -->
      <div class="height-5"></div>
    </div>
  </div>
</section>
<script type="text/javascript">
$('.fancy').fancybox({
        // type: 'image',
});
</script>
<style type="text/css">
  @media (max-width: 767px) {
    .tops-cont-insidepage.back-products{ background: none !important; height: auto !important; }
    .content-text h3{ padding: 0px; }
  }
  .content-text h3{
    font-size: 17px;
    padding: 0 11em;
  }
  a.custom-gema.btn.btn-link {
      font-size: 16px;
      color: #656565;
      font-weight: 700;
  }
</style>