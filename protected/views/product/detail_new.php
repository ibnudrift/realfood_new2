<div class="outers_cont_inside_page back-white about_page" id="inside_page">

  <section class="blocks_illustration_dln pg_product">    
    <div class="prelatife blocksn_in_illustration_top_pg">

      <div class="insln_lwn text-center">
        <div class="sn_block_title">
          <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(240,150, '/images/category/'.$data->image2 , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img-responsive center-block">
        </div>
      </div>
      <div class="clear height-50"></div><div class="height-20"></div>
      <div class="picturn_slogan_products">
        <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(455,455, '/images/category/'.$data->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img-responsive center-block">
      </div>

      <div class="clear"></div>
    </div>
  </section>
  <div class="clear"></div>

  <section class="default_sci back-white blocks_middle_inside_outern text-center pg_product">
    <div class="prelatife container pb-0 pt-0">
      <div class="insides content-text text-center">
        <div class="clear height-25"></div>
        <div class="maw990 tengah">
        <h3><?php echo $data->description->name ?></h3>
        <?php echo $data->description->content ?>
        </div>

        <div class="clear height-50"></div>
        <div class="clear height-30"></div>

        <div class="panel-def">
          <div class="list-panel item1">
            <div class="head_panel">
              <h4>PRODUCT VARIANTS</h4>
              <a href="#"><i class="fa fa-minus"></i></a>
            </div>
            <div class="body_panel">
<?php
$criteria2=new CDbCriteria;
$criteria2->with = array('description');
$criteria2->order = 'date DESC';
$criteria2->addCondition('status = "1"');
$criteria2->addCondition('description.language_id = :language_id');
$criteria2->params[':language_id'] = $this->languageID;
$criteria2->addCondition('t.category_id = :category_id');
$criteria2->params[':category_id'] = $data->id;
$products = PrdProduct::model()->findAll($criteria2);
?>
              <div class="lists_def_nutriciton">
                <div class="row">
                  <?php foreach ($products as $key => $value): ?>
                    <?php $dataSerialize = unserialize($value->data) ?>
                  <div class="col-md-4">
                    <div class="items">
                      <div class="pict"><img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(478,433, '/images/product/'.$value->image , array('method' => 'resize', 'quality' => '90')) ?>" alt="" class="img-responsive center-block"></div>
                      <div class="buysn_btn">
                        <a href="<?php echo $dataSerialize['url'] ?>" class="btn btn-default buttons_custom_def_black">BUY NOW</a>
                      </div>
                    </div>
                  </div>
                  <?php endforeach ?>

                </div>
              </div>

              <div class="clear"></div>
            </div>
            <div class="clear"></div>
          </div>
          <div class="clear"></div>

          <div class="list-panel item2">
            <div class="head_panel">
              <h4>PRODUCT BENEFITS</h4>
              <a href="#"><i class="fa fa-minus"></i></a>
            </div>
            <div class="body_panel">

              <div class="block_brownsn_pan_toplln1 mb-18" style="background-image: url('<?php echo Yii::app()->baseUrl.ImageHelper::thumb(1475,540, '/images/category/'.$data->benefit_image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>');">
                <div class="inset tengah">
                  <div class="maw805 tengah">
                    <h4><?php echo $data->description->benefit_title ?></h4>
                    <?php echo $data->description->benefit_content ?>
                  </div>
                </div>
                <div class="clear"></div>
              </div>

              <div class="boxs-grey_qualtyPbtm">
                <div class="inner">
                  <div class="lists_qlytnPbtm">
                    <div class="row">
                      <?php for ($i=1; $i < 4; $i++) { ?>
                      <div class="col-md-4">
                        <div class="items">
                          <div class="pict"><img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(228,246, '/images/category/'.$data->{'benefit_image_'.$i} , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img-responsive center-block"></div>
                          <div class="info">
                            <h5><?php echo $data->description->{'benefit_title_'.$i} ?></h5>
                            <p><?php echo nl2br($data->description->{'benefit_content_'.$i}) ?></p>
                            <div class="clear"></div>
                          </div>
                        </div>
                      </div>
                      <?php } ?>

                    </div>
                  </div>
                  <div class="clear"></div>
                </div>
              </div>

              <div class="clear"></div>
            </div>
            <div class="clear"></div>
          </div>

        </div>

        <div class="clear height-50"></div>

        <div class="view-logo-merchant">
          <?php
            $logo_m = ViewMerchant::model()->findAll('language_id = :language_id AND topik_id = 0 ORDER BY sort ASC', array(':language_id'=>$this->languageID));
            ?>
            <?php if (count($logo_m) > 0): ?>
            <div class="listing-logo_merchant">
              <p class="text-center"><b>Available at</b></p>
              <div class="clear height-15"></div>
              <div class="row">
                <?php foreach ($logo_m as $key => $value): ?>
                <div class="col-md-3 col-sm-4 col-xs-6">
                  <div class="items">
                    <img class="img-responsive" src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(160, 120, '/images/merchant/'.$value->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="<?php echo $value->title; ?>">
                  </div>
                </div>
                <?php endforeach ?>
              </div>
              <div class="clear"></div>
            </div>
            <?php endif ?>

          <div class="clear"></div>
        </div>

        <div class="clear height-10"></div>
        <!-- <div class="height-20"></div> -->
        <div class="mw-783 tengah text-center content-text ins_bottom_productLet">
            <div class="height-5"></div>
            <h3><?php echo $this->setting['product_title'] ?></h3>
            <?php echo $this->setting['product_content'] ?>
            <div class="clear"></div>
            <a href="<?php echo CHtml::normalizeUrl(array('/home/gema')); ?>" class="custom-gema btn btn-link"><i class="fa fa-chevron-right"></i> &nbsp;GE-MA</a>
            <div class="clear height-15"></div>
            <!-- <div class="clear height-25"></div> -->
            <div class="clear"></div>
            <div class="shares-text">
                  <span class="inline-t">SHARE</span>&nbsp;&nbsp; / &nbsp;&nbsp;<a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode(Yii::app()->request->hostInfo.Yii::app()->request->url) ?>">FACEBOOK</a>&nbsp;&nbsp; /
                  &nbsp;&nbsp;<a target="_blank" href="https://plus.google.com/share?url=<?php echo urlencode(Yii::app()->request->hostInfo.Yii::app()->request->url) ?>">GOOGLE PLUS</a>&nbsp;&nbsp; /
                  &nbsp;&nbsp;<a target="_blank" href="https://twitter.com/home?status=<?php echo urlencode(Yii::app()->request->hostInfo.Yii::app()->request->url) ?>">TWITTER</a>
                </div>
          
            <div class="clear"></div>
        </div>


        <div class="clear height-30"></div>
        <div class="clear"></div>
      </div>
    </div>

  </section>


  <div class="clear"></div>
</div>