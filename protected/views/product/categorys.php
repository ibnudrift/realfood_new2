<!-- Start fcs -->
<?php
/*
$slide = ViewSlide::model()->findAll('language_id = :language_id AND topik_id = 5 ORDER BY sort ASC', array(':language_id'=>$this->languageID));
?>
<div class="fcs-wrapper outers_fcs_wrapper prelatife wrapper-slide home">
    <div id="myCarousel_home" class="carousel carousel-fade" data-ride="carousel" data-interval="4500">
            <div class="carousel-inner">
                <?php foreach ($slide as $key => $value): ?>
                <div class="carousel-item <?php if ($key == 0): ?>active<?php endif ?> home-slider-new">
                <img class="w-100 d-none d-sm-block" src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(1920, 1078, '/images/slide/'.$value->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="First slide">
                <img class="w-100 d-block d-sm-none" src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(375, 667, '/images/slide/'.$value->image2 , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="First slide">
                    <div class="carousel-caption caption-slider-home mx-auto">
                        <div class="prelative container mx-auto">
                            <div class="bxsl_tx_fcs">
                                <div class="row no-gutters">
                                    <div class="col-md-60">
                                        <?php echo $value->subtitle ?>
                                        <div class="subtitle py-4">
                                            <?php echo $value->content ?>
                                            <div class="pt-5"></div>
                                            <a href="<?php echo $value->url ?>">LEARN MORE</a>
                                        </div>
                                    </div>
                                    <div class="col-md-27"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endforeach ?>
            </div>
    </div>
    <div class="clear-both"></div>
</div>
<!-- End fcs -->
*/ ?>

<section class="back-white block-outers-product-category py-5">
	<div class="prelative container">
		<div class="inner-section py-5 text-center">
			<div class="tops-sect content-text">
				<h2 class="small-title">PRODUCTS</h2>	
				<div class="lgo_company d-block mx-auto py-4">
					<img src="<?php echo Yii::app()->baseUrl.'/images/category/'. $strCategory->image; ?>" alt="" class="img img-fluid">
				</div>
				<?php echo $strCategory->content; ?>
			</div>
			<div class="py-4 mb-2"></div>
			<div class="full-banner">
				<img src="<?php echo Yii::app()->baseUrl.'/images/category/'. $strCategory->benefit_image_2; ?>" alt="" class="img img-fluid"></div>

			<div class="py-4 mb-2"></div>
			<div class="middles-productsec py-2">
				<?php 
				$products_cat = PrdCategoryFilter::model()->findAll('category_id = :category_id', array(':category_id'=> $_GET['category']));
				?>
				<?php if ( count($products_cat) > 0 ): ?>
				<div class="tops_filters_category pb-5">
					<h6>CHOOSE HOW YOU WANT TO BUY</h6>
					<div class="py-3"></div>
					<ul class="list-inline filter_cat justify-content-center">
					  <?php foreach ($products_cat as $key => $value): ?>
					  <li class="list-inline-item <?php if ($_GET['type_prd'] == $value->id): ?>active<?php endif ?>">
					  	<a href="<?php echo CHtml::normalizeUrl(array('/product/index/', 'category'=> intval($_GET['category']), 'type_prd'=> $value->id)); ?>">
					  		<?php echo strtoupper($value->nama) ?>
					  	</a>
					  </li>
					  <?php endforeach ?>
					</ul>
				</div>
				<?php else: ?>
				<div class="py-5"></div>
				<?php endif ?>

				<div class="lists-products-item">

					<div class="row">
						<?php foreach ($product->getData() as $key => $value): ?>
						<div class="col-md-20">
							<div class="items pb-4">
								<div class="picture">
									<a href="<?php echo CHtml::normalizeUrl(array('/product/detail', 'id'=> $value->id, 'lang'=>Yii::app()->language)); ?>"><img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(397, 318, '/images/product/'.$value->image2 , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img-fluid d-block mx-auto"></a>
								</div>
								<div class="info pt-2">
									<h6 class="title-prouct"><?php echo strtoupper($value->description->name) ?></h6>
									<div class="py-3"></div>
									<p><?php echo nl2br($value->description->intro_desc) ?></p>
									<div class="py-2"></div>
									<a href="<?php echo CHtml::normalizeUrl(array('/product/detail', 'id'=> $value->id, 'lang'=>Yii::app()->language)); ?>" class="btn btn-defaults_set">BUY NOW</a>
								</div>
							</div>
						</div>
						<?php endforeach ?>
					</div>
                    <?php $this->widget('CLinkPager', array(
                        'pages' => $product->getPagination(),
                        'header'=>'',
                        'htmlOptions'=>array('class'=>'pagination'),
                        'selectedPageCssClass'=>'active',
                    )) ?>

					<div class="clear clearfix"></div>
				</div>
			</div>
			<div class="py-3"></div>
			<div class="py-4 mb-2"></div>
			<div class="full-banner"><img src="<?php echo Yii::app()->baseUrl.'/images/category/'. $strCategory->benefit_image_3; ?>" alt="" class="img img-fluid"></div>
		</div>
		<!-- End inner section -->

		<div class="clear"></div>
	</div>
</section>