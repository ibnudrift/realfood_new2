<!-- Start fcs -->
<?php
$slide = ViewSlide::model()->findAll('language_id = :language_id AND topik_id = 5 ORDER BY sort ASC', array(':language_id'=>$this->languageID));
?>
<div class="fcs-wrapper outers_fcs_wrapper prelatife wrapper-slide home">
    <div id="myCarousel_home" class="carousel carousel-fade" data-ride="carousel" data-interval="4500">
            <div class="carousel-inner">
                <?php foreach ($slide as $key => $value): ?>
                <div class="carousel-item <?php if ($key == 0): ?>active<?php endif ?> home-slider-new">
                <img class="w-100 d-none d-sm-block" src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(1920, 1078, '/images/slide/'.$value->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="First slide">
                <img class="w-100 d-block d-sm-none" src="<?php echo Yii::app()->baseUrl; ?>/images/slide/<?php echo $value->image2 ?>" alt="First slide">
                    <div class="carousel-caption caption-slider-home mx-auto">
                        <div class="prelative container mx-auto">
                            <div class="bxsl_tx_fcs">
                                <div class="row no-gutters">
                                    <div class="col-md-60">
                                        <?php echo $value->subtitle ?>
                                        <div class="subtitle py-4">
                                            <?php echo $value->content ?>
                                            <div class="pt-5"></div>
                                            <a href="<?php echo $value->url ?>">LEARN MORE</a>
                                        </div>
                                    </div>
                                    <div class="col-md-27"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endforeach ?>
            </div>
            <a class="carousel-control-prev" href="#myCarousel_home" role="button" data-slide="prev">
	            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
	            <span class="sr-only">Previous</span>
	          </a>
	          <a class="carousel-control-next" href="#myCarousel_home" role="button" data-slide="next">
	            <span class="carousel-control-next-icon" aria-hidden="true"></span>
	            <span class="sr-only">Next</span>
	          </a>
            <?php /*<ol class="carousel-indicators">
                <li data-target="#myCarousel_home" data-slide-to="" class="active"></li>
            </ol>*/ ?>
    </div>
    <div class="clear-both"></div>
</div>
<!-- End fcs -->

<?php 

$data_category = [
				1 => [
					'name'=>'product 1',
					'picture'=>'picts-prd_1.jpg',
					],
					[
					'name'=>'product 2',
					'picture'=>'picts-prd_2.jpg',
					],
					[
					'name'=>'product 3',
					'picture'=>'picts-prd_3.jpg',
					],
					[
					'name'=>'product 4',
					'picture'=>'picts-prd_4.jpg',
					],
					[
					'name'=>'product 5',
					'picture'=>'picts-prd_5.jpg',
					],
				];

            $criteria=new CDbCriteria;
            $criteria->with = array('description');
            $criteria->addCondition('description.language_id = :language_id');
            $criteria->params[':language_id'] = $this->languageID;
            $criteria->order = 't.sort ASC';
            $data_category = PrdCategory::model()->findAll($criteria);
?>

<section class="back-white block-outers-product-category py-5">
	<div class="prelative container">
		<div class="inner-section py-5 text-left">
			
			<div class="py-2"></div>
			<div class="cv_content">
				<?php echo $this->setting['product_landing_content'] ?>
				<div class="clear"></div>
			</div>

			<div class="py-4"></div>

			<div class="lists_featured_categorys">
				<div class="row">
					<?php foreach ($data_category as $key => $value): ?>
					<?php if ($value->feature_type == 1): ?>
						<div class="col-md-60 horizontal my-3 d-none d-sm-block">
							<div class="item-block prelatife">
								<a <?php if ($value->status == 0): ?>class="click_disabled"<?php endif ?> href="<?php echo CHtml::normalizeUrl(array('/product/index', 'category'=>$value->id,'slug'=> Slug::Create($value->description->name), 'lang'=>Yii::app()->language)); ?>"><img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(1220,493, '/images/category/'. $value->benefit_image , array('method' => 'adaptiveResize', 'quality' => '90')); ?>" alt="" class="img img-fluid"></a>
							</div>
						</div>
					<?php else: ?>
						<div class="col-md-30 my-3 d-none d-sm-block">
							<div class="item-block prelatife">
								<a <?php if ($value->status == 0): ?>class="click_disabled"<?php endif ?> href="<?php echo CHtml::normalizeUrl(array('/product/index', 'category'=>$value->id, 'slug'=> Slug::Create($value->description->name), 'lang'=>Yii::app()->language)); ?>"><img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(595,807, '/images/category/'. $value->benefit_image_1 , array('method' => 'adaptiveResize', 'quality' => '90')); ?>" alt="" class="img img-fluid"></a>
							</div>
						</div>
					<?php endif ?>

					<div class="col-md-30 my-3 d-block d-sm-none">
						<div class="item-block prelatife">
							<a <?php if ($value->status == 0): ?>class="click_disabled"<?php endif ?> href="<?php echo CHtml::normalizeUrl(array('/product/index', 'category'=>$value->id, 'slug'=> Slug::Create($value->description->name), 'lang'=>Yii::app()->language)); ?>"><img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(595,807, '/images/category/'. $value->benefit_image_1 , array('method' => 'adaptiveResize', 'quality' => '90')); ?>" alt="" class="img img-fluid"></a>
						</div>
					</div>
					<?php endforeach ?>

				</div>
				<div class="clear"></div>
			</div>

			<div class="clear clearfix"></div>
		</div>
		<!-- End inner section -->
		<div class="clear"></div>
	</div>
</section>

<script type="text/javascript">
	$('.click_disabled').click(function(e){
		e.preventDefault();
		return false;
	});
</script>