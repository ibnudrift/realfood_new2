<?php	
	$productsn_brand = [
						1 => [
							'picture'=>'brands-logon_1.jpg',
							'name'=>'brands img',
							],
							[
							'picture'=>'brands-logon_2.jpg',
							'name'=>'brands img',
							],
							[
							'picture'=>'brands-logon_3.jpg',
							'name'=>'brands img',
							],
							[
							'picture'=>'brands-logon_4.jpg',
							'name'=>'brands img',
							],
							[
							'picture'=>'brands-logon_5.jpg',
							'name'=>'brands img',
							],
							[
							'picture'=>'brands-logon_6.jpg',
							'name'=>'brands img',
							],
							[
							'picture'=>'brands-logon_7.jpg',
							'name'=>'brands img',
							],
							[
							'picture'=>'brands-logon_8.jpg',
							'name'=>'brands img',
							],
							[
							'picture'=>'brands-logon_9.jpg',
							'name'=>'brands img',
							],
							[
							'picture'=>'brands-logon_10.jpg',
							'name'=>'brands img',
							],
							[
							'picture'=>'brands-logon_11.jpg',
							'name'=>'brands img',
							],
							[
							'picture'=>'brands-logon_12.jpg',
							'name'=>'brands img',
							],
						];
?>
<section class="back-white block-outers-product-category py-5">
	<div class="prelative container">
		<div class="inner-section py-5 text-left content-text">
			<div class="middles-inner outer_blocks_detail_products">

				<div class="tops pb-5 text-center">
					<h2 class="title-page-products">PRODUCTS</h2>
					<div class="py-2 my-1"></div>
					<div class="centersn_image">
						<img src="<?php echo Yii::app()->baseUrl; ?>/images/category/<?php echo $category->image ?>" alt="" class="img img-fluid d-block mx-auto">
					</div>
				</div>

				<div class="row">
					<div class="col-md-30">

						<div class="pictures pictures_bg">
							<img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(614,582, '/images/product/'.$data->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img img-fluid d-block mx-auto">
						</div>
                    	<?php 
                    		$md_image = PrdProductImage::model()->findAll('product_id = :product_ids', array(':product_ids' => $data->id));
                    	?>
                    	<?php if ( count($md_image) > 0): ?>
                        <div class="bottoms_thumbnail pt-4">
                            <ul class="list-inline m-0">
                            	<li class="list-inline-item">
                            		<a href="#" data-src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(614,582, '/images/product/'.$data->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>">
                            			<img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(80,80, '/images/product/'.$data->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" class="img img-fluid">
                            		</a>
                            	</li>
                            	<?php foreach ($md_image as $key_img => $value_img): ?>
                                <li class="list-inline-item">
                                	<a href="#" data-src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(614,582, '/images/product/'.$value_img->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>">
                                		<img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(80,80, '/images/product/'.$value_img->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" class="img img-fluid">
                                	</a>
                                </li>
                            	<?php endforeach ?>
                            </ul>
                        </div>
                        <script type="text/javascript">
                        	$(document).ready(function(){

                        		var bigs_pict = $('.pictures_bg img');
                        		var s_linkthumb = $('.bottoms_thumbnail ul li');

                        		$(s_linkthumb).find('a').on('mouseenter', function(){
                        			var inds_active = $(this).attr('data-src');
                        			$(bigs_pict).attr('src', inds_active);

                        			return false;
                        		});

                        	});
                        </script>
                    	<?php endif ?>

					</div>
					<div class="col-md-30">
						<div class="info py-5">
							<div class="py-2"></div>
							<h3 class="titles-default"><?php echo $data->description->name ?></h3>
							<?php echo $data->description->desc ?>
							<?php 
							$datas = unserialize($data->data);
							?>
							<?php if ($datas['prices'] and $datas['prices'] != 0): ?>
							<p class="bold-greens-price">RP <?php echo number_format($datas['prices']) ?>,-</p>
							<?php endif ?>
							
							<p>Visit Realfood Official Store or Contact Realfood Customer Care:</p>

							<div class="blocks_linkl_toped">
								<ul class="list-inline m-0">
									<?php if (isset($datas['tokopedia']) and $datas['tokopedia'] != '#'): ?>
									<li class="list-inline-item">
										<?php 
										$link_full_toped = '';
										if (strpos($datas['tokopedia'], 'http') !== false) { 
										    $link_full_toped = $datas['tokopedia'];
										}else{
											$link_full_toped = 'http://'.$datas['tokopedia'];
										}
										?>
										<a target="_blank" href="<?php echo $link_full_toped ?>"><img src="<?php echo $this->assetBaseurl ?>new/tokopedia.png" alt="" class="img img-fluid"></a>
									</li>
									<?php endif ?>
									<?php if (isset($datas['lazada']) and $datas['lazada'] != '#'): ?>
										<?php 
										$link_full_lazada = '';
										if (strpos($datas['lazada'], 'http') !== false) { 
										    $link_full_lazada = $datas['lazada'];
										}else{
											$link_full_lazada = 'http://'.$datas['lazada'];
										}
										?>
									<li class="list-inline-item">
										<a target="_blank" href="<?php echo $link_full_lazada ?>"><img src="<?php echo $this->assetBaseurl ?>new/shopee.png" alt="" class="img img-fluid"></a>
									</li>
									<?php endif ?>
									<?php if (isset($datas['jdid']) and $datas['jdid'] != '#'): ?>
										<?php 
										$link_full_jdid = '';
										if (strpos($datas['jdid'], 'http') !== false) { 
										    $link_full_jdid = $datas['jdid'];
										}else{
											$link_full_jdid = 'http://'.$datas['jdid'];
										}
										?>
									<li class="list-inline-item">
										<a target="_blank" href="<?php echo $link_full_jdid ?>"><img src="<?php echo $this->assetBaseurl ?>new/jd-id.png" alt="" class="img img-fluid"></a>
									</li>
									<?php endif ?>
									<?php if ($datas['lines'] != '#' AND $datas['lines'] != ''): ?>
									<li class="list-inline-item">
										<a target="_blank" href="<?php echo $datas['lines'] ?>"><img src="<?php echo $this->assetBaseurl ?>new/line.png" alt="" class="img img-fluid"></a>
									</li>
									<?php endif ?>
									<li class="list-inline-item">
										<a target="_blank" href="https://wa.me/<?php echo str_replace('08', '628', $this->setting['contact_wa_order']) ?>"><img src="<?php echo $this->assetBaseurl ?>new/whatsapp.png" alt="" class="img img-fluid"></a>
									</li>
									
								</ul>
							</div>
							<div class="clear"></div>
						</div>
					</div>
				</div>

				<div class="py-4"></div>
				<div class="row">
					<div class="col-md-60">
						<div class="blocks_nutritions">
							<?php echo $data->description->nutrisi ?>
							<div class="clear"></div>
						</div>
					</div>
				</div>

				<div class="clear"></div>
			</div>



			<?php if ( count($data_brand) > 0 ): ?>
			<div class="py-5 my-2"></div>
			<div class="block-banners_sect text-center">
				<h5 class="small-title">YOU CAN ALSO FIND REALFOOD BIRD’S NEST PRODUCT AT</h5>
				<div class="py-4 mb-1"></div>
				<ul class="list-inline justify-content-center m-0 text-center">
					<?php foreach ($data_brand as $key => $value): ?>
					<li class="list-inline-item">
						<img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(246,117, '/images/brand/'.$value->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img img-fluid">
					</li>
					<?php endforeach ?>
				</ul>
			</div>
			<?php endif ?>

			<?php if ( count($product) > 0 ): ?>
			<div class="py-5 my-2"></div>
			<div class="block-banners_sect text-center">
				<h5 class="small-title">YOU MIGHT WANT TO LOOK AT THIS OTHER REALFOOD PRODUCTS</h5>
				<div class="py-4 mb-1"></div>

				<div class="lists-products-item">

					<div class="row">

						<?php foreach ($product as $key => $value): ?>
						<div class="col-md-20">
							<div class="items pb-4">
								<div class="picture">
									<a href="<?php echo CHtml::normalizeUrl(array('/product/detail', 'id'=> $value->id, 'lang'=>Yii::app()->language)); ?>"><img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(397, 318, '/images/product/'.$value->image2 , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img-fluid d-block mx-auto"></a>
								</div>
								<div class="info pt-2">
									<h6 class="title-prouct"><?php echo strtoupper($value->description->name) ?></h6>
									<div class="py-3"></div>
									<p><?php echo nl2br($value->description->intro_desc) ?></p>
									<div class="py-2"></div>
									<a href="<?php echo CHtml::normalizeUrl(array('/product/detail', 'id'=> $value->id, 'lang'=>Yii::app()->language)); ?>" class="btn btn-defaults_set">BUY NOW</a>
								</div>
							</div>
						</div>
						<?php endforeach ?>

					</div>
					<div class="clear clearfix"></div>
				</div>

				<div class="clear"></div>
			</div>
			<?php endif ?>

			<div class="clear clearfix"></div>
		</div>
		<!-- End inner section -->
		<div class="clear"></div>
	</div>
</section>