<div class="back-white hidden-xs">
    <div class="clear height-50"></div>
    <div class="clear height-50"></div>
</div>
<div class="back-grey h61">    
    <div class="prelatife container">
        <div class="top-hov-menu-productchoose text-center">
            <ul class="list-inline">
<?php
$gallery = ViewGallery::model()->findAll('language_id = :language_id ORDER BY date_input DESC', array(':language_id'=>$this->languageID));
?>
                <?php foreach ($gallery as $key => $value): ?>
                <li <?php if ($value->id == $_GET['id']): ?>class="active"<?php endif ?>><a href="<?php echo CHtml::normalizeUrl(array('detail', 'id'=>$value->id)); ?>"><?php echo $value->title ?></a></li>
                <?php endforeach ?>

            </ul>
            <div class="clear"></div>
        </div>
        <div class="clear"></div>
    </div>
</div>
<section class="back-white insides-product-detail">
    <div class="row hg100per">
        <div class="col-md-5 col-lg-5 hg100per">
            <!-- <div class="background-cloud-awan"></div> -->
            <!-- <div class="pict-full visible-lg">
                <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(620,1114, '/images/gallery/'.$data->image_background , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img-responsive">
            </div> -->
            <div class="background-cloud-awan" style="bacground-image:url(<?php echo Yii::app()->baseUrl.ImageHelper::thumb(620,1114, '/images/gallery/'.$data->image_background , array('method' => 'adaptiveResize', 'quality' => '90')) ?>);">
                <div class="table-center">
                    <div class="pict-product-big left2 text-center hidden-lg">
                        <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(184,559, '/images/gallery/'.$data->image , array('method' => 'resize', 'quality' => '90')) ?>" alt="" class="img-responsive">
                    </div>
                </div>
            </div>
            <div class="clear"></div>
        </div>
        <div class="col-md-7 col-lg-7">
            <div class="mw-775 margin-leftmin90">
                <div class="clear height-135"></div>
                <div class="row">
                    <div class="col-md-4 col-lg-4 visible-lg">
                        <div class="pict-product-big">
                            <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(184,559, '/images/gallery/'.$data->image , array('method' => 'resize', 'quality' => '90')) ?>" alt="" class="img-responsive">
                        </div>
                    </div>
                    <div class="col-md-12 col-lg-8">
                        <div class="cont-right">
                            <div class="row">
                                <div class="col-md-9 col-sm-10">
                                    <h3 class="sub-titles"><b><?php echo $data->description->title_bold ?></b> 
                                        <?php echo $data->description->title ?></h3>
                                </div>
                                <div class="col-md-3 col-sm-2">
                                    <div class="text-right back-toright">
                                        <a href="<?php echo CHtml::normalizeUrl(array('index')); ?>">
                                            <img src="<?php echo Yii::app()->baseUrl; ?>/asset/images/icons-back-btn-hover-left.jpg" alt="" class="inline-pict">
                                            BACK
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="clear height-45"></div><div class="height-3"></div>
                            <div class="list-info-detail">
                                <div class="list">
                                    <div class="sub-title">
                                        <img src="<?php echo Yii::app()->baseUrl; ?>/asset/images/lines-black-sub-title-productdata.jpg" alt="" class="inline-pict">
                                        DESCRIPTION
                                        <div class="clear"></div>
                                    </div>
                                    <div class="clear"></div>
                                    <div class="padding-left-50">
                                        <?php echo $data->description->content ?>
                                    </div>
                                </div>
                                <div class="list">
                                    <div class="sub-title">
                                        <img src="<?php echo Yii::app()->baseUrl; ?>/asset/images/lines-black-sub-title-productdata.jpg" alt="" class="inline-pict">
                                        PRODUCT FEATURES
                                        <div class="clear"></div>
                                    </div>
                                    <div class="clear"></div>
                                    <div class="padding-left-50">
                                        <?php echo $data->description->features ?>
                                    </div>
                                </div>
                                <div class="list">
                                    <div class="sub-title">
                                        <img src="<?php echo Yii::app()->baseUrl; ?>/asset/images/lines-black-sub-title-productdata.jpg" alt="" class="inline-pict">
                                        PRODUCT BENEFITS
                                        <div class="clear"></div>
                                    </div>
                                    <div class="clear"></div>
                                    <div class="padding-left-50">
                                        <?php echo $data->description->benefit ?>
                                    </div>
                                </div>
                                <div class="list">
                                    <div class="sub-title">
                                        <img src="<?php echo Yii::app()->baseUrl; ?>/asset/images/lines-black-sub-title-productdata.jpg" alt="" class="inline-pict">
                                        NUTRITION FACTS
                                        <div class="clear"></div>
                                    </div>
                                    <div class="clear"></div>
                                    <div class="padding-left-50">
                                        <p><img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(403,2000, '/images/gallery/'.$data->image_nutrition , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt=""></p>
                                    </div>
                                </div>
                                <div class="clear"></div>
                            </div>
                            <div class="clear height-5"></div>
                            <div class="btns-how-get-product">
                                <a class="fancy" href="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(800,800, '/images/gallery/'.$data->image2 , array('method' => 'resize', 'quality' => '90')) ?>">How to get this product</a>
                            </div>

                            <div class="clear height-50"></div>

                            <div class="clear"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</section>

<section class="back-grey2-bottomproduct-detail hidden">
    <div class="prelatife container text-center">
        <div class="shares-text text-center">
                  <span class="inline-t">SHARE</span>&nbsp;&nbsp; / &nbsp;&nbsp;<a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode(Yii::app()->request->hostInfo.Yii::app()->request->url) ?>">FACEBOOK</a>&nbsp;&nbsp; /
                  &nbsp;&nbsp;<a target="_blank" href="https://plus.google.com/share?url=<?php echo urlencode(Yii::app()->request->hostInfo.Yii::app()->request->url) ?>">GOOGLE PLUS</a>&nbsp;&nbsp; /
                  &nbsp;&nbsp;<a target="_blank" href="https://twitter.com/home?status=<?php echo urlencode(Yii::app()->request->hostInfo.Yii::app()->request->url) ?>">TWITTER</a>
                </div>
        <div class="clear"></div>
    </div>
</section>
<div class="back-grey2" style="background:#EFEEE8;">
    <div class="clear height-30"></div>
</div>

<script type="text/javascript">
  $( window ).load(function() {
    var wbody = $(window).width();

    if (wbody > 980){
      var heightk = $('.mw-775.margin-leftmin90').height();
      $('.background-cloud-awan').css("height", heightk+"px");
    };

    if (wbody >= 768 && wbody < 980){
      var heightk2 = $('.mw-775.margin-leftmin90').height();
      heightk2 = heightk2 - 250;
      $('.background-cloud-awan').css("height", heightk2+"px");
    };
    
    if (wbody > 1280){
        $('.pict-full.visible-lg img').css("min-height", "1150px");
        // $('.background-cloud-awan').css("height", "100%");
        
        var heightk12 = $('.mw-775.margin-leftmin90').height();
        $('.col-md-5.col-lg-5.hg100per, .background-cloud-awan').css("height", heightk12+"px");
    };
  });
  
  $( window ).resize(function() {
    var wbody = $(window).width();
    if (wbody > 980){
      var heightk = $('.mw-775.margin-leftmin90').height();
      $('.background-cloud-awan').css("height", heightk+"px");
    }
    if (wbody >= 768 && wbody < 980){
      var heightk2 = $('.mw-775.margin-leftmin90').height();
      heightk2 = heightk2 - 250;
      $('.background-cloud-awan').css("height", heightk2+"px");
    };
  });

    $( document ).ready(function() {
        $('.fancy').fancybox();
    });
</script>
