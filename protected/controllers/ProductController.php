<?php

class ProductController extends Controller
{

	public function actionLanding()
	{
		$this->pageTitle = 'Products Covered - '.$this->pageTitle;
		$this->layout='//layouts/column1';

		$this->render('landing', array(	
		));
	}

	public function actionCategory()
	{
		$this->pageTitle = 'Products Category - '.$this->pageTitle;
		$this->layout='//layouts/column1';

		$this->render('categorys', array(	
		));
	}

	public function actionDetailPrd()
	{
		$this->pageTitle = 'Products Detail - '.$this->pageTitle;
		$this->layout='//layouts/column2';

		$this->render('prd-details', array(	
		));
	}


	public function actionIndex()
	{
		$criteria=new CDbCriteria;

		$criteria->with = array('description');

		// Mengatur Order
		if ($_GET['order'] == 'new-old') {
			$criteria->order = 'date DESC';
		} elseif($_GET['order'] == 'old-new') {
			$criteria->order = 'date ASC';
		} elseif($_GET['order'] == 'hight-low') {
			$criteria->order = 'harga DESC';
		} elseif($_GET['order'] == 'low-hight') {
			$criteria->order = 'harga ASC';
		} else {
			$criteria->order = 'date DESC';
		}
		

		$criteria->addCondition('status = "1"');
		
		$criteria->addCondition('description.language_id = :language_id');
		// $criteria->addCondition('categoryView.language_id = :language_id');
		// $criteria->addCondition('categoryTitle.language_id = :language_id');
		$criteria->params[':language_id'] = $this->languageID;
	
		if (isset($_GET['category']) AND $_GET['category'] != '') {
			$subMenuCategory = PrdCategory::model()->findAll('parent_id = :parent_id', array(':parent_id'=>intval($_GET['category'])));
			if (count($subMenuCategory) > 0) { // cek apakah mempunyai submenu
				$idCategory = array();
				foreach ($subMenuCategory as $key => $value) {
					$idCategory[] = $value->id;
					$subMenuCategory2 = PrdCategory::model()->findAll('parent_id = :parent_id', array(':parent_id'=>$value->id));
					if (count($subMenuCategory) > 0) {
						foreach ($subMenuCategory2 as $k => $v) {
							$idCategory[] = $v->id;
						}
					}
				}
				$criteria->addInCondition('t.category_id', $idCategory);
				$strCategory = ViewCategory::model()->find('id = :id AND language_id = :language_id', array(':id'=>intval($_GET['category']), ':language_id'=>$this->languageID))->name;
			}else{
				$criteria->addCondition('t.category_id = :category');
				$criteria->params[':category'] = intval($_GET['category']);
				$cekSubOrNot = PrdCategory::model()->find('parent_id != 0 AND id = :id', array(':id'=>intval($_GET['category']) ));
				if ($cekSubOrNot != null) {
					$strCategory = ViewCategory::model()->find('id = :id AND language_id = :language_id', array(':id'=>$cekSubOrNot->parent_id, ':language_id'=>$this->languageID));
				}else{
					$strCategory = ViewCategory::model()->find('id = :id AND language_id = :language_id', array(':id'=> intval($_GET['category']), ':language_id'=>$this->languageID));
				}
			}
		}

		if ($_GET['special'] != '') {
			$criteria->addCondition('t.terbaru = :terbaru');
			$criteria->params[':terbaru'] = 1;
		}

		if ( isset($_GET['type_prd']) AND $_GET['type_prd'] != 0) {
			$criteria->addCondition('t.product_types = :product_types');
			$criteria->params[':product_types'] = intval($_GET['type_prd']);
		}

		if ($_GET['q'] != '') {
            $criteria->addCondition('(description.name LIKE :q OR t.tag LIKE :q)');
            $criteria->params[':q'] = '%'.$_GET['q'].'%';
		}

		if ($_GET['pagesize'] != '') {
			$pageSize = $_GET['pagesize'];
		} else {
			$pageSize = 15;
		}

        $criteria->order = 't.sorts ASC';

		$product = new CActiveDataProvider('PrdProduct', array(
			'criteria'=>$criteria,
		    'pagination'=>array(
		        'pageSize'=>$pageSize,
		    ),
		));
		
		$this->layout='//layouts/column2';

		$this->render('categorys', array(
			'product'=>$product,
			'strCategory'=>$strCategory,
		)); 
	}	
	
	public function actionDetail($id)
	{
		$criteria=new CDbCriteria;
		$criteria->with = array('description', 'category');
		$criteria->addCondition('t.status = "1"');
		$criteria->addCondition('description.language_id = :language_id');
		$criteria->params[':language_id'] = $this->languageID;
		$criteria->addCondition('t.id = :id');
		$criteria->params[':id'] = intval($id);
		$data = PrdProduct::model()->find($criteria);
		
		// echo "<pre>";
		// print_r($data->attributes);
		// exit;
		if($data===null)
			throw new CHttpException(404,'The requested page does not exist.');

		// if (isset($_GET['category']) AND $_GET{'category'} == '') {
		// 	if ($data->categories->category_id != null) {
		// 		$_GET['category'] = $data->categories->category_id;
		// 	}else{
		// 		$_GET['category'] = 12;
		// 	}
		// }

		$criteria = new CDbCriteria;
		$criteria->addCondition('t.product_id = :product_id');
		$criteria->params[':product_id'] = $data->id;
		$criteria->order = 'id ASC';
		$attributes = PrdProductAttributes::model()->findAll($criteria);

		// $criteria=new CDbCriteria;
		// $criteria->with = array('description', 'category', 'categories');
		// $criteria->order = 'RAND()';
		// $criteria->addCondition('status = "1"');
		// $criteria->addCondition('description.language_id = :language_id');
		// $criteria->params[':language_id'] = $this->languageID;
		// $product = new CActiveDataProvider('PrdProduct', array(
		// 	'criteria'=>$criteria,
		//     'pagination'=>array(
		//         'pageSize'=>4,
		//     ),
		// ));

		$criteria = new CDbCriteria;
		$criteria->with = array('description');
		$criteria->addCondition('description.language_id = :language_id');
		$criteria->params[':language_id'] = $this->languageID;
		$criteria->addCondition('t.id = :id');
		$criteria->params[':id'] = $data->category_id;
		$category = PrdCategory::model()->find($criteria);

		$criteria=new CDbCriteria;
		// , 'categories'
		$criteria->with = array('description', 'category');
		$criteria->order = 't.date DESC';
		$criteria->addCondition('t.status = "1"');
		$criteria->addCondition('t.terlaris = "1"');
		$criteria->addCondition('description.language_id = :language_id');
		$criteria->params[':language_id'] = $this->languageID;
		$criteria->addCondition('t.category_id = :category');
		$criteria->params[':category'] = $data->category_id;
		$criteria->addCondition('t.id != :id');
		$criteria->params[':id'] = intval($data->id);
		$criteria->limit = 3;
		$product = PrdProduct::model()->findAll($criteria);

	    $session=new CHttpSession;
	    $session->open();
	    $login_member = $session['login_member'];

		$criteria = new CDbCriteria;
		$criteria->select = 'SUM(rating) as rating';
		$criteria->addCondition('product_id = :product_id');
		$criteria->params[':product_id'] = $id;
		$criteria->addCondition('t.status = :status');
		$criteria->params[':status'] = 1;
		// $criteria->order = 'date DESC';
		// $criteria->group = 'product_id';
		$this->pageTitle = $data->description->name.' | '.$this->pageTitle;
		$this->layout='//layouts/column2';

		$criteria=new CDbCriteria;
		$criteria->select = "t.*, prd_brand_description.title";
		$criteria->join = "LEFT JOIN prd_brand_description ON prd_brand_description.brand_id=t.id";
		$criteria->addCondition('prd_brand_description.language_id = :language_id');
		$criteria->params[':language_id'] = $this->languageID;

        $data_brand = Brand::model()->findAll($criteria);		
		
		$this->render('prd-details', array(	
			'data' => $data,
			'product' => $product,
			'category' => $category,
			'attributes' => $attributes,
			'model' => $model,
			'data_brand' => $data_brand,
		));
	}

	public function actionDetails()
	{
		$this->pageTitle = 'Our Products - '.$this->pageTitle;
		$this->layout='//layouts/column1';

		$this->render('detail', array(	
				'data' => $data,
				// 'cart' => $cart[$_GET['id']],
			));
	}
	
	public function actionAddcart()
	{
		if ($_POST['id'] != '') {
			if ( ! $_POST['id'])
				throw new CHttpException(404,'The requested page does not exist.');

			$id = $_POST['id'];
			$qty = $_POST['qty'];
			$optional = $_POST['optional'];
			$option = $_POST['option'];

			$model = new Cart;

			$data = PrdProduct::model()->findByPk($id);

			if (is_null($data))
				throw new CHttpException(404,'The requested page does not exist.');

			$model->addCart($id, $qty, $data->harga, $option, $optional);
			
			Yii::app()->user->setFlash('addcart',$qty);
			$this->redirect(array('/product/addcart', 'id'=>$data->id));
		}else{
			$criteria=new CDbCriteria;
			$criteria->with = array('description');
			$criteria->addCondition('status = "1"');
			$criteria->addCondition('description.language_id = :language_id');
			$criteria->params[':language_id'] = $this->languageID;
			$criteria->addCondition('t.id = :id');
			$criteria->params[':id'] = $_GET['id'];
			$data = PrdProduct::model()->find($criteria);
			if($data===null)
				throw new CHttpException(404,'The requested page does not exist.');
			
			$model = new Cart;
			$cart = $model->viewCart($this->languageID);

			$this->render('addcart', array(	
				'data' => $data,
				'cart' => $cart[$_GET['id']],
			));
		}
	}

	public function actionAddcart2()
	{
		if ( ! $_GET['id'])
			throw new CHttpException(404,'The requested page does not exist.');

		$id = $_GET['id'];
		$qty = 1;
		$optional = $_POST['optional'];
		$option = $_POST['option'];

		$model = new Cart;

		$data = PrdProduct::model()->findByPk($id);

		if (is_null($data))
			throw new CHttpException(404,'The requested page does not exist.');

		$model->addCart($id, $qty, $data->harga, $option, $optional);
		
		Yii::app()->user->setFlash('addcart',$qty);
		$this->redirect(array('/product/addcart', 'id'=>$data->id));
	}

	public function actionEdit()
	{
		if ( ! $_POST['id'])
			throw new CHttpException(404,'The requested page does not exist.');

		$id = $_POST['id'];
		$qty = $_POST['qty'];
		$color = $_POST['colour'];
		$option = $_POST['option'];

		$model = new Cart;

		$data = PrdProduct::model()->findByPk($id);

		if (is_null($data))
			throw new CHttpException(404,'The requested page does not exist.');

		$model->addCart($id, $qty, $data->harga, $color, $option);

		// $this->redirect(CHtml::normalizeUrl(array('/cart/shop')));
	}
	
	public function actionDestroy()
	{
		$model = new Cart;
		$model->destroyCart();
	}
	public function actionAddcompare($id)
	{
		$model = new Cart;
		$model->addCompare($id);
	}
	public function actionDeletecompare()
	{
		$model = new Cart;
		$model->deleteCompare($id);
		$this->redirect(CHtml::normalizeUrl(array('/product/index')));
	}
	public function actionViewcompare()
	{
		$model = new Cart;
		$data = $model->viewCompare($id);

		$this->layout='//layoutsAdmin/mainKosong';

		$categoryName = Product::model()->getCategoryName();

		$this->render('viewcompare', array(
			'data'=>$data,
			'categoryName'=>$categoryName,
		));
	}


}