<?php

class HomeController extends Controller
{

	public function actions()
	{
		return array(
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
		);
	}	

	public function actionDummy()
	{
		Dummy::createDummyProduct();
		echo '<META http-equiv="refresh" content="0;URL=http://localhost/dv-computers/home/dummy">
';
	}
	public function actionCroncategory()
	{
		$data = PrdProduct::model()->findAll();
		foreach ($data as $key => $value) {
			$tag = PrdCategory::model()->getBreadcrump($value->category_id, $this->languageID);
			$dataTag = array();
			foreach ($tag as $k => $v) {
				$dataTag[] = $k;
			}
			$value->tag = implode(', ', $dataTag);
			$value->save();
		}
	}

	public function actionIndex()
	{
		// $criteria = new CDbCriteria;
		// $criteria->with = array('description');
		// $criteria->addCondition('status = "1"');
		// $criteria->addCondition('description.language_id = :language_id');
		// $criteria->params[':language_id'] = $this->languageID;
		// $criteria->order = 'date_input DESC';
		// $_GET['category'] = 0;
		// if ($_GET['category'] != '') {
		// 	$criteria->addCondition('t.category_id = :category');
		// 	$criteria->params[':category'] = $_GET['category'];
		// }
		// $product = new CActiveDataProvider('PrdProduct', array(
		// 	'criteria'=>$criteria,
		//     'pagination'=>array(
		//         'pageSize'=>8,
		//     ),
		// ));

		// $this->layout='//layouts/column1';
		// $this->render('index', array(
		// 	'product'=>$product,
		// ));
		$this->pageTitle = $this->pageTitle;
		$this->layout='//layouts/column1';

		$this->render('webnew', array(	
		));

	}

	

	public function actionSugest()
	{

		if ($_POST['q'] != '') {
			$str = '<p id="searchresults">';
	            $criteria=New CDbCriteria ; 
	            $criteria->addCondition('(name LIKE :q OR tag LIKE :q)');
	            $criteria->params[':q'] = '%'.$_POST['q'].'%';
	            $criteria->addCondition('language_id = :language_id');
	            $criteria->params[':language_id'] = $this->languageID;

	            $criteria->order = 'date_input DESC';
	            $criteria->limit = 5;
	            $list = ViewProduct::model()->findAll($criteria);
	            
				$str .= '<span class="category">Search: '.$_POST['q'].'</span>';
	            foreach($list as $value)
	            {
					$str .= '<a href="'.CHtml::normalizeUrl(array('/product/detail', 'id'=>$value->id)).'">
						<span class="searchheading">'.$value->name.'</span>
					</a>';
				}
			// }
			$str .= '<span class="seperator"><a href="'.CHtml::normalizeUrl(array('/product/index', 'q'=>$_POST['q'])).'" title="Sitemap">See other result for '.$_POST['q'].' &gt;</a></span>
			<br class="break">
			</p>
			';
			echo $str;
		}
	}
	public function actionError()
	{
		$this->layout = '//layouts/error';
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else{
				$this->layout='//layouts/column2';

				$this->pageTitle = 'Error '.$error['code'].': '. $error['message'] .' - '.$this->pageTitle;
				$this->render('error', array(
					'error'=>$error,
				));
			}
		}

	}

	public function actionAbout()
	{
		$this->pageTitle = 'About Us - '.$this->pageTitle;
		$this->layout='//layouts/column1';

		$this->render('about', array(	
		));
	}

	public function actionBusiness()
	{
		$this->pageTitle = 'Bussines - '.$this->pageTitle;
		$this->layout='//layouts/column1';
		
		$this->render('bussines', array(	
		));
	}

	public function actionWebnew()
	{
		$this->pageTitle = $this->pageTitle;
		$this->layout='//layouts/column1';

		$this->render('webnew', array(	
		));
	}
	
	public function actionStory()
	{
		$this->pageTitle = 'Story - '.$this->pageTitle;
		$this->layout='//layouts/column1';

		$this->render('story', array(	
		));
	}

	public function actionCareer()
	{
		$this->pageTitle = 'Career - '.$this->pageTitle;
		$this->layout='//layouts/column1';

		$categorys = CareerCategory::model()->findAll();

		// $criteria=new CDbCriteria;
	 //    if (isset($_GET['category'])) {
	 //    	$criteria->addCondition('t.category_id = :ids');
  //       $criteria->params[':ids'] = intval($_GET['category']);   
	 //    }
	 //    $criteria->addCondition('actives = 1');
		// $model = ListCareer::model()->findAll($criteria);

		$this->render('career', array(	
			// 'categorys'=> $categorys,
			// 'data'=> $model,
		));
	}

	public function actionCareer_Detail()
	{
		$this->pageTitle = 'Career - '.$this->pageTitle;
		$this->layout='//layouts/column1';

		$criteria=new CDbCriteria;
    	$criteria->addCondition('t.id = :ids');
    	$criteria->params[':ids'] = intval($_GET['id']);   
	    $criteria->addCondition('t.actives = 1');
		$model = ListCareer::model()->find($criteria);

		$criteria=new CDbCriteria;
		$criteria->addCondition('t.id = :ids');
        $criteria->params[':ids'] = $model->category_id;   
		$categorys = CareerCategory::model()->find($criteria);

		$this->render('career_detail', array(	
			'data'=> $model,
			'categorys'=> $categorys,
		));
	}

	public function actionFaq()
	{
		$this->pageTitle = 'Faq - '.$this->pageTitle;
		$this->layout='//layouts/column1';

		$criteria=new CDbCriteria;
		$criteria->select = "t.*, pg_faq_description.question, pg_faq_description.answer";
		$criteria->join = "
		LEFT JOIN pg_faq_description ON pg_faq_description.faq_id=t.id
		";
		$criteria->addCondition('pg_faq_description.language_id = :language_id');
		$criteria->params = array(
			':language_id'=>2,
		);
		$model = Faq::model()->findAll($criteria);

		$this->render('faq', array(	
			'model' => $model,
		));
	}

	public function actionProduk()
	{
		$this->pageTitle = 'produk - '.$this->pageTitle;
		$this->layout='//layouts/column2';


		$criteria=new CDbCriteria;
		$criteria->with = array('description');
		$criteria->addCondition('description.language_id = :language_id');
		$criteria->params[':language_id'] = $this->languageID;
		$criteria->addCondition('t.id = :ids');
		$criteria->params[':ids'] = intval($_GET['id']);
		$data = PrdProduct::model()->find($criteria);

		$criteria=new CDbCriteria;
		$criteria->with = array('description');
		$criteria->addCondition('description.language_id = :language_id');
		$criteria->params[':language_id'] = $this->languageID;
		$criteria->addCondition('t.id = :id');
		$criteria->params[':id'] = $data->category_id;
		$data_category = PrdCategory::model()->find($criteria);


		$this->render('produk', array(	
			'data' => $data,
			'data_category' => $data_category,
		));
	}

	public function actionProducts()
	{
		$this->pageTitle = 'Products - '.$this->pageTitle;
		
		$this->layout='//layouts/column1';
		$this->render('product', array(	
		));
	}

	public function actionBuilding()
	{
		$this->pageTitle = 'Building  - '.$this->pageTitle;
		$this->layout='//layouts/column1';

		$this->render('building', array(	
		));
	}

	public function actionGema()
	{
		$this->pageTitle = 'GE-MA - '.$this->pageTitle;
		$this->layout='//layouts/column1';

		// $text_hero='';
		// $text_hero2='';
		// $page = isset( $_GET['tab'] )? $_GET['tab']: 'index';
		// $share = array();
		// switch ($page){
		// 	case 'share':
		// 		$page = 'share_vision';
		// 		$text_hero = 'WHAT WE SHARE';
		// 		if ($_GET['id_m'] == '') {
		// 			$share_id = Share::model()->find('1 ORDER BY id');
		// 			$_GET['id_m'] = $share_id->id;
		// 		}
		// 		$share = Share::model()->findByPk($_GET['id_m']);
		// 		$text_hero2 = '&ldquo;'.$share->title.'&rdquo;';
		// 		break;
		// 	case 'whre_we_give':
		// 		$page = 'whre_we_give';
		// 		$text_hero = 'WHERE WE GIVE';
		// 		$text_hero2 = '';
		// 		break;
		// 	case 'how_we_start':
		// 		$page = 'how_we_start';
		// 		$text_hero = 'HOW WE START';
		// 		$text_hero2 = '';
		// 		break;
		// 	case 'our_next_mission':
		// 		$page = 'our_next_mission';
		// 		$text_hero = 'OUR NEXT MISSION';
		// 		$text_hero2 = '';
		// 		break;
			
		// 	default:
		// 		$page = 'gema';
		// 		$text_hero = 'IMPROVING LIVES';
		// 		$text_hero2 = '';
		// 		break;
	// }

		$this->render('gema_new', array(	
			// 'active'=>$page,
			// 'text_hero'=>$text_hero,
			// 'text_hero2'=>$text_hero2,
			// 'share'=>$share,
		));
	}

	public function actionContactus()
	{
		$this->pageTitle = 'Contact - '.$this->pageTitle;
		$this->layout='//layouts/column1';
		
		$this->render('contact', array(	
			// 'model'=>$data,
		));
	}



	public function actionNews()
	{
		$this->pageTitle = 'News & Article - '.$this->pageTitle;
		$this->layout='//layouts/column1';

		$this->render('news', array(	
		));
	}

	public function actionNewsDetail()
	{
		$this->pageTitle = 'News & Article - '.$this->pageTitle;
		$this->layout='//layouts/column1';

		$this->render('news_detail', array(	
		));
	}

	public function actionTermsofuse()
	{
		$this->pageTitle = 'Terms of Use - '.$this->pageTitle;

		$this->render('termsofuse', array(	
		));
	}

	public function actionDesigndetailproduct()
	{
		$this->render('designdetailproduct', array(	
		));
	}

	public function actionDesignaddcart()
	{
		$this->render('addcartproduct', array(	
		));
	}

	public function actionCart()
	{
		$this->render('cartproduct', array(	
		));
	}

	public function actionShippinginfo()
	{
		$this->render('shipinfo', array(	
		));
	}

	public function actionCartsuccess()
	{
		$this->render('cartsucess', array(	
		));
	}

	public function actionDesignlistproduct()
	{
		$this->render('designlistproduct', array(	
		));
	}
	public function actionDesignblog()
	{
		$this->render('designblog', array(	
		));
	}
	public function actionDesignblogdetail()
	{
		$this->render('designblogdetail', array(	
		));
	}

	public function actionContact()
	{
		$this->pageTitle = 'Contact - '.$this->pageTitle;
		$this->layout='//layouts/column1';

		$this->render('contact', array(	
		));
	}

	public function actionLokasi()
	{
		$this->layout='//layouts/column1';
		$criteria = new CDbCriteria;
		if ($_GET['kota'] != '') {
			$criteria->addCondition('kota = :kota');
			$criteria->params[':kota'] = $_GET['kota'];
		}
		$dataAddress2 = array();
		if ($_GET['kota'] != '') {
			$dataAddress = Address::model()->findAll($criteria);
			foreach ($dataAddress as $key => $value) {
				$dataAddress2[$value->kota][] = $value;
			}
		}

		$criteria = new CDbCriteria;
		$criteria->group = 'kota';
		$listKota = Address::model()->findAll($criteria);

		$this->pageTitle = 'Our Sales Locations - '.$this->pageTitle;

		$this->render('lokasi', array(	
			'dataAddress'=>$dataAddress2,
			'listKota'=>$listKota,
		));
	}

	public function actionContact2()
	{
		$this->layout='//layouts/columnIframe';

		$this->pageTitle = 'Contact Us - '.$this->pageTitle;

		$model = new ContactForm;
		$model->scenario = 'insert';

		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];

			if($model->validate())
			{
				// config email
				$messaged = $this->renderPartial('//mail/contact2',array(
					'model'=>$model,
				),TRUE);
				$config = array(
					'to'=>array($model->email, $this->setting['email'], $this->setting['contact_email'], 'renndh2003@hotmail.com', 'dvcomputers.website@outlook.com'),
					'subject'=>'Hi, DV Computers Contact from '.$model->email,
					'message'=>$messaged,
				);
				if ($this->setting['contact_cc']) {
					$config['cc'] = array($this->setting['contact_cc']);
				}
				if ($this->setting['contact_bcc']) {
					$config['bcc'] = array($this->setting['contact_bcc']);
				}
				// kirim email
				Common::mail($config);

				Yii::app()->user->setFlash('success','Thank you for contact us. We will respond to you as soon as possible.');
				$this->refresh();
			}

		}

		$this->render('contact2', array(
			'model'=>$model,
		));
	}
	public function actionContact3()
	{
		$this->layout='//layouts/columnIframe';

		$this->pageTitle = 'Report Bugs & Error - '.$this->pageTitle;

		$model = new ContactForm2;
		$model->scenario = 'insert';

		if(isset($_POST['ContactForm2']))
		{
			$model->attributes=$_POST['ContactForm2'];

			if($model->validate())
			{
				// config email
				$messaged = $this->renderPartial('//mail/contact3',array(
					'model'=>$model,
				),TRUE);
				$config = array(
					'to'=>array($model->email, $this->setting['email'], $this->setting['contact_email'], 'renndh2003@hotmail.com', 'dvcomputers.website@outlook.com'),
					'subject'=>'Report Bugs & Error from '.$model->email,
					'message'=>$messaged,
				);
				if ($this->setting['contact_cc']) {
					$config['cc'] = array($this->setting['contact_cc']);
				}
				if ($this->setting['contact_bcc']) {
					$config['bcc'] = array($this->setting['contact_bcc']);
				}
				// kirim email
				Common::mail($config);

				Yii::app()->user->setFlash('success','Thank you for contact us. We will respond to you as soon as possible.');
				$this->refresh();
			}

		}

		$this->render('contact3', array(
			'model'=>$model,
		));
	}

}

