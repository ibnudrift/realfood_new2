<?php
$this->breadcrumbs=array(
	'List Careers'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List ListCareer', 'icon'=>'th-list','url'=>array('index')),
	array('label'=>'Add ListCareer', 'icon'=>'plus-sign','url'=>array('create')),
	array('label'=>'Edit ListCareer', 'icon'=>'pencil','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete ListCareer', 'icon'=>'trash','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
);
?>

<h1>View ListCareer #<?php echo $model->id; ?></h1>
<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/><br/>
<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'image',
		'image2',
		'nama',
		'nama_sub',
		'location',
		'content',
		'qualification',
		'actives',
	),
)); ?>
