<?php
$this->breadcrumbs=array(
	'List Careers'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List ListCareer','url'=>array('index')),
	array('label'=>'Add ListCareer','url'=>array('create')),
);
?>

<h1>Manage List Careers</h1>
<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/><br/>
<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'list-career-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'image',
		'image2',
		'nama',
		'nama_sub',
		'location',
		/*
		'content',
		'qualification',
		'actives',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
