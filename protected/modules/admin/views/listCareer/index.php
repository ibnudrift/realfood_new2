<?php
$this->breadcrumbs=array(
	'List Careers',
);

$this->pageHeader=array(
	'icon'=>'fa fa-minus',
	'title'=>'List Career',
	'subtitle'=>'Data List Career',
);

$this->menu=array(
	array('label'=>'Add List Career', 'icon'=>'plus-sign','url'=>array('create')),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?>
<?php if(Yii::app()->user->hasFlash('success')): ?>

    <?php $this->widget('bootstrap.widgets.TbAlert', array(
        'alerts'=>array('success'),
    )); ?>

<?php endif; ?>
<h1>List Career</h1>
<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'list-career-grid',
	'dataProvider'=>$model->search(),
	// 'filter'=>$model,
	'enableSorting'=>false,
	'summaryText'=>false,
	'type'=>'bordered',
	'columns'=>array(
		// 'id',
		// 'image',
		// 'image2',
		'nama',
		array(
			'name'=>'actives',
			'header'=>'Active',
			'filter'=>array(
				'0'=>'Non Active',
				'1'=>'Active',
			),
			'type'=>'raw',
			'value'=>'($data->actives == "1") ? "Di Tampilkan" : "Di Sembunyikan"',
		),
		/*
		'nama_sub',
		'location',
		'content',
		'qualification',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template'=>'{update} &nbsp; {delete}',
		),
	),
)); ?>
