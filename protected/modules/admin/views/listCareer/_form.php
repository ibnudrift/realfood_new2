<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'list-career-form',
    'type'=>'horizontal',
	'enableAjaxValidation'=>false,
	'clientOptions'=>array(
		'validateOnSubmit'=>false,
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

<?php echo $form->errorSummary($model); ?>
<?php Yii::import('ext.imperavi-redactor-widget.ImperaviRedactorWidget'); ?>
<?php $this->widget('ImperaviRedactorWidget', array(
    'selector' => '.redactor',
    'options' => array(
        'imageUpload'=> $this->createUrl('/admin/setting/imgUpload', array('type'=>'image')),
        'clipboardUploadUrl'=> $this->createUrl('/admin/setting/imgUpload', array('type'=>'clip')),
    ),
    'plugins' => array(
        'fontfamily' => array('js' => array('fontfamily.js')),
    ),
)); ?>

<div class="widget">
<h4 class="widgettitle">Data ListCareer</h4>
<div class="widgetcontent">
	<?php
	 $models = CareerCategory::model()->findAll(); 
     $nag_resource = CHtml::listData($models, 
                'id', 'nama');  
	?>
	<?php // echo $form->textFieldRow($model,'dates_input', array('class'=>'span5 datepickers','maxlength'=>225)); ?>

	<?php // echo $form->dropDownListRow($model, 'category_id', $nag_resource, array('class'=>'span5')); ?>

	<?php // echo $form->textFieldRow($model,'image2',array('class'=>'span5','maxlength'=>225)); ?>

	<?php echo $form->textFieldRow($model,'nama',array('class'=>'span5','maxlength'=>225)); ?>

	<?php // echo $form->textFieldRow($model,'nama_sub',array('class'=>'span5','maxlength'=>225)); ?>

	<?php // echo $form->textFieldRow($model,'location',array('class'=>'span5','maxlength'=>225)); ?>

	<?php // echo $form->textAreaRow($model,'content',array('rows'=>3, 'class'=>'span8 redactor')); ?>

	<?php // echo $form->textAreaRow($model,'qualification',array('rows'=>3, 'class'=>'span8 redactor')); ?>

	<?php echo $form->fileFieldRow($model,'image', array(
	'hint'=>'<b>Note:</b> Ukuran gambar adalah 1162 x 339px. Gambar yang lebih besar akan ter-crop otomatis, tolong upload foto ukuran horizontal')); ?>

	<?php echo $form->fileFieldRow($model,'image2', array(
	'hint'=>'<b>Note:</b> Ukuran gambar adalah 1162 x 339px. Gambar yang lebih besar akan ter-crop otomatis, tolong upload foto ukuran horizontal')); ?>
	
	<?php if ($model->scenario == 'update'): ?>
	<div class="control-group">
		<label class="control-label">&nbsp;</label>
		<div class="controls">
		<img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(624,417, '/images/career/'.$model->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>"/>
		</div>
	</div>
	<?php endif; ?>

	<?php /*echo $form->fileFieldRow($model,'image2', array(
	'hint'=>'<b>Note:</b> Ukuran gambar adalah 357 x 233px. Gambar yang lebih besar akan ter-crop otomatis, tolong upload foto ukuran horizontal')); ?>
	<?php if ($model->scenario == 'update'): ?>
	<div class="control-group">
		<label class="control-label">&nbsp;</label>
		<div class="controls">
		<img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(357,233, '/images/career/'.$model->image2 , array('method' => 'adaptiveResize', 'quality' => '90')) ?>"/>
		</div>
	</div>
	<?php endif;*/ ?>

	<?php echo $form->dropDownListRow($model, 'actives', array(
        		'1'=>'Di Tampilkan',
        		'0'=>'Di Sembunyikan',
        	)); ?>

		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Add' : 'Save',
		)); ?>
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			// 'buttonType'=>'submit',
			// 'type'=>'info',
			'url'=>CHtml::normalizeUrl(array('index')),
			'label'=>'Batal',
		)); ?>
</div>
</div>
<div class="alert">
  <button type="button" class="close" data-dismiss="alert">×</button>
  <strong>Warning!</strong> Fields with <span class="required">*</span> are required.
</div>

<?php $this->endWidget(); ?>
