<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'blog-form',
    // 'type'=>'horizontal',
	'enableAjaxValidation'=>false,
	'clientOptions'=>array(
		'validateOnSubmit'=>false,
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>
	<div class="widget">
		<!-- <h4 class="widgettitle">Data Pages</h4> -->
		<div class="widgetcontent">

			<div class="multilang pj-form-langbar">
				<?php foreach (Language::model()->getLanguage() as $key => $value): ?>
				<a href="#" data-index="<?php echo $value->id ?>" data-abbr="<?php echo Yii::app()->baseUrl.'/asset/backend/language/'.$value->code.'.png' ?>" class="pj-form-langbar-item <?php if ($value->code==$this->setting['lang_deff']): ?>pj-form-langbar-item-active<?php endif ?>"><abbr style="background-image: url(<?php echo Yii::app()->baseUrl.'/asset/backend/language/'.$value->code.'.png' ?>);"></abbr></a>
				<?php endforeach ?>
			</div>
			<div class="divider5"></div>

			<?php echo $form->errorSummary($model); ?>
			<?php if(Yii::app()->user->hasFlash('success')): ?>
			
			    <?php $this->widget('bootstrap.widgets.TbAlert', array(
			        'alerts'=>array('success'),
			    )); ?>
			
			<?php endif; ?>

			<?php Yii::import('ext.imperavi-redactor-widget.ImperaviRedactorWidget'); ?>
			<?php $this->widget('ImperaviRedactorWidget', array(
			    'selector' => '.redactor',
			    'options' => array(
			        'imageUpload'=> $this->createUrl('/admin/setting/imgUpload', array('type'=>'image')),
			        'clipboardUploadUrl'=> $this->createUrl('/admin/setting/imgUpload', array('type'=>'clip')),
			    ),
			    'plugins' => array(
			        'fontfamily' => array('js' => array('fontfamily.js')),
			    ),
			)); ?>
			<div class="row-fluid">
				<div class="span8">
		        	<?php echo $form->textFieldRow($model, 'title',array('class'=>'span12')); ?>
		        	<?php echo $form->textAreaRow($model, 'content',array('class'=>'span12')); ?>
		        	<?php echo $form->textFieldRow($model, 'text_url',array('class'=>'span12')); ?>
		        	<?php echo $form->textFieldRow($model, 'url',array('class'=>'span12')); ?>
		        	<?php echo $form->dropDownListRow($model, 'color', array(
		        		'black'=>'Black',
		        		'grey'=>'Grey',
		        		'white'=>'White',
		        	), array('class'=>'span12')); ?>
		        	<?php echo $form->dropDownListRow($model, 'alignment', array(
		        		'1'=>'Left',
		        		'2'=>'Right',
		        		'3'=>'Center',
		        	), array('class'=>'span12')); ?>

				</div>
				<div class="span4">
					<?php echo $form->fileFieldRow($model,'image',array(
					'hint'=>'<b>Note:</b> Ukuran gambar landscape')); ?>
					<?php if ($model->scenario == 'update'): ?>
					<div class="control-group">
						<label class="control-label">&nbsp;</label>
						<div class="controls">
						<img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(300,300, '/images/live/'.$model->image , array('method' => 'resize', 'quality' => '90')) ?>"/>
						</div>
					</div>
					<?php endif; ?>
				</div>
			</div>


			
		</div>
	<!-- span 12 -->
	</div>
		<div class="clear"></div>
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Add' : 'Save',
		)); ?>
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			// 'buttonType'=>'submit',
			// 'type'=>'info',
			'url'=>CHtml::normalizeUrl(array('index')),
			'label'=>'Batal',
		)); ?>
<?php $this->endWidget(); ?>
<script type="text/javascript">
if (typeof RedactorPlugins === 'undefined') var RedactorPlugins = {};

RedactorPlugins.advanced = {
    init: function()
    {
        alert(1);
    }
}
jQuery(function( $ ) {
	$('.multilang').multiLang({
	});
})

</script>