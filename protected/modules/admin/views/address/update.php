<?php
$this->breadcrumbs=array(
	'Distribution'=>array('index'),
	// $model->id=>array('view','id'=>$model->id),
	'Edit',
);

$this->pageHeader=array(
	'icon'=>'fa fa-bank',
	'title'=>'Distribution',
	'subtitle'=>'Edit Distribution',
);

$this->menu=array(
	array('label'=>'List Distribution', 'icon'=>'th-list','url'=>array('index')),
	array('label'=>'Add Distribution', 'icon'=>'plus-sign','url'=>array('create')),
	// array('label'=>'View Distribution', 'icon'=>'pencil','url'=>array('view','id'=>$model->id)),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/><br/>
<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>