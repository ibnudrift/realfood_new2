<?php
$this->breadcrumbs=array(
	'Distribution'=>array('index'),
	'Add',
);

$this->pageHeader=array(
	'icon'=>'fa fa-bank',
	'title'=>'Distribution',
	'subtitle'=>'Add Distribution',
);

$this->menu=array(
	array('label'=>'List Distribution', 'icon'=>'th-list','url'=>array('index')),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/><br/>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>