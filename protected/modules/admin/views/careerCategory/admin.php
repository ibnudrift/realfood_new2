<?php
$this->breadcrumbs=array(
	'Career Categories'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List CareerCategory','url'=>array('index')),
	array('label'=>'Add CareerCategory','url'=>array('create')),
);
?>

<h1>Manage Career Categories</h1>
<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/><br/>
<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'career-category-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'nama',
		'image',
		'subtitle',
		'sortings',
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
