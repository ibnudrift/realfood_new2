<?php
$this->breadcrumbs=array(
	'Career Categories'=>array('index'),
	// $model->id=>array('view','id'=>$model->id),
	'Edit',
);

$this->pageHeader=array(
	'icon'=>'fa fa-minus',
	'title'=>'CareerCategory',
	'subtitle'=>'Edit CareerCategory',
);

$this->menu=array(
	array('label'=>'List CareerCategory', 'icon'=>'th-list','url'=>array('index')),
	array('label'=>'Add CareerCategory', 'icon'=>'plus-sign','url'=>array('create')),
	// array('label'=>'View CareerCategory', 'icon'=>'pencil','url'=>array('view','id'=>$model->id)),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/><br/>
<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>