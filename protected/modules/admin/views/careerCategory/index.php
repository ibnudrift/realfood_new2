<?php
$this->breadcrumbs=array(
	'Career Category',
);

$this->pageHeader=array(
	'icon'=>'fa fa-minus',
	'title'=>'Career Category',
	'subtitle'=>'Data Career Category',
);

$this->menu=array(
	array('label'=>'Add Career Category', 'icon'=>'th-list','url'=>array('create')),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?>
<?php if(Yii::app()->user->hasFlash('success')): ?>

    <?php $this->widget('bootstrap.widgets.TbAlert', array(
        'alerts'=>array('success'),
    )); ?>

<?php endif; ?>
<h1>Career Category</h1>
<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'career-category-grid',
	'dataProvider'=>$model->search(),
	// 'filter'=>$model,
	'enableSorting'=>false,
	'summaryText'=>false,
	'type'=>'bordered',
	'columns'=>array(
		// 'id',
		'nama',
		// 'image',
		// 'subtitle',
		// 'sortings',
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template'=>'{update} &nbsp; {delete}',
		),
	),
)); ?>
