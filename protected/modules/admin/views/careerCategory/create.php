<?php
$this->breadcrumbs=array(
	'Career Categories'=>array('index'),
	'Add',
);

$this->pageHeader=array(
	'icon'=>'fa fa-minus',
	'title'=>'CareerCategory',
	'subtitle'=>'Add CareerCategory',
);

$this->menu=array(
	array('label'=>'List CareerCategory', 'icon'=>'th-list','url'=>array('index')),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/><br/>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>