<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'category-form-update',
    // 'type'=>'horizontal',
	'enableAjaxValidation'=>false,
	'clientOptions'=>array(
		'validateOnSubmit'=>false,
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>
<?php Yii::import('ext.imperavi-redactor-widget.ImperaviRedactorWidget'); ?>
<?php $this->widget('ImperaviRedactorWidget', array(
    'selector' => '.redactor',
    'options' => array(
        'imageUpload'=> $this->createUrl('/admin/setting/imgUpload', array('type'=>'image')),
        'clipboardUploadUrl'=> $this->createUrl('/admin/setting/imgUpload', array('type'=>'clip')),
    ),
)); ?>

<div class="widget">
<h4 class="widgettitle">Edit Category</h4>
<div class="widgetcontent">

	<div class="multilang2 pj-form-langbar">
		<?php foreach (Language::model()->getLanguage() as $key => $value): ?>
		<a href="#" data-index="<?php echo $value->id ?>" data-abbr="<?php echo Yii::app()->baseUrl.'/asset/backend/language/'.$value->code.'.png' ?>" class="pj-form-langbar-item <?php if ($value->code==$this->setting['lang_deff']): ?>pj-form-langbar-item-active<?php endif ?>"><abbr style="background-image: url(<?php echo Yii::app()->baseUrl.'/asset/backend/language/'.$value->code.'.png' ?>);"></abbr></a>
		<?php endforeach ?>
	</div>
	<div class="divider5"></div>

	<?php echo $form->errorSummary($model); ?>
	<?php if(Yii::app()->user->hasFlash('success')): ?>
	
	    <?php $this->widget('bootstrap.widgets.TbAlert', array(
	        'alerts'=>array('success'),
	    )); ?>
	
	<?php endif; ?>


	<?php
	foreach ($modelDesc as $key => $value) {
		$lang = Language::model()->getName($key);
		?>
		<div class="pj-multilang-wrap myLanguage2 control-group" style="display: <?php if ($key==$this->setting['lang_deff']): ?>block<?php else: ?>none<?php endif ?>;" data-id="<?php echo $lang->id ?>">

		<?php
		echo $form->labelEx($value, '['.$lang->code.']name');
	    echo $form->textField($value,'['.$lang->code.']name',array('class'=>'span8', 'maxlength'=>100));
	    ?>
	    <span class="pj-multilang-input"><img src="<?php echo Yii::app()->baseUrl.'/asset/backend/language/'.$lang->code.'.png' ?>"></span>
	    <span class="help-inline _em_" style="display: none;">Please correct the error</span>
		</div>
	    <?php
	}
	?>

	<?php
	foreach ($modelDesc as $key => $value) {
		$lang = Language::model()->getName($key);
		?>
		<div class="pj-multilang-wrap myLanguage2 control-group" style="display: <?php if ($key==$this->setting['lang_deff']): ?>block<?php else: ?>none<?php endif ?>;" data-id="<?php echo $lang->id ?>">

		<?php
		echo $form->labelEx($value, '['.$lang->code.']content');
	    echo $form->textArea($value,'['.$lang->code.']content',array('class'=>'span5 redactor', 'maxlength'=>100));
	    ?>
	    <span class="pj-multilang-input"><img src="<?php echo Yii::app()->baseUrl.'/asset/backend/language/'.$lang->code.'.png' ?>"></span>
	    <span class="help-inline _em_" style="display: none;">Please correct the error</span>
		</div>
	    <?php
	}
	?>
	
	<div class="row-fluid">
		<div class="span8">
			<?php echo $form->fileFieldRow($model,'image',array(
			'hint'=>'<b>Note:</b> Picture size width proportional, max height 56px, Larger image will be automatically cropped.', 'style'=>"width: 100%")); ?>
			<?php if ($model->scenario == 'update'): ?>
			<div class="control-group">
				<label class="control-label">&nbsp;</label>
				<div class="controls">
					<img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(230,56, '/images/category/'.$model->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>"/>
				</div>
			</div>
			<?php endif; ?>
		</div>
	</div>

	<div class="row-fluid">
		<div class="span6">
			<?php echo $form->fileFieldRow($model,'benefit_image', array(
			'hint'=>'<b>Note:</b> Picture size width proportional, width: 1220px X height: 493px, Larger image will be automatically cropped.', 'style'=>"width: 100%")); ?>
			<?php if ($model->scenario == 'update'): ?>
			<div class="control-group">
				<label class="control-label">&nbsp;</label>
				<div class="controls">
					<img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(230,56, '/images/category/'.$model->benefit_image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>"/>
				</div>
			</div>
			<?php endif; ?>
		</div>
		<div class="span6">
			<?php echo $form->fileFieldRow($model,'benefit_image_1', array(
			'hint'=>'<b>Note:</b> Picture size width proportional, width: 596px X height: 807px, Larger image will be automatically cropped.', 'style'=>"width: 100%")); ?>
			<?php if ($model->scenario == 'update'): ?>
			<div class="control-group">
				<label class="control-label">&nbsp;</label>
				<div class="controls">
					<img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(160,210, '/images/category/'.$model->benefit_image_1 , array('method' => 'adaptiveResize', 'quality' => '90')) ?>"/>
				</div>
			</div>
			<?php endif; ?>
		</div>
	</div>

	<div class="divider clear clearfix" style="height: 10px;"></div>
	<hr>
	<div class="divider clear clearfix" style="height: 10px;"></div>

	<div class="row-fluid">
		<div class="span6">
			<?php echo $form->fileFieldRow($model,'benefit_image_2', array(
			'hint'=>'<b>Note:</b> Picture size width proportional, max-width: 1223px, Larger image will be automatically cropped.', 'style'=>"width: 100%")); ?>
			<?php if ($model->scenario == 'update'): ?>
			<div class="control-group">
				<label class="control-label">&nbsp;</label>
				<div class="controls">
					<img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(230,75, '/images/category/'.$model->benefit_image_2 , array('method' => 'adaptiveResize', 'quality' => '90')) ?>"/>
				</div>
			</div>
			<?php endif; ?>
		</div>
		<div class="span6">
			<?php echo $form->fileFieldRow($model,'benefit_image_3', array(
			'hint'=>'<b>Note:</b> Picture size width proportional, max-width: 1223px, Larger image will be automatically cropped.', 'style'=>"width: 100%")); ?>
			<?php if ($model->scenario == 'update'): ?>
			<div class="control-group">
				<label class="control-label">&nbsp;</label>
				<div class="controls">
					<img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(230,75, '/images/category/'.$model->benefit_image_3 , array('method' => 'adaptiveResize', 'quality' => '90')) ?>"/>
				</div>
			</div>
			<?php endif; ?>
		</div>
	</div>
	<div class="divider10"></div>
	<div class="row-fluid">
		<div class="span4">
			<?php echo $form->dropDownListRow($model, 'status', array(
				1=>'Active Link',
				0=>'Disable Link',
			), array('empty'=>'-- Pilih Status --')); ?>	
		</div>
		<div class="span4">
			<?php echo $form->dropDownListRow($model, 'feature_type', array(
				1=>'Landscape Type',
				2=>'Potrait Type',
			), array('empty'=>'-- Pilih Type --')); ?>	
		</div>
		<div class="span4">
			<?php echo $form->dropDownListRow($model, 'cover_home_prd', array(
				0=>'Disable to Home',
				1=>'Active to Home',
			), array()); ?>	
		</div>
	</div>


	<br>
	<?php $this->widget('bootstrap.widgets.TbButton', array(
		'buttonType'=>'submit',
		'type'=>'primary',
		'label'=>$model->isNewRecord ? 'Add' : 'Save',
	)); ?>

</div>
</div>

<div class="widget">
	<h4 class="widgettitle">Edit Filter</h4>
	<div class="widgetcontent">
		<table class="table table-bordered responsive">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <!-- <th>&nbsp;</th> -->
                        </tr>
                    </thead>
                    <tbody class="option-add">
                    	<?php 
                    	$max_child = 2;
                    	?>
                    	<?php if (count($PrdCategoryFilter) > 0): ?>
	                    	<?php foreach ($PrdCategoryFilter as $key => $value): ?>
	                        <tr>
	                            <td><input type="text" name="PrdCategoryFilter[][nama]" class="input-block-level" value="<?php echo $value->nama ?>" placeholder="Package name"></td>
	                        </tr>                        
	                    	<?php endforeach ?>

                    	<?php else: ?>
                    		<?php for ($i=1; $i <= $max_child ; $i++) { ?>
                    		<tr>
	                            <td><input type="text" name="PrdCategoryFilter[][nama]" class="input-block-level" value="" placeholder="Package name"></td>
	                        </tr>
                    		<?php } ?>
                    	<?php endif ?>
                    </tbody>
                </table>
				<div class="divider5"></div>
				<br>
				<?php $this->widget('bootstrap.widgets.TbButton', array(
					'buttonType'=>'submit',
					'type'=>'primary',
					'label'=>$model->isNewRecord ? 'Add' : 'Save',
				)); ?>
		<div class="clear clearfix"></div>
	</div>
</div>

<?php
/*
<div class="widget">
<h4 class="widgettitle">Edit Category</h4>
<div class="widgetcontent">

	<?php echo $form->fileFieldRow($model,'benefit_image',array(
	'hint'=>'<b>Note:</b> Picture size 1475 x 540px, Larger image will be automatically cropped.', 'style'=>"width: 100%")); ?>
	<?php if ($model->scenario == 'update'): ?>
	<div class="control-group">
		<label class="control-label">&nbsp;</label>
		<div class="controls">
			<img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(1475,540, '/images/category/'.$model->benefit_image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>"/>
		</div>
	</div>
	<?php endif; ?>

	<?php
	foreach ($modelDesc as $key => $value) {
		$lang = Language::model()->getName($key);
		?>
		<div class="pj-multilang-wrap myLanguage2 control-group" style="display: <?php if ($key==$this->setting['lang_deff']): ?>block<?php else: ?>none<?php endif ?>;" data-id="<?php echo $lang->id ?>">

		<?php
		echo $form->labelEx($value, '['.$lang->code.']benefit_title');
	    echo $form->textArea($value,'['.$lang->code.']benefit_title',array('class'=>'span8', 'maxlength'=>100));
	    ?>
	    <span class="pj-multilang-input"><img src="<?php echo Yii::app()->baseUrl.'/asset/backend/language/'.$lang->code.'.png' ?>"></span>
	    <span class="help-inline _em_" style="display: none;">Please correct the error</span>
		</div>
	    <?php
	}
	?>

	<?php
	foreach ($modelDesc as $key => $value) {
		$lang = Language::model()->getName($key);
		?>
		<div class="pj-multilang-wrap myLanguage2 control-group" style="display: <?php if ($key==$this->setting['lang_deff']): ?>block<?php else: ?>none<?php endif ?>;" data-id="<?php echo $lang->id ?>">

		<?php
		echo $form->labelEx($value, '['.$lang->code.']benefit_content');
	    echo $form->textArea($value,'['.$lang->code.']benefit_content',array('class'=>'span5 redactor', 'maxlength'=>100));
	    ?>
	    <span class="pj-multilang-input"><img src="<?php echo Yii::app()->baseUrl.'/asset/backend/language/'.$lang->code.'.png' ?>"></span>
	    <span class="help-inline _em_" style="display: none;">Please correct the error</span>
		</div>
	    <?php
	}
	?>
	
	<div class="row-fluid">
		<div class="span4">
			<?php echo $form->fileFieldRow($model,'benefit_image_1',array(
			'hint'=>'<b>Note:</b> Picture size 228 x 246px, Larger image will be automatically', 'style'=>"width: 100%")); ?>
			<?php if ($model->scenario == 'update'): ?>
			<div class="control-group">
				<label class="control-label">&nbsp;</label>
				<div class="controls">
					<img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(228,246, '/images/category/'.$model->benefit_image_1 , array('method' => 'adaptiveResize', 'quality' => '90')) ?>"/>
				</div>
			</div>
			<?php endif; ?>

			<?php
			foreach ($modelDesc as $key => $value) {
				$lang = Language::model()->getName($key);
				?>
				<div class="pj-multilang-wrap myLanguage2 control-group" style="display: <?php if ($key==$this->setting['lang_deff']): ?>block<?php else: ?>none<?php endif ?>;" data-id="<?php echo $lang->id ?>">

				<?php
				echo $form->labelEx($value, '['.$lang->code.']benefit_title_1');
			    echo $form->textField($value,'['.$lang->code.']benefit_title_1',array('class'=>'span8', 'maxlength'=>100));
			    ?>
			    <span class="pj-multilang-input"><img src="<?php echo Yii::app()->baseUrl.'/asset/backend/language/'.$lang->code.'.png' ?>"></span>
			    <span class="help-inline _em_" style="display: none;">Please correct the error</span>
				</div>
			    <?php
			}
			?>

			<?php
			foreach ($modelDesc as $key => $value) {
				$lang = Language::model()->getName($key);
				?>
				<div class="pj-multilang-wrap myLanguage2 control-group" style="display: <?php if ($key==$this->setting['lang_deff']): ?>block<?php else: ?>none<?php endif ?>;" data-id="<?php echo $lang->id ?>">

				<?php
				echo $form->labelEx($value, '['.$lang->code.']benefit_content_1');
			    echo $form->textArea($value,'['.$lang->code.']benefit_content_1',array('class'=>'span10', 'maxlength'=>100));
			    ?>
			    <span class="pj-multilang-input"><img src="<?php echo Yii::app()->baseUrl.'/asset/backend/language/'.$lang->code.'.png' ?>"></span>
			    <span class="help-inline _em_" style="display: none;">Please correct the error</span>
				</div>
			    <?php
			}
			?>
		</div>
		<div class="span4">
			<?php echo $form->fileFieldRow($model,'benefit_image_2',array(
			'hint'=>'<b>Note:</b> Picture size 228 x 246px, Larger image will be automatically', 'style'=>"width: 100%")); ?>
			<?php if ($model->scenario == 'update'): ?>
			<div class="control-group">
				<label class="control-label">&nbsp;</label>
				<div class="controls">
					<img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(228,246, '/images/category/'.$model->benefit_image_2 , array('method' => 'adaptiveResize', 'quality' => '90')) ?>"/>
				</div>
			</div>
			<?php endif; ?>

			<?php
			foreach ($modelDesc as $key => $value) {
				$lang = Language::model()->getName($key);
				?>
				<div class="pj-multilang-wrap myLanguage2 control-group" style="display: <?php if ($key==$this->setting['lang_deff']): ?>block<?php else: ?>none<?php endif ?>;" data-id="<?php echo $lang->id ?>">

				<?php
				echo $form->labelEx($value, '['.$lang->code.']benefit_title_2');
			    echo $form->textField($value,'['.$lang->code.']benefit_title_2',array('class'=>'span8', 'maxlength'=>100));
			    ?>
			    <span class="pj-multilang-input"><img src="<?php echo Yii::app()->baseUrl.'/asset/backend/language/'.$lang->code.'.png' ?>"></span>
			    <span class="help-inline _em_" style="display: none;">Please correct the error</span>
				</div>
			    <?php
			}
			?>

			<?php
			foreach ($modelDesc as $key => $value) {
				$lang = Language::model()->getName($key);
				?>
				<div class="pj-multilang-wrap myLanguage2 control-group" style="display: <?php if ($key==$this->setting['lang_deff']): ?>block<?php else: ?>none<?php endif ?>;" data-id="<?php echo $lang->id ?>">

				<?php
				echo $form->labelEx($value, '['.$lang->code.']benefit_content_2');
			    echo $form->textArea($value,'['.$lang->code.']benefit_content_2',array('class'=>'span10', 'maxlength'=>100));
			    ?>
			    <span class="pj-multilang-input"><img src="<?php echo Yii::app()->baseUrl.'/asset/backend/language/'.$lang->code.'.png' ?>"></span>
			    <span class="help-inline _em_" style="display: none;">Please correct the error</span>
				</div>
			    <?php
			}
			?>
		</div>
		<div class="span4">
			<?php echo $form->fileFieldRow($model,'benefit_image_3',array(
			'hint'=>'<b>Note:</b> Picture size 228 x 246px, Larger image will be automatically', 'style'=>"width: 100%")); ?>
			<?php if ($model->scenario == 'update'): ?>
			<div class="control-group">
				<label class="control-label">&nbsp;</label>
				<div class="controls">
					<img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(228,246, '/images/category/'.$model->benefit_image_3 , array('method' => 'adaptiveResize', 'quality' => '90')) ?>"/>
				</div>
			</div>
			<?php endif; ?>

			<?php
			foreach ($modelDesc as $key => $value) {
				$lang = Language::model()->getName($key);
				?>
				<div class="pj-multilang-wrap myLanguage2 control-group" style="display: <?php if ($key==$this->setting['lang_deff']): ?>block<?php else: ?>none<?php endif ?>;" data-id="<?php echo $lang->id ?>">

				<?php
				echo $form->labelEx($value, '['.$lang->code.']benefit_title_3');
			    echo $form->textField($value,'['.$lang->code.']benefit_title_3',array('class'=>'span8', 'maxlength'=>100));
			    ?>
			    <span class="pj-multilang-input"><img src="<?php echo Yii::app()->baseUrl.'/asset/backend/language/'.$lang->code.'.png' ?>"></span>
			    <span class="help-inline _em_" style="display: none;">Please correct the error</span>
				</div>
			    <?php
			}
			?>

			<?php
			foreach ($modelDesc as $key => $value) {
				$lang = Language::model()->getName($key);
				?>
				<div class="pj-multilang-wrap myLanguage2 control-group" style="display: <?php if ($key==$this->setting['lang_deff']): ?>block<?php else: ?>none<?php endif ?>;" data-id="<?php echo $lang->id ?>">

				<?php
				echo $form->labelEx($value, '['.$lang->code.']benefit_content_3');
			    echo $form->textArea($value,'['.$lang->code.']benefit_content_3',array('class'=>'span10', 'maxlength'=>100));
			    ?>
			    <span class="pj-multilang-input"><img src="<?php echo Yii::app()->baseUrl.'/asset/backend/language/'.$lang->code.'.png' ?>"></span>
			    <span class="help-inline _em_" style="display: none;">Please correct the error</span>
				</div>
			    <?php
			}
			?>
		</div>
	</div>
	<br>
	<?php $this->widget('bootstrap.widgets.TbButton', array(
		'buttonType'=>'submit',
		'type'=>'primary',
		'label'=>$model->isNewRecord ? 'Add' : 'Save',
	)); ?>

</div>
</div>
*/
?>

<div class="alert">
  <button type="button" class="close" data-dismiss="alert">×</button>
  <strong>Warning!</strong> Fields with <span class="required">*</span> are required.
</div>

<?php $this->endWidget(); ?>
<script type="text/javascript">
jQuery(function( $ ) {
	$('.multilang2').multiLang({
		target: '.myLanguage2',
	});
})
if (typeof RedactorPlugins === 'undefined') var RedactorPlugins = {};

RedactorPlugins.advanced = {
    init: function()
    {
        alert(1);
    }
}
</script>