<?php
$this->breadcrumbs=array(
	'What we Share'=>array('index'),
	// $model->id=>array('view','id'=>$model->id),
	'Edit',
);

$this->pageHeader=array(
	'icon'=>'fa fa-book',
	'title'=>'What we Share',
	'subtitle'=>'Data What we Share',
);

$this->menu=array(
	array('label'=>'List What we Share', 'icon'=>'th-list','url'=>array('index')),
	array('label'=>'Add What we Share', 'icon'=>'plus-sign','url'=>array('create')),
	// array('label'=>'View What we Share', 'icon'=>'pencil','url'=>array('view','id'=>$model->id)),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?>
<?php echo $this->renderPartial('_form',array('model'=>$model, 'modelDesc'=>$modelDesc)); ?>
