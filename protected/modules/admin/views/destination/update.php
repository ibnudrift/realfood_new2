<?php
$this->breadcrumbs=array(
	'Destination'=>array('index'),
	// $model->id=>array('view','id'=>$model->id),
	'Edit',
);

$this->pageHeader=array(
	'icon'=>'fa fa-book',
	'title'=>'Destination',
	'subtitle'=>'Data Destination',
);

$this->menu=array(
	array('label'=>'List Destination', 'icon'=>'th-list','url'=>array('index')),
	array('label'=>'Add Destination', 'icon'=>'plus-sign','url'=>array('create')),
	// array('label'=>'View Destination', 'icon'=>'pencil','url'=>array('view','id'=>$model->id)),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?>
<?php echo $this->renderPartial('_form',array('model'=>$model, 'modelDesc'=>$modelDesc)); ?>
