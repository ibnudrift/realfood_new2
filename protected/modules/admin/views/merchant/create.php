<?php
$this->breadcrumbs=array(
	'Logo Merchant'=>array('index'),
	'Add',
);

$this->pageHeader=array(
	'icon'=>'fa fa-image',
	'title'=>'Logo Merchant',
	'subtitle'=>'Data Logo Merchant',
);

$this->menu=array(
	array('label'=>'List Logo Merchant', 'icon'=>'th-list','url'=>array('index')),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?>
<?php echo $this->renderPartial('_form', array('model'=>$model, 'modelDesc'=>$modelDesc)); ?>
