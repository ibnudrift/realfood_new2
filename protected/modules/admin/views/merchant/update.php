<?php
$this->breadcrumbs=array(
	'Logo Merchant'=>array('index'),
	// $model->id=>array('view','id'=>$model->id),
	'Edit',
);

$this->pageHeader=array(
	'icon'=>'fa fa-image',
	'title'=>'Logo Merchant',
	'subtitle'=>'Data Logo Merchant',
);

$this->menu=array(
	array('label'=>'List Logo Merchant', 'icon'=>'th-list','url'=>array('index')),
	array('label'=>'Add Logo Merchant', 'icon'=>'plus-sign','url'=>array('create')),
	// array('label'=>'View Logo Merchant', 'icon'=>'pencil','url'=>array('view','id'=>$model->id)),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?>
<?php echo $this->renderPartial('_form',array('model'=>$model, 'modelDesc'=>$modelDesc)); ?>
